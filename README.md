# Página del ENJM (Encuentro Nacional de Juegos de Mesa)

## Pasos para levantar el proyecto localmente
1. Instalar nvm, npm y node
2. Correr `npm install`
3. Si se desea cambiar el backend, cambiar `BACKEND_IP` en `src/apiInteractions/apiCalls.js` por la ip / url del backend
4. Correr `npm run start`

## Estructura
El código en si (componentes y recursos) se encuentran en `src/`. Dentro de ese directorio se están los siguientes subdirectorios:

- `apiInteractions:` En este directorio esta lo que serían integraciones con servicios / recursos externos.
  - `apiCalls:` Integración con el backend
  - `galleryWinners:` Ganadores de la muestra de wargames / escenografía, hardcodeados, se tuvo que hacer con poco tiempo asi que no quería pensar una manera de integrarlo con la DB y manejar esa lógica.
  - `ponchoInteractions:` Como lo de las galerías solo que en vez de porque hubiera poco tiempo lo hice asi por pajero.

- `components:` En este directorio están todos los componentes de React. Atoms, Molecules y Organisms sirven para indicar el grado de complejidad del componente, siendo Atoms el más básico y Organisms el más complejo.

- `images:` En este directorio simplemente están tiradas todas las imágenes que no iba a guardar en la base de datos, banners, logos, etc. Necesitaría una reorganización interna para ver como agruparlas y que no sea un caos total.

- `utils:` En este directorio hay funcioncitas o cosas para usar por muchos modulos.
  - `constants:` La funcionalidad principal de este directorio es tener a text, que es un JSON gigante con todo (cuando había que hacer cosas rapido no) el texto de la página. Constants tiene la funcionalidad de obtener el texto especifico a través de keys. Links esta deprecadísimo.
  - `helpers:` Tiene funciones chicas que por ahi no tenía sentido que estuvieran en un componente porque se usaben en otro componente o porque no tenía nada que ver con ese componente en si.
  