import React from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  const generateMemes = () => {
    const memes = [];

    for (let i = 0; i < 100; i++) {
      memes.push(
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
      );
    }

    return memes;
  };

  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          {generateMemes()}
        </header>
      </div>
  );
}

export default App;
