// const TEST_IP = "http://localhost:3001/";
// :sunglasses:
const BACKEND_IP = "https://gonzah.com:3001/";
// const apiKey = "";

const postHeaders = new Headers();
postHeaders.append("Content-Type", "application/json");

const getHeaders = new Headers();
getHeaders.append("Sec-Fetch-Dest", "document");
getHeaders.append("Sec-Fetch-Mode", "navigate");
getHeaders.append("Referrer-Policy", "no-referrer");

const getter = (url) =>
  fetch(`${BACKEND_IP}${url}`, { headers: getHeaders }).then((response) =>
    response.json()
  );

const verbWithBodyEr = ({ url, body, method }) =>
  fetch(url, {
    method,
    body: JSON.stringify(body),
    headers: postHeaders,
    "content-type": "application/json",
  }).then((response) => response.json());

const poster = (url, body) =>
  verbWithBodyEr({ url: `${BACKEND_IP}${url}`, body, method: "POST" });

const putter = (url, body) =>
  verbWithBodyEr({ url: `${BACKEND_IP}${url}`, body, method: "PUT" });

export const getRightNow = () => getter("ahora-mismo");

export const updateRightNow = (body) => poster("update-right-now", body);

// Query string
// - from
// - to
// - type
export const getEvents = (queryString = "") =>
  getter(`get-events${queryString}`);

export const getGameProposals = (queryString = "") =>
  getter(`get-game-proposals${queryString}`);

export const getEntities = (queryString = "") =>
  getter(`get-entities${queryString}`);

export const getEntity = (queryString = "") =>
  getter(`get-entity${queryString}`);

export const getEnjmInfo = (id) => getter(`get-enjm-info?id=${id}`);

export const handleGalleryEntry = (body) => poster("new-entry", body);

export const getGalleryPictures = (galleryType, ids = "") =>
  getter(`get-gallery?galleryType=${galleryType}${ids}`);

export const getABitFromEachGallery = () => getter("get-from-each-gallery");

export const getGalleryPictureById = ({ galleryType, id, browserId }) =>
  getter(
    `get-image-by-id?galleryType=${galleryType}&id=${id}&browserId=${browserId}`
  );

export const toggleLikeStatus = (body) => putter("toggle-like", body);

export const submitComment = (body) => putter("add-comment", body);

export const saveGame = (body) => poster("save-game", body);

export const updateEvent = (body) => putter("update-event", body);

export const getGames = (queryString = "") => getter(`get-games${queryString}`);

export const saveEvent = (body) => poster("save-event", body);

export const saveParticipant = (body) => poster("save-participant", body);

export const getSponsors = () => getter("get-sponsors");

export const getShops = () => getter("get-shops");

export const editShop = (body) => putter("edit-publisher", body);

export const getPublishers = (queryParams = "") =>
  getter(`get-publishers${queryParams}`);

export const getDiscounts = () => getter("get-discounts");

export const saveDiscount = (body) => poster("save-discount", body);

// export const saveEvent = (body) => getter('save-shops');

export const getTeam = () => getter(`get-team`);

export const getNextUp = () => getter(`next-up`);

export const updateNextUp = (body) => putter("next-up", body);

export const saveMessage = (body) => poster("save-message", body);

export const joinList = (body) => poster("join-list", body);

export const getAllMessages = (password) =>
  getter(`get-all-messages?password=${password}`);

export const markAsRead = (body) => putter("mark-as-read", body);

export const getUnread = (password) =>
  getter(`get-unread?password=${password}`);

export const getInList = (password) =>
  getter(`get-in-list?password=${password}`);

export const markAsJoined = (body) => putter("mark-as-joined", body);

export const getNotJoined = (password) =>
  getter(`get-not-joined?password=${password}`);

export const getSedes = () => getter("sedes");

export const updateSede = (body) => putter("sede", body);

export const createSede = (body) => poster("sede", body);
