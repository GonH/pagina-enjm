import ElColosoDeSven from "../images/Coloso.jpg";
import PortalAbisal from "../images/Portal.jpg";
import EntrandoEnBatalla from "../images/EntrandoEnBatalla_01.jpg";
import IllidanStormrage from "../images/Illidan-Bazuzu-4.jpg";
import TrollDeLasCavernas from "../images/Troll.jpg";
import TyranidHarpy from "../images/Tyranid.jpg";

export const esce = [
  {
    src:
      "https://lh3.googleusercontent.com/fife/ABSRlIpAtRa3Civ0avI5SgRBRptaGE4sHiauR9kEmoEJan2OceRW0Mk6zfliq8JfLYL4NdvgfHruUh9UTVfw-rEByaJKk36NrAFQkw5Wfb4qRDTTVoC78lJEJlKpzeUUBpm2JyMH5biBV2byVhXmgjBsQ650ofwZStprLOeKIKhqSTJ9n7DM1RCz0waQDqZW8gxKyWFqdVu99xPbPcHR1n5iX4EoLVCXpKxDzPR3sTD7Ko0_dk_r8M_A7MaS2QxZEl8hKqghbIQakTLKgbco6pDoZji83gmhXGxVB4CBCloCII83JFCcAdBDNrtXR0WM9P9g6nzf04sTibpaa0XZ7st7jJ-WYAJ2cAya8FpwspZkR4gd_sYjHq9jZBOI_D0NddnszQ6d3-K5shn_XEBE-Ywx54YDTCfAHSwIxhW9UuGa1lTZwG88fHAIknSCXIPsApJeSDy6ZqJjd8b6b-nuRDpuU_srJCH-2yxopOGQ0-m2WpLFpn1tMhFTgH_tf9DAxtvW5GU8AbOx17V4xtH39YkLJ2a1_se5a9yv10s7ZML2Lb6X1EAGSAgnhC2ks0JCS187q6AT8A24hpkpK2YpMsyzVjgc6ZyAYstzyz1UXYbYqtnB4II2w46qE3aOLrRaZk62CSPbEnhov1z2dzddkRybAEDOYUk2G1JlurD7G1Ux22frinDFrCcCIRik4GobBH_Muf6Wc2wl_HVjW9qozoqxYBlDn4OqoasNTQ=w1920-h974-ft",
    name: "Acorralado",
    author: "Joaquin Luciano Godoy",
  },
  {
    src: ElColosoDeSven,
    name: "El Coloso de Sven",
    author: 'Nicolas "La Hire" Arce Minuet',
  },
  { src: PortalAbisal, name: "Portal Abisal", author: "Cristian Rainoldi Paz" },
];

export const mini = [
  {
    src:
      "https://lh3.googleusercontent.com/fife/ABSRlIo6_E0X5eHygpULn_2q9o5CSVtrE_OC3AkkcZKZ03hGUCL1ibzD-o0Sx6I4SSoBj9QW5T4rCjL20Lzd52J7d0OijRiMjf_U4rQoMbsODLTOtyahVzJ3OLlrAEPo-PYFvRx0p6FS_z3RPJ8Cu0YnzcHhd7kHZssayJZ2RBBouWvsmpPdM26EEfOavRnefXvp7ZMDIoJyTC2Qv6lz90w5REw7JVkJNUqa612NZIh91zgE8F0DExcdKIQuhZMDWHVsHJKaEx_L_wGk5CWF-xL2Uabw3hrO6r_CEsggvAWuyFQR-fHVyRTphD2-11WkSv9Xd8gfq61IMmvdeGgXinB0q1SoNUgeINB6iMRrpPnQx4wEx55W0JMvf-qM9g-4VLyAN40nN7SgDlCS5qnXsW2Hr-HcYYN-ZabTDbabAQLH2Lqe91pJQTklLfuwaNfKqwghru_bfLaXCEWJVMze7W25SLyZtdnyZEMAyxZ30ATq7Cx4ENiVW1oPXi-Ax5m7uxchtA4DrlbqQyOhAZ5Xb2LAGMHScOa1U5HWlwRqb6zQpVU9MAJjHwTpD3x7EAi7xmuWr77Ty9X8MNcsbCZ5nV2cDBcY0OdnnEuRr7ZqAxohEqFyD_s_O5vFOT1HD5KbKfN0RR1xBcqWSCds7IFiB6EAhR12LkiIugEGBqIE8rr62BSwTqIZG6DgGw7atgdf1dqMtEyzIvp3u4BgJc0i4gkhqbFi9p4u-B9ffQ=w1920-h648-ft",
    name: "Brummbar el Viejo",
    author: 'Nicolas "La Hire" Arce Minuet',
  },
  {
    src: EntrandoEnBatalla,
    name: "Entrando en Batalla",
    author: "Ana Carolina Massone (Betiel)",
  },
  {
    src:
      "https://lh3.googleusercontent.com/fife/ABSRlIo9E9cYxOxYj5WZlasaI6N4rZ2y48LN_nDgIqazKtCmDIqkkytX7a5hOCekxA6p_F2b5PgR5jKSsM7rGDioPbTebjfvnlE3H2vmyD7ValfiXFzm0lYQ-9rsWWtHyqirCOagLRIuendmsw-O5p71DDpx7hGjQDtskwvOxSIgBGK89olmN5voiZk3fymYs_e6XkvFqGxVR0uFBecl0tahe1_39H-OP3dtdf4UjRHf33TCVg3e8FCGeU54M_Jc_zbdWDOJFA9iJ4ydPNw0OZBRQyexPTHBzPgyMHtY2nOHYa-AwMRBI8_UFTe6_YSugmi3wmKnMfVCEolCmZ9ySrqnQZAYpOQ-Flv6zqwv0PgXKId6tbav8UJd24Kx3iwTixe4_TwGdQlbeL_21q7dEO8UXp9hLKHXM5DQr6lYQcowJSsxaF_AcaQ_g4gbIa73dOh4H5A9xOwiokoLthKfnsuGUPeWBInYIdcFLctKkzJd_RwQP_dblQ45_gpUOSf8bvMU-52xNeYuawqkL77CaXR5BlvD-2oKoITjFFrO_rOcV0zESEvnlC4uNCUllIy0Mg7wI3tnax27drhUDy2T8k2PAuMjNmf4wyqIf2OHuKIxtLEQtUSX1eb9qLTD_NDLUPR9z6ya99Ged2-DgRNT8GUpi7YKW6IW6QQ1o85YiBvPt76FYyfBjDQaC_16QQxL7aw4Sz5JhP3k4_Z1mlGdd1pe-zWxP8rLuL_7SA=w1920-h648-ft",
    name: "Henan!",
    author: 'Nicolas "La Hire" Arce Minuet',
  },
  {
    src: IllidanStormrage,
    name: "Illidan Stormrage / Bazuzu",
    author: "Cristian Rainoldi Paz",
  },
  {
    src: TrollDeLasCavernas,
    name: "Troll de Las Cavernas",
    author: "Crespo Lionel Ezequiel",
  },
  { src: TyranidHarpy, name: "Tyranid Harpy", author: "Walter Gabriel Bruno" },
];
