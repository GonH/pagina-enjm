import SoyPicture from "../images/SoyJugandoASer.png";

const KINMO = {
  name: "Kinmo",
  src:
    "https://i.pinimg.com/originals/d8/67/0a/d8670ade5804cdfe09a69b41359c0f7e.jpg",
  link: "https://www.facebook.com/KinmoPasacronos",
  editorial: "",
  autor: "Bruss Brusco",
};
const CORONA_DE_HIERRO = {
  name: "Corona de Hierro",
  src:
    "https://www.eldragonazul.com.ar/wp-content/uploads/2017/03/Tapa-Corona.jpg",
  link: "https://www.eldragonazul.com.ar/juegos-corona-de-hierro/",
  editorial: "El dragón azul",
  autor: "",
};
const COMBATE_DE_SAN_LORENZO = {
  name: "Combate de San Lorenzo",
  src:
    "https://juegosdemesaargentinos.files.wordpress.com/2015/04/combate-de-san-lorenzo.jpg?w=1180",
  link: "http://epicajuegos.com.ar/juegos/",
  editorial: "Épica",
  autor: "",
};
const DIXIT = {
  name: "Dixit",
  src:
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.iH3fJq-Gy78ZA497btSilwHaHn%26pid%3DApi&f=1",
  editorial: "",
  autor: "Jean-Louis Roubira",
};
const SOY_JUGANDO_A_SER = {
  name: "Soy Jugando a Ser",
  link: "https://www.facebook.com/SoyJugandoaSer",
  editorial: "",
  autor: "",
  src: SoyPicture,
};
const CONEJOS_EN_EL_HUERTO = {
  name: "Conejos en el Huerto",
  src:
    "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fenderjuegos.com%2Fmedia%2Fcatalog%2Fproduct%2Fcache%2F1%2Fimage%2F85e4522595efc69f496374d01ef2bf13%2Fc%2Fo%2Fconejos05.jpg&f=1&nofb=1",
  link: "https://ruibalgames.com/conejos-por-todas-partes/",
  editorial: "Ruibal",
  autor: "",
};
const CULTIVOS_MUTANTES = {
  name: "Cultivos Mutantes",
  src: "https://www.eldragonazul.com.ar/wp-content/uploads/2020/06/header5.jpg",
  link: "https://www.eldragonazul.com.ar/juegos-cultivos-mutantes/",
  editorial: "El dragón azul",
  autor: "",
};
const SUCESOS_ARGENTINOS = {
  name: "Sucesos Argentinos",
  src: "https://lajugandera.com/wp-content/uploads/2020/04/sucesos-1.jpg",
  editorial: "AA Ludica",
  autor: "",
};
const CARCASSONE = {
  name: "Carcassonne",
  src:
    "https://www.misifu.es/wp-content/uploads/2018/11/carcassonne-juego-de-mesa-min.png",
  link: "https://aaludica.com.ar/",
  editorial: "",
  autor: "Klaus-Jürgen Wrede",
};
const NACIO_POPULAR = {
  name: "Nació Popular",
  src:
    "https://d26lpennugtm8s.cloudfront.net/stores/579/420/products/img_48071-a3db0a7c2a78eed26e15663097986581-320-01-2364becf962c8e3ffe15980277672132-1024-1024.jpg",
  link: "https://www.facebook.com/editorial.lajugandera",
  editorial: "La Jugandera",
  autor: "",
};
const CAMARERO = {
  name: "Camarero",
  src:
    "https://res.cloudinary.com/srcouto/image/upload/c_fill,g_center,h_900,q_auto:eco,w_900,x_0,y_0/v1633459303/testenjm10/camarero_ufxsq6.jpg",
  link: "https://maldon.com.ar/blog/projects/el-camarero/",
  editorial: "Maldón",
  autor: "",
};
const LUNES = {
  name: "Lunes",
  src:
    "https://images.ctfassets.net/mc6j42086v0m/4rvcvdr7SPISTFWPLG9ntC/63502008cf54dece0a6600605ddf3344/Lunes.jpg",
  link: "http://supernoobgames.com/lunes/",
  editorial: "Super Noob Games",
  autor: "",
};
const FAST_FOOD = {
  name: "Fast Food!",
  src: "http://juegosdemesa.com.ar/fotos/fastfood1.jpg",
  link: "http://juegosdemesa.com.ar/juegos/#fastfood",
  editorial: "Juegos de Mesa",
  autor: "",
};
const MAGIC_MAZE = {
  name: "Magic Maze",
  src:
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.YrJ1jn8mr-tUJoK8Gqz4FgHaHa%26pid%3DApi&f=1",
  editorial: "Sit Down!",
  autor: "Kasper Lapp",
};
const ADIVINA_QUE_TENGO = {
  name: "Adiviná qué tengo",
  src: "https://lajugandera.com/wp-content/uploads/2020/04/15003273_569027966624287_2602250287942553630_o11-e9e70cf682e4aaa5a015124461268559-640-0.jpg",
  link: "https://www.facebook.com/espejodemundos/",
  editorial: "El Espejo de Mundos",
  autor: "",
};
const QUIEN_QUIERE_SER_PRESIDENTE = {
  name: "¿Quién quiere ser presidente?",
  src: "https://juegosrollandwrite.com/wp-content/uploads/2020/04/QQSP-RW.jpg",
  link: "https://es-la.facebook.com/quienquiereserpresidente/",
  editorial: "",
  autor: "Nicolás Martínez Sáez",
};
const ROOT = {
  name: "Root",
  src: "https://cdn.shopify.com/s/files/1/0106/0162/7706/products/1-RootGameBox-Edit-Web_1024x1024.png?v=1595294735",
  link: "https://ledergames.com/products/root-a-game-of-woodland-might-and-right",
  editorial: "Leder Games",
  autor: "",
};
const BALANCE_ELEMENTAL = {
  name: "Balance Elemental",
  src: "https://aquihaydragones.com.ar/wp-content/uploads/2020/08/balance-elemental-500x500.jpg",
  link: "https://www.instagram.com/pulgaescapista/",
  editorial: "Pulga Escapista",
  autor: "Julián Vecchione",
};
const LAS_DIVAS = {
  name: "Las Divas",
  src: "https://http2.mlstatic.com/D_NQ_NP_661342-MLA31619727506_072019-O.webp",
  link: "https://www.facebook.com/playlasdivas/",
  editorial: "",
  autor: "",
};

const premios = {
  "2017": {
    Infantil: [KINMO],
    Familiar: [KINMO],
    Adultes: [CORONA_DE_HIERRO],
    Introductorio: [KINMO],
    Identidad: [COMBATE_DE_SAN_LORENZO],
    Extranjero: [DIXIT],
  },
  "2018": {
    Infantil: [SOY_JUGANDO_A_SER],
    Familiar: [CONEJOS_EN_EL_HUERTO],
    Adultes: [CONEJOS_EN_EL_HUERTO],
    Introductorio: [CULTIVOS_MUTANTES],
    Identidad: [SUCESOS_ARGENTINOS],
    Extranjero: [CARCASSONE],
  },
  "2019": {
    Infantil: [NACIO_POPULAR],
    Familiar: [CAMARERO],
    Adultes: [LUNES],
    Introductorio: [FAST_FOOD],
    Identidad: [SUCESOS_ARGENTINOS],
    Extranjero: [MAGIC_MAZE],
    Diversidad: [SOY_JUGANDO_A_SER],
  },
  "2020": {
    Infantil: [ADIVINA_QUE_TENGO],
    Intergeneracional: [CONEJOS_EN_EL_HUERTO],
    Adultes: [QUIEN_QUIERE_SER_PRESIDENTE],
    Introductorio: [BALANCE_ELEMENTAL],
    Identidad: [COMBATE_DE_SAN_LORENZO],
    Extranjero: [MAGIC_MAZE, ROOT],
    Diversidad: [LAS_DIVAS, SOY_JUGANDO_A_SER]
  }
};

export const currentYearNomination = {
  Infantil: [
    "Adivina que tengo",
    "Animal Color",
    "Fast Food",
    "Gelato",
    "Pirámide del Faraón",
  ],
  Intergeneracional: [
    "Balance Elemental",
    "Caiaaate",
    "Conejos en el Huerto",
    "Fast Food",
    "Gelato",
    "Lab Rats",
  ],
  Adultos: [
    "Corona de Hierro",
    "Intendentes",
    "Lunes",
    "Magus Aura Mortis",
    "Quien quiere ser presidente?",
    "Republia",
  ],
  Introductorio: [
    "Balance Elemental",
    "Cruce de los Andes",
    "Cultivos Mutantes",
    "Gelato",
    "Lab Rats",
  ],
  Extranjero: ["Aventureros al Tren", "Catan", "Dixit", "Magic Maze", "Root"],
  "Mención Identidad": [
    "Argentidados",
    "Caiaaate",
    "Combate de San Lorenzo",
    "Cruce de los Andes",
    "Originarios",
  ],
  "Mención Diversidad": [
    "En la Punta de la Lengua jóvenes",
    "Las Divas",
    "Soy jugando a ser",
    "Warmis",
  ],
};

const getPrizesByYears = (years) => {
  const result = {};

  years.forEach((year) => {
    result[year] = premios[year];
  });

  return result;
};

const getPrizesByCategories = (categories, filteredByYears) => {
  const result = {};

  Object.keys(filteredByYears).forEach((year) => {
    const yearInfo = filteredByYears[year];
    const yearResult = {};
    Object.keys(yearInfo).forEach((category) => {
      if (categories.includes(category))
        yearResult[category] = yearInfo[category];
    });

    if (Object.keys(yearResult).length) result[year] = yearResult;
  });

  return result;
};

export const getPonchoPrizes = ({ categories, years }) => {
  const filteredByYears = years.length ? getPrizesByYears(years) : premios;
  const filteredByCategories = categories.length
    ? getPrizesByCategories(categories, filteredByYears)
    : filteredByYears;

  console.info({ filteredByYears, filteredByCategories });

  return filteredByCategories;
};

export const getPremiosPonchoYears = () => ["2017", "2018", "2019", "2020"];

export const getPremiosPonchoCategories = () => [
  "Infantil",
  "Familiar",
  "Adultes",
  "Introductorio",
  "Identidad",
  "Diversidad",
  "Extranjero",
];
