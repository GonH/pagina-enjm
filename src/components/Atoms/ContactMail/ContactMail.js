import React from "react";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./ContactMailStyle.css";

const ContactMail = () => (
  <div className="contactMail">
    <h1>{getTextByComposedKeys("contact.mail.title")}</h1>
    <label className="relative w-full mx-2 overflow-hidden truncate">{getTextByComposedKeys("contact.mail.mail")}</label>
  </div>
);

export default ContactMail;
