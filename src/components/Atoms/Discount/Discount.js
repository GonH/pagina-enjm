import React from "react";

import "./Discount.css"

const Discount = ({ link, image, name }) => (
  <a className="discount" href={link} target="_blank" rel="noreferrer">
    <img
      src={image}
      alt={`Banner de publicidad de ${name}, para ir a la tienda y ver los descuentos andá a ${link}`}
    />
  </a>
);

export default Discount;
