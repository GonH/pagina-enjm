import React from "react";

import { GENDER_FORM } from "../../../utils/constants/constants";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./GenderFormStyles.css";

const GenderForm = () => (
  <div className="genderFormContainer">
    <h3>
      {getTextByComposedKeys("genderForm.start")}
      {getTextByComposedKeys("genderForm.middle")}
      {getTextByComposedKeys("genderForm.end")}
    </h3>
    <iframe src={GENDER_FORM} title="Formulario"/>
  </div>
);

export default GenderForm;
