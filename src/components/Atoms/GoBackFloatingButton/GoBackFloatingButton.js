import React from "react";
import { Button } from "reactstrap";
import { useHistory } from "react-router-dom";

import {getTextByComposedKeys} from "../../../utils/constants/text";
import "./GoBackFloatingButtonStyles.css";

const GoBackFloatingButton = ({ buttonText, redirectTo }) => {
  const history = useHistory();

  const { innerWidth } = window;
  const isSmallWindow = innerWidth < 700

  return (
    <div className="backToGalleryButton">
      <Button color="info" onClick={() => history.push(redirectTo)}>
        {isSmallWindow ? getTextByComposedKeys('galleries.goBack') : buttonText}
      </Button>
    </div>
  );
};

export default GoBackFloatingButton;
