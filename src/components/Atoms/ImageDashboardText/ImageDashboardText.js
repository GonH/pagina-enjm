import React from "react";
import { getTextByComposedKeys } from "../../../utils/constants/text";

const ImageDashboardText = ({ type }) => (
  <label className="max-w-2xl mx-auto font-sans text-4xl text-gray-800 ">{getTextByComposedKeys(`${type}.text`)}</label>
);

export default ImageDashboardText;
