import React from "react";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./ImageDashboardTitleStyles.css";

const ImageDashboardTitle = ({ type }) => (
  <label className="imageDashboardTitle">
    {getTextByComposedKeys(`${type}.title`)}
  </label>
);

export default ImageDashboardTitle;
