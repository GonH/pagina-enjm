import React from "react";

import Heart from "../../../images/heart-shape-outline.svg";
import "./ImageLikesButton.css";

const ImageLikesButton = ({ isLiked, onClick }) => (
  <div
    onClick={onClick}
    className={`likeButton ${isLiked ? "liked" : "notLiked"}`}
  >
    <img className="likeImage" src={Heart} />
  </div>
);

export default ImageLikesButton;
