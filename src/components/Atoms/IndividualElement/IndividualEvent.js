import React from "react"

import "./IndividualEventStyles.css"
import { CURRENT_YEAR } from "../../../utils/constants/constants";

const parseCurrentEventType = (currentEventType = "", isGameProposal) => {
  if (isGameProposal || !currentEventType) return ""

  const parsedType =
    currentEventType.toLowerCase() === "tercera edad"
      ? "Adultos Mayores"
      : currentEventType

  return `(${parsedType})`
}

const formatTime = (start, end) => {
  return `${start} - ${end}`
}

const typesDict = {
  Charla: "Charla",
  Taller: "Taller",
  Conversatorio: "Conver.",
  Torneo: "Torneo",
  Rol: "Rol",
  Entrevista: "Entrev.",
  Presentación: "Pres.",
  "Propuesta Lúdica": "P. Lúdica",
  Vivo: "Vivo",
  "Presentación de Juego": "Pres. Juego",
  "Mesa Debate": "Debate",
}

const IndividualEvent = ({ type, event, eventSelect, isGameProposal }) => {
  const { description, name, start, end, currentEventType, link, year } = event

  const { innerWidth } = window
  const mustParse = innerWidth < 700

  return (
    <li className="individualEventContainer" data-date={formatTime(start, end)}>
      <div className="individualEventTitle">
        <div className="flex flex-col items-start justify-start w-full py-0 pr-6 my-2 mr-6 font-serif text-lg font-bold text-center border-gray-200 md:items-center md:justify-center md:border-r-2 md:w-64">
          <div>
            {start} - {end}hs{" "}
          </div>
          {type && (
            <h4 className="flex justify-start w-full md:justify-center">
              <span className={`${type}Badge Badge`}>
                {mustParse ? typesDict[type] : type}
              </span>
            </h4>
          )}
        </div>
        <div className="w-full mx-0 my-2 md:mx-4 md:my-0">
          <h3 onClick={() => eventSelect({ ...event, type })}>{name}</h3>
          <small className="text-sm font-bold uppercase">
            {" "}
            {parseCurrentEventType(currentEventType, isGameProposal)}
          </small>

          <p className="text-left line-clamp-3">
            {description &&
              description
                .split("\n")
                .map((paragraph, index) => <p key={index}>{paragraph}</p>)}
          </p>
        </div>
        <button
          className="w-full md:mt-3 md:w-auto btn btn-info "
          onClick={() => eventSelect({ ...event, type })}
        >
          {link && CURRENT_YEAR === year ? "Participar" : "Ver más info"}
        </button>
      </div>
    </li>
  )
}

export default IndividualEvent
