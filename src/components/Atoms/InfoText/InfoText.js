import React from "react";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./InfoTextStyles.css";

const InfoText = ({ prefix, suffix }) => {
  const text = getTextByComposedKeys(`${prefix}.texts.${suffix}`);

  let paragraphs = [];
  text.split("\n").forEach((paragraph) => {
    paragraphs.push(<label>{paragraph}</label>);
    paragraphs.push(<br />);
  });

  return <div className="wraper">{paragraphs}</div>;
};

export default InfoText;
