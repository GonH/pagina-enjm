import React from "react"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./InfoTitleStyles.css"

const InfoTitle = ({ prefix, suffix }) => {
  const title = getTextByComposedKeys(`${prefix}.titles.${suffix}`)

  return <label className="infoTitle">{title}</label>
}

export default InfoTitle
