import React from "react";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./MeetingEventStyles.css";

const MeetingEvents = ({ title, content }) => (
  <div className="meetingEventContainer">
    <h2>{getTextByComposedKeys(`previousEvents.titles.${title}`)}:</h2>
    <div className="meetingEventContent">
      {content.map((item) => (
        <label>{`• ${item}`}</label>
      ))}
    </div>
  </div>
);

export default MeetingEvents;
