import React from "react";

const PlaceInfo = ({ place, placeLink }) => {
  if (!place && !placeLink) return <div />;

  if (place && !placeLink) return <label>{place}</label>;

  return (
    <a href={placeLink} target="_blank" rel="noopener noreferrer">
      {place || "Hacé click acá para ver el lugar en el mapa"}
    </a>
  );
};

export default PlaceInfo;
