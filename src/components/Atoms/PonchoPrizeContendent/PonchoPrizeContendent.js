import React from "react"

import { getTextByComposedKeys } from "../../../utils/constants/text"
import { openLinkOnNewTab } from "../../../utils/helpers/uglyHelpers"
import "./PonchoPrizeContendentStyles.css"
import { LazyLoadImage } from "react-lazy-load-image-component"
import "react-lazy-load-image-component/src/effects/blur.css"

const PonchoPrizeContendent = ({ name, src, link = "", editorial, autor }) => {
  console.info({ src })

  return (
    <div className="h-64 contendantName">
      <div className="relative z-10 text-left ">
        <div className="absolute inset-0 z-0 w-full h-64 overflow-hidden opacity-80">
          <LazyLoadImage
            className="object-cover w-full h-64 "
            alt={`Foto de ${name}`}
            effect="blur"
            src={src}
          />
        </div>
        <div className="relative z-30 h-64 p-3 py-3 text-white bg-gradient-to-b from-indigo-900 via-indigo-90 to-transparent ">
          <div className="relative flex flex-col pt-0 pr-10">
            <h2 className="m-0 text-xl">{name}</h2>
            {autor && (
              <small className="font-serif text-sm italic font-bold text-gray-100">
                de {autor}
              </small>
            )}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href={link}
              className="absolute top-0 right-0 z-50 font-sans text-white underline hover:opacity-80 "
              onClick={() => openLinkOnNewTab(link)}
            >
              Link
            </a>
          </div>

          <div className="">
            {editorial && (
              <div className="text-sm">
                <label className="hidden ">
                  {getTextByComposedKeys(`ponchoPrizes.prize.publisher`)}
                </label>
                <label>{editorial}</label>
              </div>
            )}
            {/* {autor && (
              <div className="hidden winnerInfo">
                <label className=" winnerInfoTitle">
                  {getTextByComposedKeys(`ponchoPrizes.prize.author`)}
                </label>
                <label>{autor}</label>
              </div>
            )} */}
          </div>
        </div>
      </div>
    </div>
  )
}

export default PonchoPrizeContendent
