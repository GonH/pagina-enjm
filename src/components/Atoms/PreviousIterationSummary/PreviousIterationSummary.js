import React from "react"

import "./PreviousIterationSummaryStyle.css"
import { LazyLoadImage } from "react-lazy-load-image-component"
import "react-lazy-load-image-component/src/effects/blur.css"

const PreviousIterationSummary = ({ id, src }) => {
  return (
    <a href={`/encuentro/${id}`} className="p-12 duration-300 bg-red-500">
      <LazyLoadImage
        alt={`ENJM ${id}`}
        effect="blur"
        src={src}
      />
    </a>
  )
}

export default PreviousIterationSummary
