import React from "react";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import { FB_LINK, TW_LINK, IG_LINK } from "../../../utils/constants/constants";
import FBLogo from "../../../images/fbLogo.png";
import TWLogo from "../../../images/twLogo.png";
import IGLogo from "../../../images/igLogo.png";
import "./SocialNetworksStyles.css";

const SocialNetworks = () => {
  return (
    <div className="socialNetworks">
      <h2>{getTextByComposedKeys("contact.socialNetworks")}</h2>
      <div className="socialNetworksAnchorContainer">
        <a href={FB_LINK} target="_blank" rel="noopener noreferrer">
          <img src={FBLogo} alt={`Logo de facebook que redirige a ${FB_LINK}`} />
        </a>
        <a href={IG_LINK} target="_blank" rel="noopener noreferrer">
          <img
            src={IGLogo}
            alt={`Logo de instagram que redirige a ${IG_LINK}`}
          />
        </a>
        <a href={TW_LINK} target="_blank" rel="noopener noreferrer">
          <img src={TWLogo} alt={`Logo de twitter que redirige a ${TW_LINK}`} />
        </a>
      </div>
    </div>
  );
};

export default SocialNetworks;
