import React from "react";

import { getStyle } from "../../../utils/helpers/uglyHelpers";
import "./TwitchEmbedStyles.css";

const parent = window.location.host;

const TwitchEmbed = ({ src, title, customClass, style = {} }) => (
  <iframe
    title={title}
    className={customClass || "twitchEmbed"}
    src={`${src}&parent=${parent}`}
    scrolling="no"
    frameBorder="0"
    allowFullScreen
    style={getStyle(style)}
  />
);

export default TwitchEmbed;
