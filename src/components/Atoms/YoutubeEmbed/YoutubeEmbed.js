import React from "react";

import { getStyle } from "../../../utils/helpers/uglyHelpers";
import "./YoutubeEmbedStyles.css";

const YoutubeEmbed = ({ src, title, customClass, style = {}, autoPlay }) => (
  <iframe
    title={title}
    className={customClass || "youtubeEmbed"}
    src={`${src}?rel=0&autoplay=${autoPlay ? 1 : 0}`}
    frameBorder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    allowFullScreen
    style={getStyle(style)}
  />
);

export default YoutubeEmbed;
