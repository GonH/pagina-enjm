import React from "react";

import { getStyle } from "../../../utils/helpers/uglyHelpers";
import "./YoutubeEmbedStyles.css";

const YoutubePlaylistEmbed = ({ customClass, style = {} }) => (
  <iframe
    title="Playlist ENJM"
    className={customClass || "youtubeEmbed"}
    src="https://www.youtube.com/embed/videoseries?list=PLIylPgicDk_AK-XNev885_2M1NF9Tabcm"
    frameBorder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    allowFullScreen
    style={style && getStyle(style)}
  />
);

export default YoutubePlaylistEmbed;
