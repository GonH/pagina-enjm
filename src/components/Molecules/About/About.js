// import { Button } from "reactstrap";
// import { getTextByComposedKeys } from "../../../utils/constants/text"

import React from "react"
import "./AboutStyles.css"
import Fade from "react-reveal/Fade"
import { LazyLoadImage } from "react-lazy-load-image-component"
import "react-lazy-load-image-component/src/effects/blur.css"
import HomeEnjmTablero from "../../../images/enjm10/enjm10-tablero.jpg"
import AnchorLink from "react-anchor-link-smooth-scroll"
// const basesIlustracion =
//   "https://drive.google.com/file/d/132MSO56AOuD-X30dxAkif4VOU73jLZ7k/preview"
// const boletin2 =
//   "https://v3.esmsv.com/campaign/htmlversion?AdministratorID=132382&CampaignID=22&StatisticID=16&MemberID=428&s=26704e6b7ca45ba31962df48984ab69b&isDemo=0"
// const rumbo =
//   "https://docs.google.com/forms/d/e/1FAIpQLSdzCi2SZnSFjz1v1N2JPfQSRsmVtqEata3V_0QIdL28plRkIw/viewform"
// const escuelasForm =
//   "https://docs.google.com/forms/d/e/1FAIpQLSc8abi5yM-2HBhOSaEVxZXdp-TOQnydZHUdU2n8X917jv92wg/viewform"
// const escuelasPropuesta =
//   "https://drive.google.com/file/d/1pxGqWI7K_kt_nIQLLpPtN6HSBGBuVEaX/preview"

// const optionsObj = {
//   basesIlustracion,
//   boletin2,
//   rumbo,
//   escuelasForm,
//   escuelasPropuesta,
// }
import { SRLWrapper } from "simple-react-lightbox"
const About = () => {
  // const [selectedOption, setSelectedOption] = useState("basesIlustracion")

  // const { innerHeight } = window

  // const options = [
  //   "basesIlustracion",
  //   "boletin2",
  //   "rumbo",
  //   "escuelasForm",
  //   "escuelasPropuesta",
  // ]

  return (
    <div className="about ">
      <div className="bg-indigo-600 ">
        <Fade delay={100} duration={1200}>
          <div className="flex flex-col text-white bg-indigo-600 py-42 pt-52 hero bg-gradient-to-tr from-indigo-600 to-indigo-800">
            <AnchorLink href="#about" className="no-underline">
              <div className="w-full max-w-xs mx-auto h-80">
                <LazyLoadImage
                  alt="ENJM Logo"
                  effect="blur"
                  src="https://res.cloudinary.com/srcouto/image/upload/v1633445047/testenjm10/enjm10/logo-enjm10_u18e8q.png"
                  width="300"
                  height="300"
                />
              </div>
            </AnchorLink>
            <h1 className="my-16 font-mono font-bold text-center uppercase text-8xl">
              Acerca del ENJM
            </h1>
          </div>
        </Fade>
      </div>
      <SRLWrapper>
        <article
          id="about"
          className="max-w-full px-6 pt-24 pb-24 mx-auto prose prose-xl md:px-0 prose-indigo md:prose-1xl"
        >
          <p>
            El Encuentro Nacional de Juegos de Mesa, ENJM, es el festejo anual que
            realizamos sobre el campo de los Juegos de Mesa en la Argentina. Este año
            estamos celebrando el #10ENJM durante los días 30 y 31 de Octubre en la Casa
            de la Cultura de Almirante Brown, provincia de Buenos Aires.
          </p>
          <p>
            Un nuevo encuentro dentro del actual contexto, por lo que este año el evento
            será de manera híbrida, con un fuerte compromiso y entusiasmo en recobrar la
            presencialidad pero buscando sostener la virtualidad ganada el año pasado para
            sumar a quienes no puedan acercarse.
          </p>
          <p>
            Cada edición nos proponemos pensar en torno a un eje temático que nos genere
            preguntas y ponga en tensión algunas certezas. Este año particular, cumpliendo
            el décimo aniversario de encontrarnos, el manual titula: La Historia Cuenta un
            Juego.
          </p>
          <p>
            Allá por el 2012 imaginamos que era posible encontrarnos quienes veníamos o
            queríamos hacer un nuevo mundo posible, el de los juegos de mesa en nuestro
            país. Por aquella época no había tanta gente y nos juntamos cerca de 100
            personas durante un día intenso pero que dio inicio a una partida que aún se
            sigue jugando. Cada nuevo encuentro fue ampliando la mirada, aumentando el
            caudal de propuestas y la diversidad de participantes, sumando clubes,
            tiendas, podcasts, editoriales y hasta presencias internacionales, para lograr
            así festejar un 2020 que, pese a la pandemia,  convocó a clubes de juego de
            distintas provincias, todas las editoriales del país y extranjeras,  páginas
            especializadas, presentación de juegos y prototipos, y en el que participaron
            personas de Argentina, Chile, Uruguay, Bolivia, Brasil, México, Colombia,
            España y Francia…
          </p>
          <p>
            Así fue que el completamos un nuevo hito, celebrando el primer ENJM de forma
            virtual, luego de los 4 primeros realizados en la Ciudad Autónoma de Buenos
            Aires y que después del #5 al #8 haya rodado por el país, desde la ciudad de
            Córdoba, hasta Mendoza, Bahía Blanca y Villa María.
          </p>
          <div className="relative px-5 py-64 pt-64 overflow-hidden bg-indigo-900 background-image group">
          <Fade bottom delay={100} duration={1200}>
            <h4 className="relative z-10 px-12 py-6 mb-6 overflow-hidden font-mono text-5xl font-normal text-center text-white duration-500 bg-gray-900 shadow-2xl group-hover:bg-indigo-700 rounded-xl bg-opacity-95 lg:text-5xl">
              Aquel primer encuentro de un día entre gente conocida para darnos un gustito
              se convirtió en algunos años en uno de los eventos más grandes del mundo de
              los juegos de mesa en Latinoamérica.
            </h4>
            </Fade>
            <img
              className="absolute inset-0 w-full h-auto duration-500 group-hover:opacity-10"
              src={HomeEnjmTablero}
              alt=""
            />
          </div>
          <Fade bottom delay={100} duration={1200}>
          <h3>¿Encuentro Nacional de Juegos de Mesa?</h3>
          </Fade>
          <p>
            Sí, así lo planteamos desde un inicio e intentamos sostener esta matriz. Es un
            encuentro porque nos parece importante que se centre en lo vincular. La idea
            es vernos, compartir ideas y ganas, contactar con gente que no conocíamos pero
            que está en la misma. Que la gente que difunde pueda conocer a quienes crean
            los juegos para comentarles cómo repercuten en sus encuentros, que tomen forma
            esos nombres que suelen aparecer como artistas, testers o ilustradores.
          </p>

          <p>Dentro de las propuestas, nos parece importante poder contar con:</p>
          <ul>
            <li>
              Espacios de relatos de experiencias, para aprender de los recorridos de
              otras y otros
            </li>
            <li>
              Talleres de quienes han logrado sintetizar parte de ese recorrido de una
              manera ordenada, buscando que sea un espacio de crecimiento en busca de
              profesionalizar roles y recorridos
            </li>
            <li>
              Contactar referentes de los juegos en otros lugares del mundo. para que nos
              ayuden a pensar lo local con estrategias y formas que van dando resultados
              en sus lugares de trabajo. Hemos entrevistado en estos diez años a gente de
              Brasil, Chile, México, Uruguay, Estados Unidos, España, Francia, Alemania e
              Inglaterra, quienes desde el diseño, la promoción, la ilustración y las
              partidas nos han compartido en vivo o vía videoconferencia, algunas ideas y
              certezas en torno a sus campos de intervención.
            </li>
          </ul>
          <p>
            Hablamos de un Encuentro Nacional, por lo que nos parece sumamente importante:
          </p>
          <ul>
            <li>Que se escuchen voces y se vean juegos de distintos puntos del país.</li>
            <li>
              Que cuenten de sus realidades y compartan los diseños realizados,
              compartiendo partidas
            </li>
            <li>Que puedan llegar a nuevos hogares, hasta otras latitudes.</li>
          </ul>
          <p>
            En los últimos encuentros aumentó notoriamente la gente que viajaba para
            participar, compartiendo en 2019 con participantes de 16 provincias, y el año
            pasado, con la ventaja de lo virtual, superamos la participación federal con
            20 provincias participando e internacional, con participantes de 10 países.
          </p>
          <p>
            Es interesante rescatar del encuentro la convivencia de una gran multiplicidad
            de juegos y jugadores. Uno de los objetivos de los ENJM es su carácter
            inclusivo y eso se cumple edición tras edición. Podemos ver sobre las mesas
            desde juegos abstractos como Go, Othello y Mahjong, juegos clásicos como el
            TEG o nuevos juegos recién premiados en Alemania o España, tanto como
            prototipos que todavía se están moldeando a lo largo y ancho de nuestras
            tierras. Pueden encontrarse espacios con partidas de rol, mesas gigantes de
            wargames, juegos modernos, clásicos; con miniaturas, fichas, cartas, dados,
            híbridos y hasta juegos en versiones gigantes, en madera u otros materiales,
            recreaciones cual obras de arte con grandes producciones o simples impresiones
            en blanco y negro para marcar con lápiz u ocupar directamente como si fuéramos
            piezas quienes jugamos las partidas.
          </p>
          <p>
            Después de un año atípico, el juego de mesa ha llegado como aliado a muchas
            casas, como recurso a través de propuestas docentes, como alternativa de las
            editoriales que fueron elaborando juegos a través de las redes o, como
            novedad, a través de plataformas y nuevas apps. A pesar de la imposibilidad de
            encontrarnos físicamente, los juegos han logrado mantenernos cerca. E incluso
            en este contexto, a priori desfavorable, no sólo se han publicado nuevos
            juegos sino que además han aparecido nuevas plataformas de juego online,
            alguna de las que registraron más de 10 mil personas jugando simultáneamente,
            han cuadruplicado las propuestas en algunas de ellas y las ventas en sólo un
            año aumentaron un 55%, lo cual viene generando una mayor visibilidad de las
            ideas locales, tanto dentro de nuestro territorio como a nivel mundial. Y
            junto a esto, se ha permitido poder conocer y testear, aunque de formas
            diferentes, una amplia gama de proyectos y nuevas ideas.
          </p>
          <p>
            Este año en el que continuamos con un contexto pandémico, nos animamos a
            imaginar la fiesta de un nuevo Encuentro Nacional sosteniendo lo realizado en
            el 2020 desde la virtualidad donde participaron más de 3000 personas y con
            mucho entusiasmo puesto también en la posibilidad de buscar presencialidades
            cuidadas, para volver a disfrutar del encuentro sobre la mesa..
          </p>
          <p>
            Desde las organizaciones que estamos impulsando este #10ENJM, les invitamos a
            seguir co construyendo nuevas partidas, desde las pantallas y dentro de lo que
            la presencialidad de sus lugares nos permitan. La convocatoria como siempre
            abierta para participar, proponer y soñar junto a nosotres un nuevo Encuentro,
            nada más que el décimo, sosteniendo la mirada atenta y la cercanía amorosa que
            creemos tan necesaria y con la cabeza preparada para escuchar las nuevas
            historias que los juegos tienen para contarnos.
          </p>
        </article>
      </SRLWrapper>
    </div>
  )
}

export default About
