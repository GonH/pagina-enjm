import React from "react";

import Afiche from "../../../images/Afichefinal.png";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./BuildingStyles.css";

const Building = () => {
  const subtitles = getTextByComposedKeys("building.subtitle")
    .split("\n")
    .map((paragraph) => <label>{paragraph}</label>);

  return (
    <div className="container buildingContainer">
      <img src={Afiche} alt="Afiche del enjm. Título: Imaginando tableros infinitos. Fecha: 20, 21 y 22 de noviembre. Contenido: charlas, talleres, juegos, prototipos. Más información: 9no encuentro nacional de juegos de mesa, @encuentronacionaldejuegosdemesa" />
      <h2>{getTextByComposedKeys("building.title")}</h2>
      {subtitles}
    </div>
  );
};

export default Building;
