import React, { useState } from "react";
import { Collapse } from "reactstrap";

import PonchoPrizeContendent from "../../Atoms/PonchoPrizeContendent/PonchoPrizeContendent";
import "./CategoryPrizeStyles.css";

const CategoryPrize = ({ categoryName, categoryInfo }) => (
  <div className="relative mb-3 overflow-hidden bg-white shadow-md">
    <div
      className={`category-${categoryName} absolute left-0 bottom-0 right-0 z-20 flex items-center justify-center `}
    >
      <h2 className="ponchoPrizeCategory">Categoria {categoryName}</h2>
    </div>
    <Collapse isOpen={true}>
      {categoryInfo.map((info) => (
        <div className="ponchoGamesGrid">
          <PonchoPrizeContendent {...info} />
        </div>
      ))}
    </Collapse>
  </div>
);

export default CategoryPrize;
