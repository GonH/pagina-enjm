import React from "react"

import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./ClearableItemListStyles.css"
import { FiDelete } from "react-icons/fi";
const ClearableItemList = ({ type, itemList, handleItemRemoval }) => {
  if (!itemList.length) return <div />

  return (
    <div className="text-gray-900 filterList">
      <label className=" filterListExplanation">
        {getTextByComposedKeys(`ponchoPrizes.filter.filterBy${type}`)}
      </label>
      {itemList.map((item) => (
        <div className="px-2 py-1 btn btn-success clearableItem" onClick={() => handleItemRemoval(item)}>
          <label>{item}<FiDelete className="inline-block w-4 h-4 ml-3"/></label>
        </div>
      ))}
    </div>
  )
}

export default ClearableItemList
