import React, { useState } from "react"
import { Button } from "reactstrap"

import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./ContactFormStyles.css"
// import SocialNetworks from "../../Atoms/SocialNetworks/SocialNetworks";

const ContactForm = ({ handleSubmit }) => {
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [topic, setTopic] = useState("")
  const [message, setMessage] = useState("")

  const getPlaceholder = (key) => getTextByComposedKeys(`contact.sendMail.${key}`)

  const internalSubmit = () => {
    handleSubmit({ email, name, topic, message }).then(() => {
      setName("")
      setEmail("")
      setTopic("")
      setMessage("")
    })
  }

  return (
    <div className="contactForm">
      <div className="gridRow">
        <input
          placeholder={getPlaceholder("name")}
          value={name}
          onChange={({ target: { value } }) => setName(value)}
        />
      </div>
      <div className="gridRow">
        <input
          placeholder={getPlaceholder("email")}
          value={email}
          onChange={({ target: { value } }) => setEmail(value)}
        />
      </div>
      <div className="gridRow">
        <input
          className="fullColInput"
          placeholder={getPlaceholder("topic")}
          value={topic}
          onChange={({ target: { value } }) => setTopic(value)}
        />
      </div>
      <div className="gridRow">
        <textarea
          className="fullColTextarea"
          placeholder={getPlaceholder("message")}
          value={message}
          onChange={({ target: { value } }) => setMessage(value)}
        />
      </div>
      <div className="gridRow">
        <Button
          color="primary"
          // disabled={!name || !email}
          className="sendButton"
          onClick={internalSubmit}
        >
          {getTextByComposedKeys("contact.sendMail.send")}
        </Button>
      </div>
    </div>
  )
}

export default ContactForm
