import React, { useState } from "react";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import ContactForm from "../ContactForm/ContactForm";
import "./ContactInfoStyles.css";

const ContactInfo = ({ handleMessageSubmit }) => {
  const [success, setSuccess] = useState(false);

  const internalSubmit = (info) =>
    handleMessageSubmit(info).then(() => {
      setSuccess(true);
    });

  return (
    <div className="formContainer">
      {success ? <h2>¡El mensaje se guardó con éxito!</h2> : " "}
      <h2>{getTextByComposedKeys("contact.sendMail.explanation")}</h2>
      <ContactForm handleSubmit={internalSubmit} />
    </div>
  );
};

export default ContactInfo;
