import React, { useEffect, useState } from "react"
import { Button, Input, Form, FormGroup, Label } from "reactstrap"
import { GrPowerReset } from "react-icons/gr"

import { TYPES, EVENT_CATEGORIES } from "../../../utils/constants/constants"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./CronogramaFilterStyles.css"

const availableDays = [
  { name: "Viernes", dayNumber: 1 },
  { name: "Sabado", dayNumber: 2 },
  { name: "Domingo", dayNumber: 3 },
]
const availableHours = [
  "00",
  "01",
  "02",
  "03",
  "04",
  "05",
  "06",
  "07",
  "08",
  "09",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15",
  "16",
  "17",
  "18",
  "19",
  "20",
  "21",
  "22",
  "23",
]
const availableMinutes = ["00", "15", "30", "45"]

const CronogramaFilter = ({ isGameProposal, handleSubmit }) => {
  const [showEverything, setShowEverything] = useState(false)
  const [type, setType] = useState(0)
  const [category, setCategory] = useState("")
  const [startHour, setStartHour] = useState("")
  const [startMinute, setStartMinutes] = useState("")
  const [endHour, setEndHour] = useState("")
  const [endMinute, setEndMinutes] = useState("")
  const [day, setDay] = useState("")

  useEffect(() => {
    if (showEverything) {
      console.info("xd")
    }
  }, [showEverything])

  const cleanFilters = () => {
    setShowEverything(false)
    setType(0)
    setCategory("")
    setStartHour("")
    setStartMinutes("")
    setEndHour("")
    setEndMinutes("")
    setDay("")

    handleSubmit({})
  }

  return (
    <Form className="filterForm">
      <FormGroup className="cronogramaFilterFormGroup">
        <Label for="types">{getTextByComposedKeys("cronograma.filter.day")}</Label>
        <Input
          value={day}
          onChange={({ target: { value } }) => setDay(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultDay")}
          </option>
          {availableDays.map(({ name, dayNumber }) => (
            <option key={dayNumber} value={dayNumber}>
              {name}
            </option>
          ))}
        </Input>
      </FormGroup>

      {!isGameProposal && (
        <FormGroup className="cronogramaFilterFormGroup">
          <Label for="types">{getTextByComposedKeys("cronograma.filter.type")}</Label>
          <Input
            value={type}
            onChange={({ target: { value } }) => setType(value)}
            type="select"
            name="types"
            id="exampleSelectMulti"
          >
            <option value={0}>
              {getTextByComposedKeys("cronograma.filter.defaultType")}
            </option>
            {TYPES.map((type) => (
              <option key={type} value={type}>
                {type}
              </option>
            ))}
          </Input>
        </FormGroup>
      )}
      {!isGameProposal && (
        <FormGroup className="cronogramaFilterFormGroup">
          <Label for="types">Categoría</Label>
          <Input
            value={category}
            onChange={({ target: { value } }) => setCategory(value)}
            type="select"
            name="types"
            id="exampleSelectMulti"
          >
            <option value={""}>Elegí categoría</option>
            {EVENT_CATEGORIES.map((cat) => (
              <option key={cat} type={cat} value={cat}>
                {cat}
              </option>
            ))}
          </Input>
        </FormGroup>
      )}

      <FormGroup className="flex space-x-2 md:max-w-xs cronogramaFilterFormGroup">
        <Button
          onClick={() =>
            handleSubmit({
              showEverything,
              startHour,
              startMinute,
              endHour,
              endMinute,
              type,
              day,
              category,
            })
          }
          className="cronogramaButton cronogramaFilterButton"
          color="success"
        >
          {getTextByComposedKeys("cronograma.filter.filter")}
        </Button>
        <Button
          onClick={cleanFilters}
          className="cronogramaButton cronogramaButtonReset"
          color="warning"
        >
          <GrPowerReset className="text-xl" />
        </Button>
      </FormGroup>
    </Form>
  )
}

export default CronogramaFilter
