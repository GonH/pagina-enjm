import React from "react"
import { Modal, ModalHeader, ModalBody } from "reactstrap"

import { getTextByComposedKeys } from "../../../utils/constants/text"
import { CURRENT_YEAR } from "../../../utils/constants/constants"
import "./CronogramaModalStyles.css"

const getEventLinkInfo = (link, isPastEvent) => {
  if (!link) {
    if (isPastEvent) return

    return <label className="hidden">{getTextByComposedKeys("cronograma.noLink")}</label>
  }

  const linkText = isPastEvent ? "watchEvent" : "takePart"

  return (
    <a href={link} target="_blank" className="btn-primary btn" rel="noopener noreferrer">
      {getTextByComposedKeys(`cronograma.${linkText}`)}
    </a>
  )
}

const getHora = ({ start, end, confirmedTime }) => {
  const endString = end ? ` - ${end}` : "";
  const confirmedString = confirmedTime ? "" : " estimada";

  return `Hora${confirmedString} ${start}${endString}`;
};

const CronogramaModal = ({ isOpen, toggle, event = {} }) => {
  const {
    name,
    description,
    start,
    end,
    currentEventType,
    type,
    link,
    eventParticipants = [],
    confirmedTime,
    inscriptionLink,
    inscriptionLinkAsLink,
    year,
  } = event;

  const isPastEvent = CURRENT_YEAR > year

  return (
    <Modal centered isOpen={!!isOpen} toggle={toggle}>
      <ModalHeader className={`${type}ModalHeader`} toggle={toggle}>
        <h2 className="w-full font-mono font-bold text-center text-7xl">{name}</h2>
        <div className="w-full eventBasicInfo ">
          {!isPastEvent && (
            <label>
              {getHora({ confirmedTime, start, end })}
            </label>
          )}
          <div className="mt-3 ">
          {getEventLinkInfo(link, isPastEvent)}
          </div>
        </div>
      </ModalHeader>
      <ModalBody className="w-full max-w-2xl px-12 mx-auto prose prose-xl prose-indigo">
        <div className={type}>
          <label className="event">
            {getTextByComposedKeys(`cronograma.types.${type}`)}
          </label>
        </div>
        <div className="w-full eventBasicInfo ">
          <div>
            {getEventLinkInfo(link, isPastEvent)}
            {!isPastEvent && (
              <label>{getHora({ confirmedTime, start, end })}</label>
            )}
          </div>
        </div>
        {type ? (
          <label>
            {type} para {currentEventType}
          </label>
        ) : (
          <label className="hidden" />
        )}
        <div>
          {!!eventParticipants.length && (
            <label className="eventParticipants">Participantes:</label>
          )}
          {eventParticipants.map((participant) => (
            <label key={participant}>{`• ${participant}`}</label>
          ))}
        </div>
        <label>
          {description &&
            description.split("\n").map((paragraph) => <label>{paragraph}</label>)}
        </label>
        {inscriptionLink && <iframe className="inscriptionForm" src={inscriptionLink} />}
        {inscriptionLinkAsLink && (
          <a
            className="inscriptionLinkContainer"
            href="https://docs.google.com/forms/d/e/1FAIpQLSdXuSWmsxfOG32wgKYViVxYmmt654bNCD5RaktEydMW8VlQ3A/viewform"
            target="_blank"
            rel="noopener noreferrer"
          >
            Inscribite acá
          </a>
        )}
      </ModalBody>
    </Modal>
  )
}

export default CronogramaModal
