import React, { useState } from "react";

import TwitchEmbed from "../../Atoms/TwitchEmbed/TwitchEmbed";
import YoutubeEmbed from "../../Atoms/YoutubeEmbed/YoutubeEmbed";
import "./CurrentStreamStyles.css";

const CurrentStream = ({
  customClass,
  youtubeLive,
  twitchLive,
}) => {
  const { innerWidth } = window;
  const [useOGOrder, setUseOGOrder] = useState(true);

  let style = {};

  if (innerWidth > 700) {
    const width = innerWidth * 0.6;
    const height = width * 0.55;

    style = {
      width,
      height,
    };
  }

  const getParams = () => {
    const { src, title } = youtubeLive || twitchLive;

    return { src, title, style, customClass, autoPlay: true };
  };

  const embedParams = getParams();

  if (youtubeLive && !twitchLive) return <YoutubeEmbed {...embedParams} />;
  if (!youtubeLive) return <TwitchEmbed {...embedParams} />;

  const toggleOrder = () => setUseOGOrder(!useOGOrder);

  const renderEmbed = ({ title, src }) => {
    const params = {
      src,
      title,
      style,
      customClass,
      autoPlay: true,
    };

    return title === "Youtube" ? (
      <YoutubeEmbed {...params} />
    ) : (
      <TwitchEmbed {...params} />
    );
  };

  const renderToggleButton = ({ title }) => {
    const streamType = title.toLowerCase();

    return (
    <button className={`${streamType}Button`} onClick={toggleOrder}>
      Ver vivo de {streamType}
    </button>
  )};

  const liveArray = useOGOrder
    ? [youtubeLive, twitchLive]
    : [twitchLive, youtubeLive];

  return (
    <div className="currentLiveContainer currentstreamContainer liveGrid">
      {renderEmbed(liveArray[0])}
      {renderToggleButton(liveArray[1])}
    </div>
  );
};

export default CurrentStream;
