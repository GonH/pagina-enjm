import React from "react";

import "./CurrentYearNomineesStyles.css";

const Category = ({ name, nominees }) => (
  <div className="ponchoPrizeCategoryContainer">
    <h3>{name}</h3>
    <ul>
      {nominees.map((nominee) => (
        <li key={nominee}>{nominee}</li>
      ))}
    </ul>
  </div>
);

const RenderNominees = ({ nominees }) => (
  <div className="nomineesTypeContainer">
    {Object.keys(nominees).map((key, index) => (
      <Category
        key={key}
        name={key}
        nominees={nominees[key]}
        useWhite={index % 3 === 1}
      />
    ))}
  </div>
);

const CurrentYearNominees = ({ nominees }) => {
  return (
    <div>
      <RenderNominees nominees={nominees} />
    </div>
  );
};

export default CurrentYearNominees;
