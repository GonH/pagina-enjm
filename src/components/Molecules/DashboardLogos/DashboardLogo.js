import React from "react";

import "./DashboardLogoStyles.css";

const getLink = (link) => {
  if(!link) return window.location.href;

  return link.includes("https://") || link.includes("http://") ? link : `https://${link}`;
};

const DashboardLogo = ({
  team,
  image,
  link,
  className,
  noBackground,
}) => {
  const parsedLink = getLink(link);

  return (
    <a
      className={className}
      href={parsedLink}
      target="_blank"
      rel="noopener noreferrer"
    >
      <img
        className={`dashboardLogo ${noBackground ? "" : "needsBackground"}`}
        src={image}
        alt={`Logo de ${team}, que redirige a ${parsedLink}`}
      />
    </a>
  );
};

export default DashboardLogo;
