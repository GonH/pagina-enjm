import React, { useState } from "react";
import { Collapse } from "reactstrap";

import EventList from "../EventList/EventList";
import "./DayContainerStyles.css";

const daysDict = {
  "1": "Viernes 20",
  "2": "Sábado 21",
  "3": "Domingo 22",
};

const daysDict2 = {
  "4": "Jueves 4",
  "5": "Viernes 5",
  "6": "Sábado 6",
  "7": "Domingo 7"
};

const getTitleClass = (isOpen) => {
  return isOpen ? "dayTitleContainer active" : "dayTitleContainer";
};

const getDefaultCollapseState = (dayId = 1) => {
  const dayByDayId = [20, 21, 22];

  const now = new Date();
  now.setHours(now.getHours() - 3);
  const [, , day] = now
    .toISOString()
    .split("T")[0]
    .split("-");

  return day <= dayByDayId[dayId - 1];
};

const getDayText = () => window.location.pathname.includes("archivo") ? daysDict : daysDict2;

const DayContainer = ({ day, dayInfo, eventSelect, isGameProposal }) => {
  const [isOpen, setIsOpen] = useState(getDefaultCollapseState(day));

  const toggleCollapse = () => setIsOpen(!isOpen);

  return (
    <div className="customCronogramaDayContainer ">
      <div onClick={toggleCollapse} className={getTitleClass(isOpen)}>
        <h2>{getDayText()[day]}</h2>
        <div
          className={`fas ${
            isOpen ? "fa-chevron-up" : "fa-chevron-down"
          } fa-2x`}
        />
      </div>
      <Collapse isOpen={isOpen}>
        <div className="customCronogramaTypeContainer">
          <EventList
            events={dayInfo}
            eventSelect={eventSelect}
            isGameProposal={isGameProposal}
          />
        </div>
      </Collapse>
    </div>
  );
};

export default DayContainer;
