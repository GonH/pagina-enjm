import React from "react";

import IndividualEvent from "../../Atoms/IndividualElement/IndividualEvent";
import "./EventListStyles.css";

const EventList = ({ events, eventSelect, isGameProposal }) => (
  <div className="eventTypeContainer">
    <ol>
      {events.map(({ _id, type, ...rest }) => (
        <IndividualEvent
          type={type}
          key={_id}
          event={rest}
          eventSelect={eventSelect}
          isGameProposal={isGameProposal}
        />
      ))}
    </ol>
  </div>
);

export default EventList;
