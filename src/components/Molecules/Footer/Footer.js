import React from "react"

import { getTextByComposedKeys } from "../../../utils/constants/text"
import {
  DISCORD_LINK,
  FB_LINK,
  TW_LINK,
  IG_LINK,
} from "../../../utils/constants/constants"
import "./FooterStyles.css"

const activities = [
  "cronograma",
  "ponchoPrizes",
]

const info = ["home", "about", "team", "sponsors", "contact"]
const networks = ["discord", "facebook", "instagram", "twitter"]

const logoByNetwork = {
  discord: DISCORD_LINK,
  facebook: FB_LINK,
  instagram: IG_LINK,
  twitter: TW_LINK,
}

const Footer = () => {
  return (
    <footer id="main-footer">
      <div className="footerContainer footer-container">
        <div>
          <h3>Actividades</h3>
          <ul className="list">
            {activities.map((activityName) => (
              <li key={activityName}>
                <a href={getTextByComposedKeys(`header.links.${activityName}`)}>
                  {getTextByComposedKeys(`header.text.${activityName}`)}
                </a>
              </li>
            ))}
          </ul>
        </div>
        <div>
          <h3>Información</h3>
          <ul className="list">
            {info.map((activityName) => (
              <li key={activityName}>
                <a href={getTextByComposedKeys(`header.links.${activityName}`)}>
                  {getTextByComposedKeys(`header.text.${activityName}`)}
                </a>
              </li>
            ))}
          </ul>
        </div>
        <div className="social">
          {networks.map((network) => (
            <a key={network} target="_blank" href={logoByNetwork[network]}>
              <i className={`fab fa-${network}`} />
            </a>
          ))}
        </div>
      </div>
      <div className="w-full py-3 bg-gray-800">
        <p className="font-mono text-3xl text-center">10 ENJM - La historia cuenta un juego</p>
      </div>
    </footer>
  )
}

export default Footer
