import React from "react";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";

import "./GalleryCarouselStyles.css";

const GalleryCarousel = ({
  allSources,
  setCurrentIndex,
  onClick,
  slideInterval = 10000,
}) => {
  const items = allSources.map(({ image }) => ({
    original: image,
    thumbnailClass: "imageGalleryThumbnail",
  }));

  const galleryProps = {
    showFullscreenButton: false,
    slideInterval,
    items,
    onBeforeSlide: setCurrentIndex,
    onClick,
  };

  return <ImageGallery autoplay showBullets {...galleryProps} />;
};

export default GalleryCarousel;
