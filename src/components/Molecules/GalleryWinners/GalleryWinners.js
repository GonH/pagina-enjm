import React from "react"
import "./GalleryWinnersStyles.css"
import { SRLWrapper } from "simple-react-lightbox"
import { LazyLoadImage } from "react-lazy-load-image-component"
import "react-lazy-load-image-component/src/effects/blur.css"
const GalleryWinners = ({ winners, title }) => (
  <div className="galleryWinnersSegmenContainer">
    <h2 className="text-3xl">
      Estos son los {winners.length} nominados para la competencia de {title}
    </h2>
    <div className="galleryWinnersContainer">
      {winners.map(({ src, name, author }) => (
        <div className="galleryWinnersItem">
          <div className="galleryWinnersItemImage">
            <LazyLoadImage
              className="object-fill object-center w-full h-full "
              alt={`${name}, finalista del torneo de ${title}, por ${author}`}
              effect="blur"
              src={src}
            />
          </div>
          <div className="galleryWinnersItemMeta">
            <h3>{name}</h3>
            {author && <p>{`${author}`}</p>}
          </div>
        </div>
      ))}
    </div>
  </div>
)

export default GalleryWinners
