import React, { useState } from "react";

import GameInfo from "../GameInfo/GameInfo";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./GameStyles.css";

const Game = ({ game }) => {
  const { name, playerCount, playTime, image } = game;

  const [showGame, setShowGame] = useState(false);

  const toggle = () => setShowGame(!showGame);

  return (
    <div className="gameContainer">
      <div className="gameRow" onClick={toggle}>
        <h3>{name}</h3>
        {!showGame && (
          <div>
            <div className="labelTitle">
              {getTextByComposedKeys("gameLibrary.modal.players")}
            </div>
            <label>{playerCount}</label>
          </div>
        )}
        {!showGame && (
          <div>
            <div className="labelTitle">
              {getTextByComposedKeys("gameLibrary.modal.playTime")}
            </div>
            <label>{playTime}</label>
          </div>
        )}
        {!showGame && <img src={image} alt={name}/>}
      </div>
      <GameInfo game={game} showGame={showGame} toggle={toggle} />
    </div>
  );
};

export default Game;
