import React from "react";
import { Collapse, Card, CardBody } from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./GameInfoStyles.css";

const GameInfo = ({
  game: {
    title,
    description,
    platform,
    publisher,
    playerCount,
    playTime,
    gameType,
    image,
  },
  showGame,
}) => {
  return (
    <Collapse className="gameInfoContainer" isOpen={showGame}>
      <Card>
        <CardBody>
          <div className={`${gameType}Game`}>
            <label className="gameTypeLabel">{gameType}</label>
          </div>
          <div className="basicGameInfo">
            <div className="titleValueCombo">
              <div className="labelTitle">
                {getTextByComposedKeys("gameLibrary.modal.players")}
              </div>
              <label>{playerCount}</label>
            </div>
            <div className="titleValueCombo">
              <div className="labelTitle">
                {getTextByComposedKeys("gameLibrary.modal.playTime")}
              </div>
              <label>{playTime}</label>
            </div>
            <div className="titleValueCombo">
              <div className="labelTitle">
                {getTextByComposedKeys("gameLibrary.modal.editorial")}
              </div>
              <label>{publisher}</label>
            </div>
          </div>
          <div className="extraGameInfo">
            <div className="extraGameInfoContent descriptionContainer">
              <div className="platformCombo">
                <div className="labelTitle">
                  {getTextByComposedKeys(
                    `gameLibrary.modal.${
                      platform && platform.length > 1
                        ? "platformPlural"
                        : "platformSingular"
                    }`
                  )}
                </div>
                {platform.map((platform) => (
                  <a href={platform} target="_blank" rel="noopener noreferrer">
                    {platform}
                  </a>
                ))}
              </div>
              <label className="gameDescription">{description}</label>
            </div>
            <div className="extraGameInfoContent">
              <img className="gameImage" src={image} alt={title} />
            </div>
          </div>
        </CardBody>
      </Card>
    </Collapse>
  );
};

export default GameInfo;
