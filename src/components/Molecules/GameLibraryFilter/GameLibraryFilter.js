import React, { useState } from "react";
import { Button, Input, Form, FormGroup, Label } from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./GameLibraryFilterStyles.css";

const gameCategories = [
  { value: "party", name: "Party" },
  { value: "memoria", name: "Memoria" },
  { value: "roles", name: "Roles Ocultos" },
];

const getPlaceholder = (key) =>
  getTextByComposedKeys(`gameLibrary.placeholders.${key}`);

const GameLibraryFilter = ({ handleSubmit }) => {
  const [minPlayerNumber, setMinPlayerNumber] = useState();
  const [maxPlayerNumber, setMaxPlayerNumber] = useState();
  const [minPlayTime, setMinPlayTime] = useState();
  const [maxPlayTime, setMaxPlayTime] = useState();
  const [gameType, setGameType] = useState();

  return (
    <Form className="gameFilterForm">
      <FormGroup className="gamesFilterFormGroup">
        <Label for="datetime">
          {getTextByComposedKeys("gameLibrary.filter.playerCount")}
        </Label>
        <div className="gameDatetimeFilter">
          <Input
            className=""
            type="number"
            placeholder={getPlaceholder("minPlayerNumber")}
            value={minPlayerNumber}
            onChange={({ target: { value } }) => setMinPlayerNumber(value)}
          />
          <Input
            className=""
            type="number"
            placeholder={getPlaceholder("maxPlayerNumber")}
            value={maxPlayerNumber}
            onChange={({ target: { value } }) => setMaxPlayerNumber(value)}
          />
        </div>
      </FormGroup>
      <FormGroup className="gamesFilterFormGroup">
        <Label for="datetime2">
          {getTextByComposedKeys("gameLibrary.filter.playTime")}
        </Label>
        <div className="gameDatetimeFilter">
          <Input
            className=""
            type="number"
            placeholder={getPlaceholder("minPlayTime")}
            value={minPlayTime}
            onChange={({ target: { value } }) => setMinPlayTime(value)}
          />
          <Input
            className=""
            type="number"
            placeholder={getPlaceholder("maxPlayTime")}
            value={maxPlayTime}
            onChange={({ target: { value } }) => setMaxPlayTime(value)}
          />
        </div>
      </FormGroup>
      <FormGroup className="gamesFilterFormGroup">
        <Label for="types">
          {getTextByComposedKeys("gameLibrary.filter.type")}
        </Label>
        <Input
          value={gameType}
          onChange={({ target: { value } }) => setGameType(value)}
          type="select"
          name="types"
          id="exampleSelectMulti"
        >
          <option value={0}>
            {getTextByComposedKeys("gameLibrary.filter.defaultType")}
          </option>
          {gameCategories.map(({ name, value }) => (
            <option value={value}>{name}</option>
          ))}
        </Input>
      </FormGroup>
      <FormGroup className="gamesFilterFormGroup">
        <Button
          onClick={() =>
            handleSubmit({
              minPlayerNumber,
              maxPlayerNumber,
              minPlayTime,
              maxPlayTime,
              gameType,
            })
          }
          className="cronogramaButton"
          color="success"
        >
          {getTextByComposedKeys("gameLibrary.filter.filter")}
        </Button>
      </FormGroup>
    </Form>
  );
};

export default GameLibraryFilter;
