import React from "react"
import { Link, NavLink } from "react-router-dom"
import "./HeaderStyles.css"
import "./Headroom.css"
import Headroom from "react-headroom"
import HomeEnjmLogo from "../../../images/enjm10/logo512.png"

const Header = () => {
  return (
    <div>
      <Headroom disableInlineStyles >
        <Link
          to="/"
          className="fixed z-50 flex header-logo hover:no-underline top-2 left-4"
        >
          <img alt="ENJM Logo" className="w-24 h-24 " src={HomeEnjmLogo} />
          <div className="hidden pt-1 pl-3 font-mono text-4xl font-bold text-gray-700 no-underline lg:flex ">
            <div>LA HISTORIA CUENTA UN JUEGO</div>
            <div className="hidden pt-1 font-sans text-2xl font-normal text-indigo-600 no-underline xl:block logoLegend">
              4, 5, 6 y 7 de Noviembre. Casa de la Cultura, Alte. Brown
            </div>
          </div>
        </Link>
        <header id="header" className="bg-white bg-opacity-90 backdrop-blur-lg">
          <div className="hidden mx-auto lg:pr-36 lg:flex">
            <nav className="items-center justify-end w-full lg:flex">
              {routes.map((route, i) => {
                return (
                  <NavLink
                    key={i}
                    alt={route.ariaLabel}
                    title={route.ariaLabel}
                    aria-label={route.ariaLabel}
                    to={route.slug}
                  >
                    {route.title}
                  </NavLink>
                )
              })}
            </nav>
          </div>
        </header>
      </Headroom>
    </div>
  )
}

export default Header

const routes = [
  {
    title: "Cronograma",
    slug: "/cronograma/",
    ariaLabel: "Ver cronograma",
  },
  {
    title: "Acerca del enjm",
    slug: "/acerca-del-enjm/",
    ariaLabel: "Ver Acerca del enjm",
  },
  {
    title: "Sponsors",
    slug: "/sponsors/",
    ariaLabel: "Ver Sponsors",
  },
  {
    title: "Tiendas",
    slug: "/tiendas/",
    ariaLabel: "Ver Tiendas",
  },
]
