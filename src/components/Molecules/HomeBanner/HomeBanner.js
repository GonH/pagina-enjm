import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import GalleryCarousel from "..//GalleryCarousel/GalleryCarousel";
import Team from "../../../images/Equipo.png";
import Galleries from "../../../images/Muestras.png";
import PonchoPrizes from "../../../images/Poncho.png";
import Shops from "../../../images/Tiendas.png";
import RightNow from "../../../images/Vivos.png";

// import Cronograma from "../../../images/CronogramaBanner.png";
// import GameLibrary from "../../../images/LudotecaBanner.png";
// import Sponsors from "../../../images/SponsorsBanner.png";

const HomeBanner = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const history = useHistory();
  const tabs = [
    // { link: "cronograma", image: Cronograma },
    { link: "galleries", image: Galleries },
    // { link: "sponsors", image: Sponsors },
    // { link: "gameLibrary", image: GameLibrary },
    { link: "shops", image: Shops },
    { link: "ponchoPrizes", image: PonchoPrizes },
    { link: "team", image: Team },
    // { link: "currentThings", image: RightNow },
  ];

  const redirectFromBannerImage = () => {
    const { link } = tabs[currentIndex];

    history.push(getTextByComposedKeys(`header.links.${link}`));
  };

  return (
    <GalleryCarousel
      allSources={tabs}
      setCurrentIndex={setCurrentIndex}
      onClick={redirectFromBannerImage}
      slideInterval={10000}
    />
  );
};

export default HomeBanner;
