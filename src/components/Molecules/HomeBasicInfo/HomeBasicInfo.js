import React, { useState } from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import JoinLinks from "../JoinLinks/JoinLinks";
import "./HomeBasicInfoStyles.css";

// TODO add real link to join xd
const HomeBasicInfo = ({ title, where, when }) => {
  const [currentModal, setCurrentModal] = useState("");

  const closeModal = () => setCurrentModal("");

  return (
    <div className="introText">
      {currentModal && (
        <Modal centered isOpen={currentModal} toggle={closeModal}>
          <ModalHeader toggle={closeModal}>
            <h2>{getTextByComposedKeys(`home.join.${currentModal}.title`)}</h2>
          </ModalHeader>
          <ModalBody>
            <div className="joinModalBody">
              <a
                className="alreadyKnow"
                href="AAAAAAAAAAAA"
                target="_blank"
                rel="noopener noreferrer"
              >
                {getTextByComposedKeys("home.join.alreadyKnow")}
              </a>
              <a className="dontKnow" href={`/tutorial/${currentModal}`}>
                {getTextByComposedKeys("home.join.dontKnow")}
              </a>
            </div>
          </ModalBody>
        </Modal>
      )}
      <label className="basicInfoTitle">{title}</label>
      <label className="basicInfoPlace">{where}</label>
      <label className="basicInfoTime">{when}</label>
      <div className="linksContainer">
        <JoinLinks openModal={setCurrentModal} />
      </div>
    </div>
  );
};

export default HomeBasicInfo;
