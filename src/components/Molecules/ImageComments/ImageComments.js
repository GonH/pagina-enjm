import React from "react";
import { Button, Input } from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./ImageCommentsStyles.css";

const ImageComments = ({
  comments,
  handleCommentSubmit,
  loadingSubmit,
  newComment,
  setNewComment,
}) => (
  <div className="commentsContainer">
    <h3>{getTextByComposedKeys("galleries.specificImage.comments")}</h3>
    <div className="commentsBundle">
      {comments.map((comment) => (
        <div className="comment">
          {comment.split("\n").map((paragraph) => (
            <label>{paragraph}</label>
          ))}
        </div>
      ))}
    </div>
    <Input
      id="newComment"
      value={newComment}
      type="textarea"
      disabled={loadingSubmit}
      className="newComment"
      onChange={({ target: { value } }) => setNewComment(value)}
      placeholder={getTextByComposedKeys(
        "galleries.specificImage.newCommentPlaceholder"
      )}
    />
    <Button
      className="submitNewComment"
      color="success"
      onClick={() => handleCommentSubmit(newComment)}
    >
      {getTextByComposedKeys("galleries.specificImage.submitComment")}
    </Button>
  </div>
);

export default ImageComments;
