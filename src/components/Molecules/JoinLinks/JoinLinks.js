import React from "react";
import { Button } from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";

const howToParticipate = {
  gamingPlace: "https://discord.gg/DKCghf",
  live:
    "https://www.youtube.com/results?search_query=encuentrto+nacional+de+juegos+de+mesa",
  meetings: "https://zoom.com",
};

const JoinLinks = ({ openModal }) => {
  const links = Object.keys(howToParticipate).map((key) => (
    <Button
      color="success"
      className="basicInfoLink"
      onClick={() => openModal(key)}
    >
      {getTextByComposedKeys(`home.link.${key}`)}
    </Button>
  ));

  return links;
};

export default JoinLinks;
