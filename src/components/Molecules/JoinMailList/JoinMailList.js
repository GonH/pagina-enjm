import React, { useState } from "react"
import { Button } from "reactstrap"

import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./JoinMailListStyles.css"

const getPlaceholder = (key) => getTextByComposedKeys(`contact.sendMail.${key}`)

const JoinMailList = ({ handleSubmit }) => {
  const [email, setEmail] = useState("")
  const [success, setSuccess] = useState(false)

  const internalSubmit = () => {
    handleSubmit(email).then(() => {
      setEmail("")
      setSuccess(true)
    })
  }

  return (
    <div className="joinMailListContainer">
      {success ? <h2>¡Te suscribiste con éxito!</h2> : <br />}
      <h2>{getTextByComposedKeys("contact.joinMailList.explanation")}</h2>
      <input
        className="halfColInput right"
        placeholder={getPlaceholder("email")}
        value={email}
        onChange={({ target: { value } }) => setEmail(value)}
      />
      <Button
        //  disabled={!email}
        color="success"
        onClick={internalSubmit}
      >
        {getTextByComposedKeys("contact.joinMailList.join")}
      </Button>
    </div>
  )
}

export default JoinMailList
