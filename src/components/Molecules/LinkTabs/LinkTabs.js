import React, { useState } from "react"
import { Link, useLocation, useHistory } from "react-router-dom"
import { ButtonDropdown, DropdownToggle, DropdownMenu } from "reactstrap"

import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./LinkTabsStyles.css"

const checkIsSelected = (pathname, tabLink) =>
  tabLink === "/" ? pathname === tabLink : pathname.includes(tabLink)

const generateLinkByKey = ({ key, pathname, redirect, extraOnClick, closeMenu }) => {
  const tabLink = getTextByComposedKeys(`header.links.${key}`)
  const isSelected = checkIsSelected(pathname, tabLink)

  return (
    <li
      key={key}
      className={`headerTab ${isSelected ? "selectedTab" : "unselectedTab"}`}
      onClick={() => {
        redirect(tabLink)
        extraOnClick()
        closeMenu()
      }}
    >
      <Link to={tabLink}>{getTextByComposedKeys(`header.text.${key}`)}</Link>
    </li>
  )
}

const LinkTabs = ({ extraOnClick = () => {}, onlyDropDown, blacklist }) => {
  const [menuIsOpen, setMenuIsOpen] = useState(false)
  const toggleMenu = () => setMenuIsOpen(!menuIsOpen)
  const closeMenu = () => setMenuIsOpen(false)

  const { pathname } = useLocation()
  const history = useHistory()

  const redirect = (newPath) => {
    history.push(newPath)
    window.scrollTo(0, 0)
    setMenuIsOpen(false)
  }

  const rawTabs = onlyDropDown
    ? [
        "home",
        "cronograma",
        "gameProposals",
        "about",
        // "gameLibrary",
        "sponsors",
        "shops",
        "pastEvents",
        "contact",
        "ponchoPrizes",
      ]
    : [
        "home",
        "cronograma",
        "gameProposals",
        "about",
        // "gameLibrary",
        "galleries",
      ]

  const tabs = rawTabs.filter((tab) => !blacklist.includes(tab))

  const rawAlwaysInMenuTabs = [
    "team",
    "sponsors",
    "shops",
    "publishers",
    "pastEvents",
    "previousIterations",
    "currentThings",
    "contact",
    "ponchoPrizes",
    "tutorial",
  ]

  const alwaysInMenuTabs = rawAlwaysInMenuTabs.filter((tab) => !blacklist.includes(tab))

  return (
    <ul className="tabs">
      {tabs.map((key) =>
        generateLinkByKey({ key, pathname, redirect, extraOnClick, closeMenu })
      )}
      {!onlyDropDown && !!alwaysInMenuTabs.length && (
        // Cambiar este li por otro elem y arreglar los estilos
        <li>
          {alwaysInMenuTabs.map((key) =>
            generateLinkByKey({
              key,
              pathname,
              redirect,
              extraOnClick,
              toggleMenu,
              closeMenu,
            })
          )}
        </li>
      )}
    </ul>
  )
}

export default LinkTabs
