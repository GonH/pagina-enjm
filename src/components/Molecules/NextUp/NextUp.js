import React, { useEffect, useState } from "react";

import { getNextUp } from "../../../apiInteractions/apiCalls";
import TwitchLogo from "../../../images/TwitchLogo.png";
import YTLogo from "../../../images/YTLogo.png";
import "./NextUpStyles.css";

const NextUp = () => {
  const [nextInfo, setNextInfo] = useState([]);
  const [isYoutube, setIsYoutube] = useState(true);

  const handleInfoUpdate = () => {
    getNextUp().then(({ events, isYoutube: isYT }) => {
      setNextInfo(events);
      setIsYoutube(isYT);
    });
  };

  useEffect(() => {
    handleInfoUpdate();
    const intervalId = window.setInterval(handleInfoUpdate, 10000);

    return () => window.clearInterval(intervalId);
  }, []);

  return (
    <div className="nextUpContainer">
      {nextInfo.map((info) => (
        <h1 className="nextUpInfo">{info}</h1>
      ))}
      <img src={isYoutube ? YTLogo : TwitchLogo} alt="logo" />
    </div>
  );
};

export default NextUp;
