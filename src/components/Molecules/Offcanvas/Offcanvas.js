import React, { useState } from "react";
import { slide as Menu } from "react-burger-menu";
import "./Offcanvas.css";
import Navigation from "./OffcanvasNavigation";

const Offcanvas = () => {
  const [openMenu, setOpenMenu] = useState(false);

  return (
    <Menu
      width={240}
      right
      isOpen={openMenu}
      onStateChange={({ isOpen }) => setOpenMenu(isOpen)}
    >
      <Navigation closeMenu={() => setOpenMenu(false)} />
    </Menu>
  );
};

export default Offcanvas;
