import React from "react"
import { Link } from "react-router-dom"
const Navigation = ({ closeMenu }) => (
  <nav className="flex flex-col w-full">
    <div className="flex flex-col w-full">
      {routesFeatured.map((route, i) => {
        return (
          <Link
            key={i}
            onClick={closeMenu}
            alt={route.ariaLabel}
            title={route.ariaLabel}
            aria-label={route.ariaLabel}
            to={route.slug}
            className="my-3 font-sans text-xl tracking-widest text-white cursor-pointer hover:text-indigo-100"
          >
            {route.title}
          </Link>
        )
      })}
    </div>
    {routesSecondary.map((route, i) => {
      return (
        <Link
          key={i}
          onClick={closeMenu}
          alt={route.ariaLabel}
          title={route.ariaLabel}
          aria-label={route.ariaLabel}
          to={route.slug}
          className="my-3 font-sans text-xl tracking-widest text-white cursor-pointer hover:text-indigo-100"
        >
          {route.title}
        </Link>
      )
    })}
  </nav>
)

export default Navigation

const routesFeatured = [
  {
    title: "Inicio",
    slug: "/",
    ariaLabel: "Inicio",
  },
  {
    title: "Cronograma",
    slug: "/cronograma/",
    ariaLabel: "Ver cronograma",
  },
  {
    title: "Acerca del enjm",
    slug: "/acerca-del-enjm/",
    ariaLabel: "Ver Acerca del enjm",
  },
]

const routesSecondary = [
  {
    title: "El Equipo",
    slug: "/el-equipo/",
    ariaLabel: "Ver al Equipo",
  },
  {
    title: "Sponsors",
    slug: "/sponsors/",
    ariaLabel: "Ver Sponsors",
  },
  {
    title: "Tiendas",
    slug: "/tiendas/",
    ariaLabel: "Ver Tiendas",
  },
  {
    title: "Premios poncho",
    slug: "/premios-poncho/",
    ariaLabel: "Ver premios-poncho",
  },
  {
    title: "Eventos Anteriores",
    slug: "/cronograma-archivo",
    ariaLabel: "Ver Eventos Anteriores",
  },
  {
    title: "Encuentros Anteriores",
    slug: "/encuentros-anteriores/",
    ariaLabel: "Ver Encuentros Anteriores",
  },

  {
    title: "Tutoriales",
    slug: "/tutoriales/",
    ariaLabel: "Ver tutoriales",
  },
  {
    title: "Contacto",
    slug: "/contacto/",
    ariaLabel: "Ver contacto",
  },
]
