import React from "react";
import { Button } from "reactstrap";

import YoutubeEmbed from "../../Atoms/YoutubeEmbed/YoutubeEmbed";
import TwitchEmbed from "../../Atoms/TwitchEmbed/TwitchEmbed";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./PastEventStyles.css";

const PastEvent = ({
  name = "Memes",
  when = "ayer",
  type = "type",
  description = "AAA",
  src = "https://player.twitch.tv/?channel=elgonboi",
  goBack,
}) => (
  <div className="container pastEventContainer">
    <div>
      <h1>{name}</h1>
      <div className="goBackLink">
        <Button color="warning" onClick={goBack}>
          {getTextByComposedKeys("previousEvents.goBack")}
        </Button>
      </div>
    </div>
    <div className="pastEventContent">
      {src.includes("youtube") ? (
        <YoutubeEmbed src={src} />
      ) : (
        <TwitchEmbed title={name} src={src} />
      )}
      <div className="pastEventTextInfo">
        <div>
          <label>{type}</label>
          <label>{when}</label>
        </div>
        <label>{description}</label>
      </div>
    </div>
  </div>
);
export default PastEvent;
