import React, { useEffect, useState } from "react"

import { Button, Input, Form, FormGroup, Label, Tooltip } from "reactstrap"
import { GrPowerReset } from "react-icons/gr"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./PonchoPrizeFilterStyles.css"

const PonchoPrizeFilter = ({
  years,
  categories,
  addYear,
  addCategory,
  handleSubmit,
  clearFilters,
}) => {
  const [tooltipOpen, setTooltipOpen] = useState(false)

  const toggle = () => setTooltipOpen(!tooltipOpen)
  return (
    <Form className="ponchoPrizesFilter">
      <FormGroup>
        <Label for="types">{getTextByComposedKeys("ponchoPrizes.filter.year")}</Label>
        <Input
          value={0}
          onChange={({ target: { value } }) => {
            if (value) addYear(value)
          }}
          type="select"
          name="types"
          id="exampleSelectMulti"
        >
          <option value={0}>
            {getTextByComposedKeys("ponchoPrizes.filter.defaultYear")}
          </option>
          {years.map((year) => (
            <option key={year} value={year}>
              {year}
            </option>
          ))}
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for="types">{getTextByComposedKeys("ponchoPrizes.filter.category")}</Label>
        <Input
          value={0}
          onChange={({ target: { value } }) => {
            if (value) addCategory(value)
          }}
          type="select"
          name="types"
          id="exampleSelectMulti"
        >
          <option value={0}>
            {getTextByComposedKeys("ponchoPrizes.filter.defaultCategory")}
          </option>
          {categories.map((category) => (
            <option key={category} value={category}>
              {category}
            </option>
          ))}
        </Input>
      </FormGroup>
      <FormGroup className="flex space-x-2">
        <Button onClick={handleSubmit} className="cronogramaButton" color="success">
          {getTextByComposedKeys("ponchoPrizes.filter.filter")}
        </Button>
        <Button
          onClick={clearFilters}
          className="cronogramaButton cronogramaButtonReset"
          color="warning"
          id="resetFilters"
        >
          <GrPowerReset className="text-xl" />
        </Button>
        <Tooltip
          placement="bottom"
          isOpen={tooltipOpen}
          target="resetFilters"
          toggle={toggle}
        >
          Reiniciar filtros
        </Tooltip>
      </FormGroup>
    </Form>
  )
}

export default PonchoPrizeFilter
