import React, { useState } from "react";
import { Button, Input, Form, FormGroup, Label } from "reactstrap";

import { TYPES } from "../../../utils/constants/constants";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./PreviousEventsFilterStyles.css";

const PreviousEventsFilter = ({ handleSubmit }) => {
  const [type, setType] = useState(0);

  return (
    <Form className="filterForm">
      <FormGroup>
        <Label for="types">
          {getTextByComposedKeys("cronograma.filter.type")}
        </Label>
        <Input
          value={type}
          onChange={({ target: { value } }) => setType(value)}
          type="select"
          name="types"
          id="exampleSelectMulti"
        >
          <option value={0}>
            {getTextByComposedKeys("cronograma.filter.defaultType")}
          </option>
          {TYPES.map((type) => (
            <option key={type} value={type}>
              {getTextByComposedKeys(`cronograma.types.${type}`)}
            </option>
          ))}
        </Input>
      </FormGroup>
      <FormGroup>
        <Button
          onClick={() =>
            handleSubmit({
              type,
            })
          }
          className="cronogramaButton"
          color="success"
        >
          {getTextByComposedKeys("cronograma.filter.filter")}
        </Button>
      </FormGroup>
    </Form>
  );
};

export default PreviousEventsFilter;
