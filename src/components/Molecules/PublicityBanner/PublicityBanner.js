import React, { useEffect, useState } from "react";

import Discount from "../../Atoms/Discount/Discount";

import "./PublicityBanner.css";

const PublicityBanner = ({ publicity, updateCurrentShopName }) => {
  const [index, setIndex] = useState(0);
  const [intervalId, setIntervalId] = useState();
  const defaultDuration = publicity.reduce(
    (accumulatedDuration, { duration }) =>
      duration ? Math.max(accumulatedDuration, duration) : accumulatedDuration,
    0
  ) || 10000;

  const isSinglePublicity = publicity.length === 1;

  const next = () => setIndex((index + 1) % publicity.length);

  const updgradeCurrentShop = () => updateCurrentShopName(publicity[index].link);

  useEffect(() => {
    if (isSinglePublicity) return updgradeCurrentShop();

    const { duration } = publicity[index];

    clearInterval(intervalId);
    const newId = setInterval(next, duration || defaultDuration);

    setIntervalId(newId);

    updgradeCurrentShop();
    return clearInterval(intervalId);
  }, [index]);

  const previous = () => setIndex(index ? index - 1 : publicity.length - 1);

  return (
    <div className="publicityBanner">
      <button onClick={previous}>{"<"}</button>
      <Discount {...publicity[index]} />
      <button onClick={next}>
        {">"}
      </button>
    </div>
  );
};

export default PublicityBanner;
