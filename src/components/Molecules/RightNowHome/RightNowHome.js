import React, { useState, useEffect } from "react";

import CurrentStream from "../../Molecules/CurrentStream/CurrentStream";
import { getRightNow } from "../../../apiInteractions/apiCalls";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./RightNowHomeStyles.css";

const RightNowHome = () => {
  const [youtubeLive, setYoutubeLive] = useState();
  const [twitchLive, setTwitchLive] = useState();

  useEffect(() => {
    getRightNow().then(({ twitchInfo, youtubeInfo }) => {
      setYoutubeLive(
        youtubeInfo && {
          src: youtubeInfo,
          title: "Youtube",
        }
      );
      setTwitchLive(twitchInfo && { src: twitchInfo, title: "Twitch" });
    });
  }, []);

  if (!(youtubeLive || twitchLive)) return <div />;

  return (
    <div className="container currentThingsContainer">
      <h2 className="currentThingsSubtitle">
        {getTextByComposedKeys("home.rightNow.subtitle")}
      </h2>
      <div className="rightNowHomeStreamContainer">
        <CurrentStream
          customClass="homeStream"
          youtubeLive={youtubeLive}
          twitchLive={twitchLive}
        />
      </div>
    </div>
  );
};

export default RightNowHome;
