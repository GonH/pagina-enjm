import React from "react";

import PlaceInfo from "../../Atoms/PlaceInfo/PlaceInfo";
import "./Sede.css";

const Sede = ({
  town,
  link,
  days,
  place,
  placeLink,
  timeframe,
  image,
  organizers,
  index,
}) => (
  <div className={`${index % 2 ? "white" : "blue"}Background sede`}>
    <h2>{town}</h2>
    <PlaceInfo place={place} placeLink={placeLink} />
    <label>
      {days}
      {timeframe ? " - " : ""}
      {timeframe}
    </label>
    <div>
      <h3>Organizan:</h3>
      <ul>
        {organizers.map((organizer) => (
          <li>- {organizer}</li>
        ))}
      </ul>
    </div>
  </div>
);

export default Sede;
