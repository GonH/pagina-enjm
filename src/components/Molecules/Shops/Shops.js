import React, { useEffect, useState } from "react";
import { Spinner } from "reactstrap";

import ImageDashboard from "../../Organisms/ImageDashboard/ImageDashboard";
import { getShops } from "../../../apiInteractions/apiCalls";
import "./ShopsStyles.css";

const Shops = ({ inHome }) => {
  const [shops, setShops] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getShops()
      .then(setShops)
      .finally(() => setLoading(false));
  }, []);

  const rows = !inHome && Math.ceil(shops.length / 3);

  return (
    <div className="shopsContainerMol">
      {loading && <Spinner color="dark" className="loadingParticipants" />}
      {!!shops.length && (
        <ImageDashboard
          infoList={shops}
          type="shops"
          className={`dashboard rows${rows}`}
        />
      )}
    </div>
  );
};

export default Shops;
