import React from 'react'

import { getLinkByComposedKeys } from '../../../utils/constants/links'

const SocialButtonIcon = ({ name, Svg }) => (<a
  href={getLinkByComposedKeys(`socialNetworks.${name}`)}
  target="_blank"
  rel="noopener noreferrer"
>
  <div className="socialButtonsIcon">
    <Svg />
  </div>
  <span>{name}</span>
</a>
)

export default SocialButtonIcon