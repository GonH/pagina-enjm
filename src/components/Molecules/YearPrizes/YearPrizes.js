import React, { useState } from "react"
import { Collapse } from "reactstrap"

import CategoryPrize from "../CategoryPrize/CategoryPrize"
import "./YearPrizesStyles.css"

const YearPrizes = ({ year, yearInfo }) => {
  const [isOpen, setIsOpen] = useState(true)
  const toggle = () => setIsOpen(!isOpen)

  return (
    <div className="ponchoPrizeCard">
      <h2 className="ponchoPrizeYear">{year}</h2>
      {/* <h2 onClick={toggle} className="ponchoPrizeYear">
        {year}
      </h2> */}
      <Collapse className="ponchoPrizeYearContainer" isOpen={isOpen}>
        {Object.keys(yearInfo).map((categoryName) => (
          <CategoryPrize
            key={categoryName}
            categoryName={categoryName}
            categoryInfo={yearInfo[categoryName]}
          />
        ))}
      </Collapse>
    </div>
  )
}

export default YearPrizes
