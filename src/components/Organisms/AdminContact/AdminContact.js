import React, { useState } from "react";

import {
  getInList,
  getNotJoined,
  markAsJoined,
} from "../../../apiInteractions/apiCalls";

const populateAnchorElement = (anchor, data) => {
  const contentType = "application/octet-stream";
  const blob = new Blob([data], { type: contentType });

  anchor.href = window.URL.createObjectURL(blob);
  anchor.target = "_blank";
  anchor.rel = "noopener noreferrer";
  anchor.style.display = "none";
  anchor.download = `Lista-mails-enjm-${
    new Date().toISOString().split(".")[0]
  }.csv`;
  anchor.dataset.downloadurl = [contentType, anchor.download, anchor.href].join(
    ":"
  );
};

const handleAnchorLifecycle = (anchor) => {
  document.body.appendChild(anchor);
  anchor.click();
  document.body.removeChild(anchor);
};

const markAllAsJoined = (password) => {
  getNotJoined().then((data) => {
    const idList = data.map(({ _id }) => _id);
    markAsJoined({ idList, password });
  });
};

const AdminContact = () => {
  const [isEmptyList, setIsEmptyList] = useState(false);
  const [password, setPassword] = useState("");

  const downloadCSV = (getter, password) => {
    const anchor = document.createElement("a");

    getter(password).then((data) => {
      const isEmpty = !data || !data.length;

      setIsEmptyList(isEmpty);

      if (isEmpty) return;

      const toDownload = data.map(({ email }) => email);

      populateAnchorElement(anchor, toDownload);
      handleAnchorLifecycle(anchor);
    });
  };

  return (
    <div className="container" style={{ margin: "5em" }}>
      <label> Contraseña</label>
      <input
        value={password}
        onChange={({ target: { value } }) => setPassword(value)}
      />
      {isEmptyList && (
        <label>La lista que acabas de pedir no tiene informacion</label>
      )}
      <button onClick={() => downloadCSV(getInList, password)}>
        Obtener lista completa
      </button>
      <button onClick={() => downloadCSV(getNotJoined, password)}>
        Obtener lista de gente no unida
      </button>
      <button onClick={() => markAllAsJoined(password)}>
        Todos estan unidos a la lista
      </button>
    </div>
  );
};

export default AdminContact;
