import React, { useEffect, useState } from "react";
import { Input } from "reactstrap";

import Dropzone from "../../Organisms/Dropzone/Dropzone";
import { getShops, saveDiscount } from "../../../apiInteractions/apiCalls";

import "./AdminDiscounts.css";

const AdminDiscounts = () => {
  const [password, setPassword] = useState();
  const [selectedShop, setSelectedShop] = useState("");
  const [shops, setShops] = useState([]);
  const [image, setImage] = useState();
  const [from, setFrom] = useState();
  const [to, setTo] = useState();
  const [activeDays, setActiveDays] = useState([]);

  const handleFirstChar = (value) => {
    const asNum = Number(value);

    if (!asNum) return "";

    if (asNum < 3) return value;

    return `0${asNum}:`;
  };

  const handleSecondChar = (value) => {
    const asNum = Number(value);

    if (!asNum) return value.slice(0, 1);

    const [first, second] = value.split(":").map(Number);

    if (first < 2) return `${asNum}:`;

    if (second > 3) return value.slice(0, 1);

    return `${asNum}:`;
  };

  const handleFourthChar = (value) => {
    const [hour, min] = value.split(":");

    const asNum = Number(min);

    if (!asNum) return `${hour}:`;

    if (asNum > 5) return `${hour}:0${min}`;

    return value;
  };

  const handleFifthChar = (value) => {
    const [hour, min] = value.split(":");

    const asNum = Number(min);

    if (!asNum) return `${hour}:${min.slice(0, 1)}`;

    return value;
  };

  const getNewTime = (value) => {
    const { length } = value;

    switch (length) {
      case 1:
        return handleFirstChar(value);
      case 2:
        return handleSecondChar(value);
      case 3:
        return value;
      case 4:
        return handleFourthChar(value);
      case 5:
        return handleFifthChar(value);
      default:
        return value.slice(0, 5);
    }
  };

  const changeTime = ({ target: { value } }, handleChange) => {
    handleChange(getNewTime(value));
  };

  useEffect(() => {
    getShops().then(setShops);
  }, []);

  const handleSubmit = () => {
    saveDiscount({
      shopId: selectedShop,
      image,
      password,
      from,
      to,
      activeDays,
    });
  };

  const handleDayChange = (toToggle) => {
    if (!activeDays.includes(toToggle))
      return setActiveDays([...activeDays, toToggle]);
    const newDays = activeDays.filter((day) => day !== toToggle);

    setActiveDays(newDays);
  };

  return (
    <div className="container discountainer" style={{ marginTop: "5em" }}>
      <div>
        <label>Contraseña</label>
        <input
          type="password"
          value={password}
          onChange={({ target: { value } }) => setPassword(value)}
        />
        <label>Desde</label>
        <input
          value={from}
          onChange={(ev) => changeTime(ev, setFrom)}
          placeholder="23:59"
        />
        <label>Hasta</label>
        <input
          value={to}
          onChange={(ev) => changeTime(ev, setTo)}
          placeholder="23:59"
        />
        <div className="dayContainer">
          <label className="AAAAAAAA" for="jueves">
            Jueves
          </label>
          <input
            className="AAAAAAAA"
            id="jueves"
            type="checkbox"
            value={activeDays.includes(4)}
            onChange={() => handleDayChange(4)}
          />
        </div>
        <div className="dayContainer">
          <label className="AAAAAAAA" for="viernes">
            Viernes
          </label>
          <input
            className="AAAAAAAA"
            id="viernes"
            type="checkbox"
            value={activeDays.includes(4)}
            onChange={() => handleDayChange(4)}
          />
        </div>
        <div className="dayContainer">
          <label className="AAAAAAAA" for="sabado">
            Sabado
          </label>
          <input
            className="AAAAAAAA"
            id="sabado"
            type="checkbox"
            value={activeDays.includes(4)}
            onChange={() => handleDayChange(4)}
          />
        </div>
        <div className="dayContainer">
          <label className="AAAAAAAA" for="domingo">
            Domingo
          </label>
          <input
            className="AAAAAAAA"
            id="domingo"
            type="checkbox"
            value={activeDays.includes(4)}
            onChange={() => handleDayChange(4)}
          />
        </div>
        <button onClick={handleSubmit}>Guardar descuento</button>
        <label>Elegi la tienda que propone este descuento</label>
        <Input
          value={selectedShop}
          onChange={({ target: { value } }) => setSelectedShop(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>Elegi una tienda</option>
          {shops.map(({ name, _id }) => (
            <option value={_id}>{name}</option>
          ))}
        </Input>
      </div>
      {selectedShop && (
        <div>
          <label>Imagen</label>
          <Dropzone image={image} setImage={setImage} setLoading={() => {}} />
        </div>
      )}
    </div>
  );
};

export default AdminDiscounts;
