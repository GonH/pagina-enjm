// edit evento

import React, { useEffect, useState } from "react";
import { Button, Input } from "reactstrap";
import { useParams } from "react-router-dom";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import {
  getEntities,
  updateEvent,
  getEntity,
} from "../../../apiInteractions/apiCalls";
import "./AdminEditFormStyles.css";

// tipo de evento
// Anuncios, Charla, Taller, Vivo, Presentacion de juego, Conversatorio, Torneo
const eventCategories = [
  { value: "Anuncio", name: "Anuncio" },
  { value: "Charla", name: "Charla" },
  { value: "Propuesta Lúdica", name: "Propuesta Lúdica" },
  { value: "Mesa Debate", name: "Mesa Debate" },
  { value: "Muestra", name: "Muestra" },
  { value: "Taller", name: "Taller" },
  { value: "Vivo", name: "Vivo" },
  { value: "Presentación de juego", name: "Presentación de juego" },
  { value: "Conversatorio", name: "Conversatorio" },
  { value: "Torneo", name: "Torneo" },
];

const eventTypes = [
  "Wargames",
  "Adultos Mayores",
  "Infantil",
  "Rol",
  "Docentes",
  "Editoriales",
  "Público Gral",
];
const getPlaceholder = (key) =>
  getTextByComposedKeys(`admin.placeholders.${key}`);

const getTitle = (key) => getTextByComposedKeys(`admin.text.${key}`);

// min max players
// min max time
/*
  Cosas subibles
  - Colectivos: Participant: { name, image, category, link }
  - Sponsors: Participant: { name, image, category, link, tier }
  - Shop: Participant: { name, image, category, link }
  - Juegos: Juego: { name, image, category, description, playerNumber, playTime, publisher, platform }
  - Eventos: Evento: { name, category, description }
*/
const AdminEditForm = () => {
  const { _id } = useParams();

  // Multiclass
  const [name, setName] = useState("");
  const [category, setCategory] = useState("party");
  const [link, setLink] = useState("");
  const [description, setDescription] = useState();
  const [errors, setErrors] = useState();
  const [saveSuccessfull, setSaveSuccessfull] = useState(false);
  const [password, setPassword] = useState("");

  // Event
  const [startHour, setStartHour] = useState("");
  const [startMinute, setStartMinutes] = useState("");
  const [endHour, setEndHour] = useState("");
  const [endMinute, setEndMinutes] = useState("");
  const [day, setDay] = useState("");
  const [eventParticipants, setEventParticipants] = useState([]);
  const [currentParticipant, setCurrentParticipant] = useState();
  const [currentEventType, setCurrentEventType] = useState("");
  const [inscriptionLink, setInscriptionLink] = useState("");

  // const [type, setType] = useState("event");
  const [selectableList, setSelectableList] = useState([]);
  const [selectedItem, setSelectedItem] = useState();
  const [fetchedItem, setFetchedItem] = useState();

  useEffect(() => {
    getEntities(`?collection=events`).then(setSelectableList);
  }, []);

  useEffect(() => {
    if (!selectedItem) return;

    console.info({ selectedItem });

    getEntity(`?collection=events&_id=${selectedItem}`).then(
      ({
        name: nam,
        type: ty,
        description: desc,
        end,
        start,
        day: d,
        eventParticipants: part,
        link: lonk,
      }) => {
        const [endH, endM] = end.split(":");
        const [startH, startM] = start.split(":");

        setName(nam);
        setCategory(ty);
        setDescription(desc);
        setEndHour(endH);
        setEndMinutes(endM);
        setStartHour(startH);
        setStartMinutes(startM);
        setDay(d);
        
        const parsedParts = part.map((value, index) => ({ value, index }))

        setEventParticipants(parsedParts);
        setLink(lonk);
        setCurrentEventType();
        setInscriptionLink();
      }
    );
  }, [selectedItem]);

  const availableHours = [
    "00",
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19",
    "20",
    "21",
    "22",
    "23",
  ];
  const availableMinutes = ["00", "15", "30", "45"];
  const availableDays = [
    { name: "Viernes", dayNumber: 1 },
    { name: "Sabado", dayNumber: 2 },
    { name: "Domingo", dayNumber: 3 },
  ];

  const saveParticipantEntry = () => {
    const { length } = eventParticipants;
    const id = length ? eventParticipants[length - 1] + 1 : 0;

    const newParticipants = [
      ...eventParticipants,
      { id, value: currentParticipant },
    ];

    setEventParticipants(newParticipants);
    setCurrentParticipant("");
  };

  const deleteParticipant = (toDelete) => {
    const newParticipants = eventParticipants.filter(
      ({ id }) => id !== toDelete
    );

    setEventParticipants(newParticipants);
  };

  const validateEvent = () => {
    let errorinos = false;

    errorinos = errorinos || !name;
    errorinos = errorinos || !category;
    errorinos = errorinos || !password;
    errorinos = errorinos || !startHour;
    errorinos = errorinos || !startMinute;
    errorinos = errorinos || !endHour;
    errorinos = errorinos || !endMinute;
    errorinos = errorinos || !day;

    return errorinos;
  };

  const getEventInfo = () => ({
    id: selectedItem,
    name,
    category,
    description,
    password,
    start: `${startHour}:${startMinute}`,
    end: `${endHour}:${endMinute}`,
    day,
    eventParticipants: eventParticipants.map(({ value }) => value),
    _id,
    currentEventType,
    currentEventType,
    inscriptionLink,
    link
  });

  const cleanUp = () => {
    setName("");
    setLink("");
    setDescription("");
    setErrors();
    setStartHour("");
    setStartMinutes("");
    setEndHour("");
    setEndMinutes("");
    setDay("");
  };

  const handleSend = () => {
    const errorinos = validateEvent();

    setErrors(errorinos);
    setSaveSuccessfull(false);

    if (errorinos) {
      return;
    }

    console.info("the info", getEventInfo());

    updateEvent(getEventInfo()).then(() => {
      cleanUp();
      setSaveSuccessfull(true);
    });
  };

  if (!selectedItem) {
    return (
      <div className="container adminEditFormContainer">
        <label>Seleccinoa el evento a editar</label>
        <Input
          value={selectedItem}
          onChange={({ target: { value } }) => setSelectedItem(value)}
          type="select"
          name="types"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultType")}
          </option>
          {selectableList.map(({ name, _id }) => (
            <option value={_id}>{name}</option>
          ))}
        </Input>
      </div>
    );
  }

  return (
    <div className="container adminEditFormContainer">
      <div className="adminEditFormContainer">
        <label>Seleccinoa el evento a editar</label>
        <Input
          value={selectedItem}
          onChange={({ target: { value } }) => setSelectedItem(value)}
          type="select"
          name="types"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultType")}
          </option>
          {selectableList.map(({ name, _id }) => (
            <option value={_id}>{name}</option>
          ))}
        </Input>
      </div>
      <Button onClick={handleSend}>Enviar información</Button>
      {errors && (
        <label className="errorLabel">Todos los campos son obligatorios</label>
      )}
      {saveSuccessfull && (
        <label className="successLabel">El guardado se hizo con éxito</label>
      )}
      <div>
        <label>{getTitle("eventTypes")}</label>
        <Input
          value={currentEventType}
          onChange={({ target: { value } }) => setCurrentEventType(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.eventTypes")}
          </option>
          {eventTypes.map((eventType) => (
            <option value={eventType}>{eventType}</option>
          ))}
        </Input>
      </div>
      <div>
        <label>Link de inscriupcion</label>
        <Input
          className=""
          placeholder="https://www.form.google.com/un-formulario-de-inscripcion"
          value={inscriptionLink}
          onChange={({ target: { value } }) => setInscriptionLink(value)}
        />
      </div>
      <div>
        <label>{getTitle("password")}</label>
        <Input
          type="password"
          name="password"
          id="examplePassword"
          value={password}
          onChange={({ target: { value } }) => setPassword(value)}
        />
      </div>
      <div>
        <label>{getTitle("category")}</label>
        <Input
          value={category}
          onChange={({ target: { value } }) => setCategory(value)}
          type="select"
          name="types"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultCategory")}
          </option>
          {eventCategories.map(({ value, name }) => (
            <option value={value}>{name}</option>
          ))}
        </Input>
      </div>
      <div>
        <label>{getTitle("name")}</label>
        <Input
          className=""
          placeholder={getPlaceholder("name")}
          value={name}
          onChange={({ target: { value } }) => setName(value)}
        />
      </div>
      <div>
        <label>{getTitle("liveLink")}</label>
        <Input
          className=""
          placeholder={getPlaceholder("link")}
          value={link}
          onChange={({ target: { value } }) => setLink(value)}
        />
      </div>
      <div>
        <label>{getTitle("description")}</label>
        <Input
          className=""
          type="textarea"
          placeholder={getPlaceholder("description")}
          value={description}
          onChange={({ target: { value } }) => setDescription(value)}
        />
      </div>
      <div>
        <label>{getTitle("participantTitle")}</label>
        <label>{getTitle("participantSubtitle")}</label>
        <div>
          {eventParticipants.map(({ id, value }) => (
            <button onClick={() => deleteParticipant(id)}>{value}</button>
          ))}
          <Input
            className=""
            placeholder={getPlaceholder("participant")}
            value={currentParticipant}
            onChange={({ target: { value } }) => setCurrentParticipant(value)}
          />
          <button onClick={saveParticipantEntry}>
            {getTitle("submitParticipants")}
          </button>
        </div>
      </div>
      <div>
        <label>{getTitle("day")}</label>
        <Input
          value={day}
          onChange={({ target: { value } }) => setDay(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultDay")}
          </option>
          {availableDays.map(({ name, dayNumber }) => (
            <option value={dayNumber}>{name}</option>
          ))}
        </Input>
      </div>
      <div>
        <label>{getTitle("startHour")}</label>
        <Input
          value={startHour}
          onChange={({ target: { value } }) => setStartHour(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultHour")}
          </option>
          {availableHours.map((hourOption) => (
            <option value={hourOption}>{hourOption}</option>
          ))}
        </Input>
      </div>
      <div>
        <label>{getTitle("startMinute")}</label>
        <Input
          value={startMinute}
          onChange={({ target: { value } }) => setStartMinutes(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultMinute")}
          </option>
          {availableMinutes.map((minuteOption) => (
            <option value={minuteOption}>{minuteOption}</option>
          ))}
        </Input>
      </div>
      <div>
        <label>{getTitle("endHour")}</label>
        <Input
          value={endHour}
          onChange={({ target: { value } }) => setEndHour(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultHour")}
          </option>
          {availableHours.map((hourOption) => (
            <option value={hourOption}>{hourOption}</option>
          ))}
        </Input>
      </div>
      <div>
        <label>{getTitle("endMinute")}</label>
        <Input
          value={endMinute}
          onChange={({ target: { value } }) => setEndMinutes(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultMinute")}
          </option>
          {availableMinutes.map((minuteOption) => (
            <option value={minuteOption}>{minuteOption}</option>
          ))}
        </Input>
      </div>
    </div>
  );
};

export default AdminEditForm;
