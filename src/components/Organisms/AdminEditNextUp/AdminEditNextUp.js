import React, { useEffect, useState } from "react";
import { CustomInput, Input } from "reactstrap";

import { updateNextUp } from "../../../apiInteractions/apiCalls";
import "./AdminEditNextUpStyles.css";

const AdminEditNextUp = () => {
  const [password, setPassword] = useState();
  const [events, setEvents] = useState([]);
  const [currentEvent, setCurrentEvent] = useState("");
  const [isYoutube, setIsYoutube] = useState(true);

  const toggle = () => setIsYoutube(!isYoutube);

  const saveParticipantEntry = () => {
    const { length } = events;
    const id = length ? events[length - 1] + 1 : 0;

    const newParticipants = [...events, { id, value: currentEvent }];

    setEvents(newParticipants);
    setCurrentEvent("");
  };

  const deleteParticipant = (toDelete) => {
    const newParticipants = events.filter(({ id }) => id !== toDelete);

    setEvents(newParticipants);
  };

  const handleSubmit = () => {
    updateNextUp({ events, isYoutube, password });
  };

  return (
    <div className="container nextUpFormContainer">
      <label>Contraseña</label>
      <Input
        type="password"
        name="password"
        id="examplePassword"
        value={password}
        onChange={({ target: { value } }) => setPassword(value)}
      />
      <label>Eventos a continuacion</label>
      <div>
        {events.map(({ id, value }) => (
          <button onClick={() => deleteParticipant(id)}>{value}</button>
        ))}
        <Input
          className=""
          placeholder="Un evento"
          value={currentEvent}
          onChange={({ target: { value } }) => setCurrentEvent(value)}
        />
        <button onClick={saveParticipantEntry}>Agregar evento</button>
      </div>
      <div className="nextUpFormSwitchContainer">
        <label>Twitch</label>
        <CustomInput
          type="switch"
          id="isYT"
          name="customSwitch"
          checked={isYoutube}
          onClick={toggle}
        />
        <label>Youtube</label>
      </div>
      <button onClick={handleSubmit}>Actualizar info</button>
    </div>
  );
};

export default AdminEditNextUp;
