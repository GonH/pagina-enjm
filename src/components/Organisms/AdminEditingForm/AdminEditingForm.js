import React, { useEffect, useState } from "react";

import { getEntities } from "../../../apiInteractions/apiCalls";

// const validTypes = [
//   { name: , value:  }
//   { name: , value:  }
//   { name: , value:  }
//   { name: , value:  }
//   { name: , value:  }
// ]

const AdminEditingForm = () => {
  // const [category, setCategory] = useState("")

  return (
    <div className="container">
      <div>
        <label>{getTitle("type")}</label>
        <Input
          value={type}
          onChange={({ target: { value } }) => setType(value)}
          type="select"
          name="types"
          id="exampleSelectMulti"
        >
          <option value={""}>
            {getTextByComposedKeys("admin.placeholders.defaultType")}
          </option>
          {types.map(({ name, value }) => (
            <option value={value}>{name}</option>
          ))}
        </Input>
      </div>
    </div>
  );
};
