import React, { useEffect, useState } from "react";
import { Button, Input } from "reactstrap";
import { useParams } from "react-router-dom";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import Dropzone from "../Dropzone/Dropzone";
import {
  saveGame,
  saveEvent,
  saveParticipant,
  editShop
} from "../../../apiInteractions/apiCalls";
import "./AdminFormStyles.css";

const tiers = [
  { value: "aus", name: "Auspiciante" },
  { value: "adh", name: "Adherente" },
  { value: "col", name: "Colaborador" },
];

const participantCategories = [
  { value: "team", name: "Colectvido de la organizacion" },
  { value: "shop", name: "Tienda" },
  { value: "sponsor", name: "Sponsor" },
  { value: "publisher", name: "Editorial" },
];

const gameCategories = [
  { value: "party", name: "Party" },
  { value: "memoria", name: "Memoria" },
  { value: "roles", name: "Roles Ocultos" },
];

// tipo de evento
// Anuncios, Charla, Taller, Vivo, Presentacion de juego, Conversatorio, Torneo
const eventCategories = [
  { value: "Anuncio", name: "Anuncio" },
  { value: "Charla", name: "Charla" },
  { value: "Propuesta Lúdica", name: "Propuesta Lúdica" },
  { value: "Mesa Debate", name: "Mesa Debate" },
  { value: "Muestra", name: "Muestra" },
  { value: "Taller", name: "Taller" },
  { value: "Vivo", name: "Vivo" },
  { value: "Rol", name: "Rol" },
  { value: "Entrevista", name: "Entrevista" },
  { value: "Presentación de juego", name: "Presentación de juego" },
  { value: "Presentación", name: "Presentación" },
  { value: "Conversatorio", name: "Conversatorio" },
  { value: "Torneo", name: "Torneo" },
];

const eventTypes = [
  "Wargames",
  "Adultos Mayores",
  "Infantil",
  "Rol",
  "Docentes",
  "Editoriales",
  "Público Gral",
];

const categoriesByType = {
  participant: participantCategories,
  game: gameCategories,
  event: eventCategories,
};

const types = [
  { value: "participant", name: "Participante" },
  { value: "game", name: "Juego" },
  { value: "event", name: "Evento" },
];

const getPlaceholder = (key) =>
  getTextByComposedKeys(`admin.placeholders.${key}`);

const getTitle = (key) => getTextByComposedKeys(`admin.text.${key}`);

const infoDict = {
  colectivo: { initialType: "participant", initialCategory: "team" },
  tienda: { initialType: "participant", initialCategory: "shop" },
  sponsor: { initialType: "participant", initialCategory: "sponsor" },
  juego: { initialType: "game", initialCategory: "party" },
  evento: { initialType: "event", initialCategory: "col" },
  editorial: { initialType: "participant", initialCategory: "publisher" },
};

// min max players
// min max time
/*
  Cosas subibles
  - Colectivos: Participant: { name, image, category, link }
  - Sponsors: Participant: { name, image, category, link, tier }
  - Shop: Participant: { name, image, category, link }
  - Juegos: Juego: { name, image, category, description, playerNumber, playTime, publisher, platform }
  - Eventos: Evento: { name, category, description }
*/
const AdminForm = () => {
  const { infoType } = useParams();
  const { initialType, initialCategory } = infoDict[infoType] || {};

  // Multiclass
  const [name, setName] = useState("");
  const [image, setImage] = useState();
  const [type, setType] = useState(initialType);
  const [category, setCategory] = useState(initialCategory);
  const [link, setLink] = useState("");
  const [description, setDescription] = useState();
  const [currentCategories, setCurrentCategories] = useState([]);
  const [errors, setErrors] = useState();
  const [saveSuccessfull, setSaveSuccessfull] = useState(false);
  const [password, setPassword] = useState("");

  // Participants
  const [tier, setTier] = useState("col");
  const [publisherType, setPublisherType] = useState("");

  // Games
  const [minPlayerNumber, setMinPlayerNumber] = useState();
  const [maxPlayerNumber, setMaxPlayerNumber] = useState();
  const [minPlayTime, setMinPlayTime] = useState();
  const [maxPlayTime, setMaxPlayTime] = useState();
  const [publisher, setPublisher] = useState();
  const [platform, setPlatform] = useState({});
  const [currentPlatform, setCurrentPlatform] = useState();

  // Event
  const [startHour, setStartHour] = useState("");
  const [startMinute, setStartMinutes] = useState("");
  const [endHour, setEndHour] = useState("");
  const [endMinute, setEndMinutes] = useState("");
  const [day, setDay] = useState("");
  const [eventParticipants, setEventParticipants] = useState([]);
  const [currentParticipant, setCurrentParticipant] = useState();
  const [currentEventType, setCurrentEventType] = useState("");
  const [inscriptionLink, setInscriptionLink] = useState("");
  const [place, setPlace] = useState("");
  const [sede, setSede] = useState("");

  const availableHours = [
    "00",
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19",
    "20",
    "21",
    "22",
    "23",
  ];
  const availableMinutes = ["00", "15", "30", "45"];
  const availableDays = [
    { name: "Jueves", dayNumber: "2021-11-04" },
    { name: "Viernes", dayNumber: "2021-11-05" },
    { name: "Sabado", dayNumber: "2021-11-06" },
    { name: "Domingo", dayNumber: "2021-11-07" },
  ];

  const savePlatform = () => {
    const newPlatforms = { ...platform };
    newPlatforms[currentPlatform] = currentPlatform;

    setCurrentPlatform("");
    setPlatform(newPlatforms);
  };

  const deletePlatform = (toDelete) => {
    const { [toDelete]: oldPlatform, ...rest } = platform;

    setPlatform(rest);
  };

  const saveParticipantEntry = () => {
    const { length } = eventParticipants;
    const id = length ? eventParticipants[length - 1] + 1 : 0;

    const newParticipants = [
      ...eventParticipants,
      { id, value: currentParticipant },
    ];

    setEventParticipants(newParticipants);
    setCurrentParticipant("");
  };

  const deleteParticipant = (toDelete) => {
    const newParticipants = eventParticipants.filter(
      ({ id }) => id !== toDelete
    );

    setEventParticipants(newParticipants);
  };

  useEffect(() => {
    console.info("type", type, "aaaa", categoriesByType[type]);

    setCurrentCategories(categoriesByType[type || "participant"]);
  }, [type]);

  const isParticipant = type === "participant";
  const isSponsor = type === "participant" && category === "sponsor";
  const isGame = type === "game";
  const isEvent = type === "event";
  const requiredField = " - Este campo es requerido";

  const validateParticipant = () => {
    let errorinos = false;
    errorinos = errorinos || !name;
    errorinos = errorinos || !image;
    errorinos =
      errorinos || !["sponsor", "shop", "team", "publisher"].includes(category);
    // errorinos = errorinos || !link;
    errorinos = errorinos || (isSponsor && !tier);
    errorinos = errorinos || !password;

    return errorinos;
  };

  const validateGame = () => {
    let errorinos = false;

    errorinos = errorinos || !name;
    errorinos = errorinos || !image;
    errorinos = errorinos || !["party", "memoria", "roles"].includes(category);
    errorinos = errorinos || !minPlayerNumber;
    errorinos = errorinos || !minPlayTime;
    errorinos = errorinos || !publisher;
    // errorinos = errorinos || !platform;
    errorinos = errorinos || !password;

    return errorinos;
  };

  const validateEvent = () => {
    let errorinos = false;

    errorinos = errorinos || !name;
    errorinos = errorinos || !category;
    errorinos = errorinos || !password;
    errorinos = errorinos || !startHour;
    errorinos = errorinos || !startMinute;
    // errorinos = errorinos || !endHour;
    // errorinos = errorinos || !endMinute;
    errorinos = errorinos || !day;

    return errorinos;
  };

  const getParticipantInfo = () => ({
    name,
    image,
    category,
    link,
    tier: category === "sponsor" && tier,
    password,
    publisherType
  });

  const getGameInfo = () => ({
    name,
    image,
    category,
    description,
    minPlayerNumber,
    maxPlayerNumber,
    minPlayTime,
    maxPlayTime,
    publisher,
    platform,
    password,
  });

  const getEventInfo = () => ({
    name,
    category,
    description,
    password,
    start: `${startHour}:${startMinute}`,
    end: `${endHour}:${endMinute}`,
    day,
    link,
    eventParticipants: eventParticipants.map(({ value }) => value),
    currentEventType,
    format: "Presencial",
    place,
    sede
  });

  const handlers = {
    participant: {
      infoGetter: getParticipantInfo,
      validator: validateParticipant,
      apiCall: saveParticipant,
    },
    game: {
      infoGetter: getGameInfo,
      validator: validateGame,
      apiCall: saveGame,
    },
    event: {
      infoGetter: getEventInfo,
      validator: validateEvent,
      apiCall: saveEvent,
    },
  };

  const cleanUp = () => {
    setName("");
    setImage();
    setLink("");
    setDescription("");
    setErrors();
    setTier("col");
    setMinPlayerNumber();
    setMaxPlayerNumber();
    setMinPlayTime();
    setMaxPlayTime();
    setPublisher();
    setPlatform([]);
    setStartHour("");
    setStartMinutes("");
    setEndHour("");
    setEndMinutes("");
    setDay("");

    if (!initialType) setType("participant");
    if (!initialCategory) setCategory("sponsor");
  };

  const handleSend = () => {
    // const { validator, infoGetter, apiCall } = handlers[type];
    // const errorinos = validator();

    // setErrors(errorinos);
    setSaveSuccessfull(false);

    // if (errorinos) {
    //   return;
    // }

    // console.info("the info", infoGetter());

    const body = {
      // disabled: false,
      // ...infoGetter(),
      image,
      _id: "5fb0bb8fdb4c600d472ddcfa",
      password
    };

    editShop(body).then(() => {
      cleanUp();
      setSaveSuccessfull(true);
    });
  };

  return (
    <div className="container adminFormContainer">
      <Button onClick={handleSend}>Enviar información</Button>
      {errors && (
        <label className="errorLabel">Todos los campos son obligatorios</label>
      )}
      {saveSuccessfull && (
        <label className="successLabel">El guardado se hizo con éxito</label>
      )}
      <div>
        <label>{getTitle("password")}</label>
        <Input
          type="password"
          name="password"
          id="examplePassword"
          value={password}
          onChange={({ target: { value } }) => setPassword(value)}
        />
      </div>
      {!initialType && (
        <div>
          <label>{getTitle("type")}</label>
          <Input
            value={type}
            onChange={({ target: { value } }) => setType(value)}
            type="select"
            name="types"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultType")}
            </option>
            {types.map(({ name, value }) => (
              <option value={value}>{name}</option>
            ))}
          </Input>
        </div>
      )}
      {initialType !== "participant" && (
        <div>
          <label>{getTitle("category")}</label>
          <Input
            value={category}
            onChange={({ target: { value } }) => setCategory(value)}
            type="select"
            name="types"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultCategory")}
            </option>
            {currentCategories.map(({ value, name }) => (
              <option value={value}>{name}</option>
            ))}
          </Input>
        </div>
      )}
      {category === "publisher" && (
        <div>
          <label>Es editorial o un juego?</label>
          <Input
            value={publisherType}
            onChange={({ target: { value } }) => setPublisherType(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>Elegi si es una editorial o un juego</option>
            <option value="publisher">Editorial</option>
            <option value="game">Juego</option>
          </Input>
        </div>
      )}
      {isEvent && (
        <div>
          <label>{getTitle("eventTypes")}</label>
          <Input
            value={currentEventType}
            onChange={({ target: { value } }) => setCurrentEventType(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.eventTypes")}
            </option>
            {eventTypes.map((eventType) => (
              <option value={eventType}>{eventType}</option>
            ))}
          </Input>
        </div>
      )}
      {isSponsor && (
        <div>
          <label>{getTitle("tier")}</label>
          <Input
            value={tier}
            onChange={({ target: { value } }) => setTier(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultTier")}
            </option>
            {tiers.map(({ name, value }) => (
              <option value={value}>{name}</option>
            ))}
          </Input>
        </div>
      )}
      <div>
        <label>{getTitle("name")}</label>
        <Input
          className=""
          placeholder={getPlaceholder("name")}
          value={name}
          onChange={({ target: { value } }) => setName(value)}
        />
      </div>
      {isEvent && (
        <div>
          <label>Link de inscriupcion</label>
          <Input
            className=""
            placeholder="https://www.form.google.com/un-formulario-de-inscripcion"
            value={inscriptionLink}
            onChange={({ target: { value } }) => setInscriptionLink(value)}
          />
        </div>
      )}
      {isEvent && (
        <div>
          <label>Lugar</label>
          <Input
            className=""
            placeholder="Donde?"
            value={place}
            onChange={({ target: { value } }) => setPlace(value)}
          />
        </div>
      )}
      {isEvent && (
        <div>
          <label>Sede</label>
          <Input
            className=""
            placeholder="Donde?"
            value={sede}
            onChange={({ target: { value } }) => setSede(value)}
          />
        </div>
      )}
      {!isGame && (
        <div>
          <label>{getTitle(isEvent ? "liveLink" : "link")}</label>
          <Input
            className=""
            placeholder={getPlaceholder("link")}
            value={link}
            onChange={({ target: { value } }) => setLink(value)}
          />
        </div>
      )}
      {!isParticipant && (
        <div>
          <label>{getTitle("description")}</label>
          <Input
            className=""
            type="textarea"
            placeholder={getPlaceholder("description")}
            value={description}
            onChange={({ target: { value } }) => setDescription(value)}
          />
        </div>
      )}
      {isGame && (
        <div>
          <label>{getTitle("minPlayerNumber")}</label>
          <Input
            className=""
            placeholder={getPlaceholder("minPlayerNumber")}
            value={minPlayerNumber}
            onChange={({ target: { value } }) => setMinPlayerNumber(value)}
          />
        </div>
      )}
      {isGame && (
        <div>
          <label>{getTitle("maxPlayerNumber")}</label>
          <Input
            className=""
            placeholder={getPlaceholder("maxPlayerNumber")}
            value={maxPlayerNumber}
            onChange={({ target: { value } }) => setMaxPlayerNumber(value)}
          />
        </div>
      )}
      {isGame && (
        <div>
          <label>{getTitle("minPlayTime")}</label>
          <Input
            className=""
            placeholder={getPlaceholder("minPlayTime")}
            value={minPlayTime}
            onChange={({ target: { value } }) => setMinPlayTime(value)}
          />
        </div>
      )}
      {isGame && (
        <div>
          <label>{getTitle("maxPlayTime")}</label>
          <Input
            className=""
            placeholder={getPlaceholder("maxPlayTime")}
            value={maxPlayTime}
            onChange={({ target: { value } }) => setMaxPlayTime(value)}
          />
        </div>
      )}
      {isGame && (
        <div>
          <label>{getTitle("publisher")}</label>
          <Input
            className=""
            placeholder={getPlaceholder("publisher")}
            value={publisher}
            onChange={({ target: { value } }) => setPublisher(value)}
          />
        </div>
      )}
      {isGame && (
        <div>
          <label>{getTitle("platformTitle")}</label>
          <label>{getTitle("platformSubtitle")}</label>
          <div>
            {Object.keys(platform).map((platformLink) => (
              <button onClick={() => deletePlatform(platformLink)}>
                {platformLink}
              </button>
            ))}
            <Input
              className=""
              placeholder={getPlaceholder("platform")}
              value={currentPlatform}
              onChange={({ target: { value } }) => setCurrentPlatform(value)}
            />
            <button onClick={savePlatform}>{getTitle("submitPlatform")}</button>
          </div>
        </div>
      )}
      {isEvent && (
        <div>
          <label>{getTitle("participantTitle")}</label>
          <label>{getTitle("participantSubtitle")}</label>
          <div>
            {eventParticipants.map(({ id, value }) => (
              <button onClick={() => deleteParticipant(id)}>{value}</button>
            ))}
            <Input
              className=""
              placeholder={getPlaceholder("participant")}
              value={currentParticipant}
              onChange={({ target: { value } }) => setCurrentParticipant(value)}
            />
            <button onClick={saveParticipantEntry}>
              {getTitle("submitParticipants")}
            </button>
          </div>
        </div>
      )}
      {!isEvent && (
        <div>
          <label>{getTitle("image")}</label>
          <Dropzone image={image} setImage={setImage} setLoading={() => {}} />
        </div>
      )}
      {isEvent && (
        <div>
          <label>{getTitle("day")}</label>
          <Input
            value={day}
            onChange={({ target: { value } }) => setDay(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultDay")}
            </option>
            {availableDays.map(({ name, dayNumber }) => (
              <option value={dayNumber}>{name}</option>
            ))}
          </Input>
        </div>
      )}
      {isEvent && (
        <div>
          <label>{getTitle("startHour")}</label>
          <Input
            value={startHour}
            onChange={({ target: { value } }) => setStartHour(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultHour")}
            </option>
            {availableHours.map((hourOption) => (
              <option value={hourOption}>{hourOption}</option>
            ))}
          </Input>
        </div>
      )}
      {isEvent && (
        <div>
          <label>{getTitle("startMinute")}</label>
          <Input
            value={startMinute}
            onChange={({ target: { value } }) => setStartMinutes(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultMinute")}
            </option>
            {availableMinutes.map((minuteOption) => (
              <option value={minuteOption}>{minuteOption}</option>
            ))}
          </Input>
        </div>
      )}
      {isEvent && (
        <div>
          <label>{getTitle("endHour")}</label>
          <Input
            value={endHour}
            onChange={({ target: { value } }) => setEndHour(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultHour")}
            </option>
            {availableHours.map((hourOption) => (
              <option value={hourOption}>{hourOption}</option>
            ))}
          </Input>
        </div>
      )}
      {isEvent && (
        <div>
          <label>{getTitle("endMinute")}</label>
          <Input
            value={endMinute}
            onChange={({ target: { value } }) => setEndMinutes(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultMinute")}
            </option>
            {availableMinutes.map((minuteOption) => (
              <option value={minuteOption}>{minuteOption}</option>
            ))}
          </Input>
        </div>
      )}
    </div>
  );
};

export default AdminForm;
