import React, { useEffect, useState } from "react";
import { Button, Input } from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import { updateEvent, getEvents } from "../../../apiInteractions/apiCalls";
import "./AdminFormStyles.css";

// tipo de evento
// Anuncios, Charla, Taller, Vivo, Presentacion de juego, Conversatorio, Torneo
const eventCategories = [
  { value: "Anuncio", name: "Anuncio" },
  { value: "Charla", name: "Charla" },
  { value: "Propuesta Lúdica", name: "Propuesta Lúdica" },
  { value: "Mesa Debate", name: "Mesa Debate" },
  { value: "Muestra", name: "Muestra" },
  { value: "Taller", name: "Taller" },
  { value: "Vivo", name: "Vivo" },
  { value: "Rol", name: "Rol" },
  { value: "Entrevista", name: "Entrevista" },
  { value: "Presentación de juego", name: "Presentación de juego" },
  { value: "Presentación", name: "Presentación" },
  { value: "Conversatorio", name: "Conversatorio" },
  { value: "Torneo", name: "Torneo" },
];

const eventTypes = [
  "Wargames",
  "Adultos Mayores",
  "Infantil",
  "Rol",
  "Docentes",
  "Editoriales",
  "Público Gral",
];

const getPlaceholder = (key) =>
  getTextByComposedKeys(`admin.placeholders.${key}`);

const getTitle = (key) => getTextByComposedKeys(`admin.text.${key}`);

const AdminForm = () => {
  const [events, setEvents] = useState([]);
  const [id, setId] = useState();
  const [name, setName] = useState("");
  const [link, setLink] = useState("");
  const [description, setDescription] = useState();
  const [errors, setErrors] = useState();
  const [saveSuccessfull, setSaveSuccessfull] = useState(false);
  const [password, setPassword] = useState("");
  const [startHour, setStartHour] = useState("");
  const [startMinute, setStartMinutes] = useState("");
  const [endHour, setEndHour] = useState("");
  const [endMinute, setEndMinutes] = useState("");
  const [day, setDay] = useState("");
  const [eventParticipants, setEventParticipants] = useState([]);
  const [currentParticipant, setCurrentParticipant] = useState();
  const [currentEventType, setCurrentEventType] = useState("");
  const [inscriptionLink, setInscriptionLink] = useState("");
  const [place, setPlace] = useState("");
  const [sede, setSede] = useState("");

  useEffect(() => {
    getEvents("?current=1").then(({ events: eventsResponse }) => {
      const eventerinos = [];
      Object.keys(eventsResponse).forEach((key) => {
        const withDay = eventsResponse[key].map(event => ({ ...event, day: key }));
        eventerinos.push(...withDay);
      });
      setEvents(eventerinos);
    });
  }, []);

  useEffect(() => {
    if (!id) return;

    const [currentEvent] = events.filter(({ _id }) => _id === id);

    const {
      name: nameOG,
      link: linkOG,
      description: descriptionOG,
      saveSuccessfull: saveSuccessfullOG,
      startHour: startHourOG,
      startMinute: startMinuteOG,
      endHour: endHourOG,
      endMinute: endMinuteOG,
      day: dayOG,
      eventParticipants: eventParticipantsOG,
      currentEventType: currentEventTypeOG,
      inscriptionLink: inscriptionLinkOG,
      place: placeOG,
      sede: sedeOG,
    } = currentEvent;

    console.info({dayOG  })

    setName(nameOG);
    setLink(linkOG);
    setDescription(descriptionOG);
    setSaveSuccessfull(saveSuccessfullOG);
    setStartHour(startHourOG);
    setStartMinutes(startMinuteOG);
    setEndHour(endHourOG);
    setEndMinutes(endMinuteOG);
    setDay(dayOG);
    setEventParticipants(eventParticipantsOG.map((value, index) => ({ id: index, value })));
    setCurrentEventType(currentEventTypeOG);
    setInscriptionLink(inscriptionLinkOG);
    setPlace(placeOG);
    setSede(sedeOG);
  }, [id]);

  const availableHours = [];

  for (let i = 1; i < 10; i++) availableHours.push(`0${i}`);
  for (let i = 10; i < 24; i++) availableHours.push(`${i}`);

  const availableMinutes = ["00", "15", "30", "45"];
  const availableDays = [
    { name: "Jueves", dayNumber: "4" },
    { name: "Viernes", dayNumber: "5" },
    { name: "Sabado", dayNumber: "6" },
    { name: "Domingo", dayNumber: "7" },
  ];

  const saveParticipantEntry = () => {
    const { length } = eventParticipants;
    const id = length ? eventParticipants[length - 1] + 1 : 0;

    const newParticipants = [
      ...eventParticipants,
      { id, value: currentParticipant },
    ];

    setEventParticipants(newParticipants);
    setCurrentParticipant("");
  };

  const deleteParticipant = (toDelete) => {
    const newParticipants = eventParticipants.filter(
      ({ id }) => id !== toDelete
    );

    setEventParticipants(newParticipants);
  };

  const isEvent = true;

  const validateEvent = () => {
    let errorinos = false;

    console.info({name,
      password,
      startHour,
      startMinute,
    day})

    errorinos = errorinos || !name;
    errorinos = errorinos || !password;
    errorinos = errorinos || !startHour;
    errorinos = errorinos || !startMinute;
    // errorinos = errorinos || !endHour;
    // errorinos = errorinos || !endMinute;
    errorinos = errorinos || !day;

    return errorinos;
  };

  const getEnd = () => {
    if (endHour && endMinute) return `${endHour}:${endMinute}`;
    if (endHour) return endHour;
    return '';
  }

  const getEventInfo = () => ({
    disabled: false,
    name,
    description,
    password,
    start: `${startHour}:${startMinute}`,
    end: getEnd,
    day,
    link,
    eventParticipants: eventParticipants.map(({ value }) => value),
    currentEventType,
    format: "Presencial",
    place,
    sede,
    id
  });

  const cleanUp = () => {
    setName("");
    setLink("");
    setDescription("");
    setErrors();
    setStartHour("");
    setStartMinutes("");
    setEndHour("");
    setEndMinutes("");
    setDay("");
  };

  const handleSend = () => {
    const errorinos = validateEvent();

    setErrors(errorinos);
    setSaveSuccessfull(false);

    if (errorinos) {
      return;
    }

    updateEvent(getEventInfo()).then(() => {
      cleanUp();
      setSaveSuccessfull(true);
    });
  };

  return (
    <div className="container adminFormContainer">
      <div>
        <label>Elegir evento a editar</label>
        <Input
          value={id}
          onChange={({ target: { value } }) => setId(value)}
          type="select"
          name="tiers"
          id="exampleSelectMulti"
        >
          <option value={""}>Elegi el evento que queres editar</option>
          {events.map(({ _id, name }) => (
            <option value={_id}>{name}</option>
          ))}
        </Input>
      </div>

      <Button onClick={handleSend}>Enviar información</Button>
      {errors && (
        <label className="errorLabel">Todos los campos son obligatorios</label>
      )}
      {saveSuccessfull && (
        <label className="successLabel">El guardado se hizo con éxito</label>
      )}
      {id && (
        <div>
          <label>{getTitle("password")}</label>
          <Input
            type="password"
            name="password"
            id="examplePassword"
            value={password}
            onChange={({ target: { value } }) => setPassword(value)}
          />
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("eventTypes")}</label>
          <Input
            value={currentEventType}
            onChange={({ target: { value } }) => setCurrentEventType(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.eventTypes")}
            </option>
            {eventTypes.map((eventType) => (
              <option value={eventType}>{eventType}</option>
            ))}
          </Input>
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("name")}</label>
          <Input
            className=""
            placeholder={getPlaceholder("name")}
            value={name}
            onChange={({ target: { value } }) => setName(value)}
          />
        </div>
      )}
      {id && (
        <div>
          <label>Link de inscripcion</label>
          <Input
            className=""
            placeholder="https://www.form.google.com/un-formulario-de-inscripcion"
            value={inscriptionLink}
            onChange={({ target: { value } }) => setInscriptionLink(value)}
          />
        </div>
      )}
      {id && (
        <div>
          <label>Lugar</label>
          <Input
            className=""
            placeholder="Donde?"
            value={place}
            onChange={({ target: { value } }) => setPlace(value)}
          />
        </div>
      )}
      {id && (
        <div>
          <label>Sede</label>
          <Input
            className=""
            placeholder="Donde?"
            value={sede}
            onChange={({ target: { value } }) => setSede(value)}
          />
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle(isEvent ? "liveLink" : "link")}</label>
          <Input
            className=""
            placeholder={getPlaceholder("link")}
            value={link}
            onChange={({ target: { value } }) => setLink(value)}
          />
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("description")}</label>
          <Input
            className=""
            type="textarea"
            placeholder={getPlaceholder("description")}
            value={description}
            onChange={({ target: { value } }) => setDescription(value)}
          />
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("participantTitle")}</label>
          <label>{getTitle("participantSubtitle")}</label>
          <div>
            {eventParticipants.map(({ id, value }) => (
              <button onClick={() => deleteParticipant(id)}>{value}</button>
            ))}
            <Input
              className=""
              placeholder={getPlaceholder("participant")}
              value={currentParticipant}
              onChange={({ target: { value } }) => setCurrentParticipant(value)}
            />
            <button onClick={saveParticipantEntry}>
              {getTitle("submitParticipants")}
            </button>
          </div>
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("day")}</label>
          <Input
            value={day}
            onChange={({ target: { value } }) => setDay(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultDay")}
            </option>
            {availableDays.map(({ name, dayNumber }) => (
              <option value={dayNumber}>{name}</option>
            ))}
          </Input>
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("startHour")}</label>
          <Input
            value={startHour}
            onChange={({ target: { value } }) => setStartHour(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultHour")}
            </option>
            {availableHours.map((hourOption) => (
              <option value={hourOption}>{hourOption}</option>
            ))}
          </Input>
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("startMinute")}</label>
          <Input
            value={startMinute}
            onChange={({ target: { value } }) => setStartMinutes(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultMinute")}
            </option>
            {availableMinutes.map((minuteOption) => (
              <option value={minuteOption}>{minuteOption}</option>
            ))}
          </Input>
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("endHour")}</label>
          <Input
            value={endHour}
            onChange={({ target: { value } }) => setEndHour(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultHour")}
            </option>
            {availableHours.map((hourOption) => (
              <option value={hourOption}>{hourOption}</option>
            ))}
          </Input>
        </div>
      )}
      {id && (
        <div>
          <label>{getTitle("endMinute")}</label>
          <Input
            value={endMinute}
            onChange={({ target: { value } }) => setEndMinutes(value)}
            type="select"
            name="tiers"
            id="exampleSelectMulti"
          >
            <option value={""}>
              {getTextByComposedKeys("admin.placeholders.defaultMinute")}
            </option>
            {availableMinutes.map((minuteOption) => (
              <option value={minuteOption}>{minuteOption}</option>
            ))}
          </Input>
        </div>
      )}
    </div>
  );
};

export default AdminForm;
