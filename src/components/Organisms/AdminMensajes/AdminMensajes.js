import React, { useEffect, useState } from "react";

import {
  getAllMessages,
  getUnread,
  markAsRead,
} from "../../../apiInteractions/apiCalls";
import "./AdminMensajesStyles.css";

const Message = ({
  message: { _id, email, name, topic, message, read },
  markMessage,
  password,
}) => {
  const [newlyRead, setNewlyRead] = useState(read);

  return (
    <div className="adminMessagesList">
      <h2>Email: {email}</h2>
      <h2>Nombre: {name}</h2>
      {read || (newlyRead && <label>Este mensaje ya fue leido</label>)}
      {topic && <div>Asunto: {topic}</div>}
      {message && (
        <div className="messagePerSe">
          Mensaje:
          <br />
          {message.split("\n").map((paragraph) => (
            <label>{paragraph}</label>
          ))}
        </div>
      )}
      {!(read || newlyRead) && (
        <button
          onClick={() => {
            if (!read)
              markMessage({ id: _id, password }).then(() => setNewlyRead(true));
          }}
        >
          Marcar como leido
        </button>
      )}
    </div>
  );
};

const AdminMensajes = () => {
  const [messages, setMessages] = useState([]);
  const [showAll, setShowAll] = useState(false);
  const [password, setPassword] = useState("");

  const fetchMessages = () => {
    if (password) {
      const handler = showAll ? getAllMessages : getUnread;

      handler(password).then(setMessages);
    }
  };

  useEffect(() => {
    fetchMessages();
  }, [showAll]);

  const toggle = () => setShowAll(!showAll);

  return (
    <div className="container adminMessagesContainer">
      <div className="passwordCombo">
        <label>Contraseña</label>
        <input
          type="password"
          value={password}
          onChange={({ target: { value } }) => setPassword(value)}
        />
      </div>
      <button onClick={fetchMessages}>
        Mostrar mensajes (tenes que tener puesta la contraseña)
      </button>
      <button onClick={toggle}>
        {showAll
          ? "Mostrar solo los que no fueron leidos"
          : "Mostrar todos los mensajes"}
      </button>
      <div>
        {messages.length ? (
          messages.map((message) => (
            <Message
              message={message}
              markMessage={markAsRead}
              password={password}
            />
          ))
        ) : (
          <label>No hay mensajes disponibles</label>
        )}
      </div>
    </div>
  );
};

export default AdminMensajes;
