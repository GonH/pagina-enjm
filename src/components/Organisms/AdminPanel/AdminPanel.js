import React, { useEffect, useState } from "react";
import { Input } from "reactstrap";

import { getRightNow, updateRightNow } from "../../../apiInteractions/apiCalls";
import LiveDataContainer from "./LiveDataContainer";
import "./AdminPanelStyles.css";

const AdminPanel = () => {
  const [youtubeSrc, setYoutubeSrc] = useState("");
  const [twitchSrc, setTwitchSrc] = useState("");
  const [enabledYoutube, setEnabledYoutube] = useState(false);
  const [enabledTwitch, setEnabledTwitch] = useState(false);
  const [youtubeLink, setYoutubeLink] = useState("");
  const [twitchLink, setTwitchLink] = useState("");
  const [success, setSuccess] = useState(false);
  const [password, setPassword] = useState("");

  useEffect(() => {
    getRightNow({}).then((response) => {
      console.info({ response });
      const { twitchInfo, youtubeInfo } = response;
      setYoutubeSrc(youtubeInfo.src);
      setTwitchSrc(twitchInfo.src);
      setEnabledYoutube(!!youtubeInfo.src);
      setEnabledTwitch(!!twitchInfo.src);
    });
  }, []);

  const updateYoutube = (link) => {
    const linkToSave = youtubeLink || link;
    const value = enabledYoutube ? linkToSave : "";
    setSuccess(false);

    updateRightNow({
      name: "youtube",
      value,
      enabled: enabledYoutube,
      password,
    }).then(() => {
      setSuccess(true);
      setYoutubeSrc(value);
      setYoutubeLink("");
    });
  };

  const updateTwitch = (link) => {
    const linkToSave = twitchLink || link;
    const value = enabledTwitch ? linkToSave : "";
    setSuccess(false);

    updateRightNow({
      name: "twitch",
      value,
      enabled: enabledTwitch,
      password,
    }).then(() => {
      setSuccess(true);
      setTwitchSrc(value);
      setTwitchLink("");
    });
  };

  const toggleYoutube = () => setEnabledYoutube(!enabledYoutube);
  const toggleTwitch = () => setEnabledTwitch(!enabledTwitch);

  return (
    <div className="container panelContainer">
      {success && <label>Los cambios se guardaron con exito</label>}
      <div>
        <label>Contraseña</label>
        <Input
          type="password"
          name="password"
          id="examplePassword"
          value={password}
          onChange={({ target: { value } }) => setPassword(value)}
        />
      </div>
      <LiveDataContainer
        name="youtube"
        submit={updateYoutube}
        src={youtubeSrc}
        enabled={enabledYoutube}
        toggle={toggleYoutube}
      />
      <LiveDataContainer
        name="twitch"
        submit={updateTwitch}
        src={twitchSrc}
        enabled={enabledTwitch}
        toggle={toggleTwitch}
      />
    </div>
  );
};

export default AdminPanel;
