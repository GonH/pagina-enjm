import React, { useState } from "react";
import { Button, CustomInput, Input } from "reactstrap";

const LiveDataContainer = ({
  name,
  submit,
  src,
  enabled,
  toggle,
}) => {
  const [innerLink, setInnerLink] = useState(src);

  return (
    <div className="platformContainer">
      <h1>{name}</h1>
      <div>
        <label>Deshabilitado</label>
        <CustomInput
          type="switch"
          id={name}
          name="customSwitch"
          checked={enabled}
          // onChange={toggle}
          onClick={toggle}
        />
        <label>Habilitado</label>
      </div>
      <label>{`Link actual: ${src}`}</label>
      <Input
        value={innerLink}
        onChange={({ target: { value } }) => setInnerLink(value)}
        placeholder={src}
      />
      <Button
        onClick={() => submit(innerLink)}
      >{`Actualizar informacion de ${name}`}</Button>
    </div>
  );
};

export default LiveDataContainer;
