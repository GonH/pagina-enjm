import React, { useState, useEffect } from "react";

import { getSedes, updateSede, createSede } from "../../../apiInteractions/apiCalls";
import Dropzone from "../Dropzone/Dropzone";

const NewSede = ({
  town = "",
  link = "",
  days = "",
  place = "",
  placeLink = "",
  timeframe = "",
  image,
  organizers = [],
  handleSubmit,
  _id
}) => {
  const [newTown, setNewTown] = useState(town);
  const [newLink, setNewLink] = useState(link);
  const [newDays, setNewDays] = useState(days);
  const [newPlace, setNewPlace] = useState(place);
  const [newPlaceLink, setNewPlaceLink] = useState(placeLink);
  const [newTimeframe, setNewTimeframe] = useState(timeframe);
  const [newImage, setNewImage] = useState(image);
  const [newOrganizers, setNewOrganizers] = useState(organizers);
  const [password, setPassword] = useState("");
  const [organizer, setOrganizer] = useState("");

  const removeName = (newName) => {
    const namesToKeep = setNewOrganizers.filter((name) => name !== newName);

    setNewOrganizers(namesToKeep);
  };

  const addName = (newName) => {
    const clonedOrganizers = [...newOrganizers];
    clonedOrganizers.push(newName);

    setNewOrganizers(clonedOrganizers);
  };

  return (
    <div>
      <div>
        <label>Ciudad</label>
        <input
          value={newTown}
          onChange={({ target: { value } }) => setNewTown(value)}
        />
      </div>
      <div>
        <label>Link</label>
        <input
          value={newLink}
          onChange={({ target: { value } }) => setNewLink(value)}
        />
      </div>
      <div>
        <label>Dias</label>
        <input
          value={newDays}
          onChange={({ target: { value } }) => setNewDays(value)}
        />
      </div>
      <div>
        <label>Lugar</label>
        <input
          value={newPlace}
          onChange={({ target: { value } }) => setNewPlace(value)}
        />
      </div>
      <div>
        <label>Link al lugar</label>
        <input
          value={newPlaceLink}
          onChange={({ target: { value } }) => setNewPlaceLink(value)}
        />
      </div>
      <div>
        <label>Horas</label>
        <input
          value={newTimeframe}
          onChange={({ target: { value } }) => setNewTimeframe(value)}
        />
      </div>
      <div>
        <label>Imagen</label>
        <Dropzone
          image={newImage}
          setImage={setNewImage}
          setLoading={() => {}}
        />
      </div>
      <div>
        <label>Organizan</label>
        <input
          value={organizer}
          onChange={({ target: { value } }) => setOrganizer(value)}
        />
        <ul>
          {newOrganizers.map((name) => (
            <li onClick={() => removeName(name)}>{name}</li>
          ))}
        </ul>
        <button onClick={() => addName(organizer)}>Agregar</button>
      </div>
      <div>
        <label>Contraseña</label>
        <input
          type="password"
          value={password}
          onChange={({ target: { value } }) => setPassword(value)}
        />
      </div>
      <button onClick={() => handleSubmit({
          town: newTown,
          link: newLink,
          days: newDays,
          place: newPlace,
          placeLink: newPlaceLink,
          timeframe: newTimeframe,
          image: newImage,
          organizers: newOrganizers,
          password,
          _id
      })}>Guardar</button>
    </div>
  );
};

const Sedes = () => {
  const [sedes, setSedes] = useState([]);
  const [update, setUpdate] = useState(false);
  const [selectedSede, setSelectedSede] = useState({});

  useEffect(() => {
    getSedes().then(setSedes);
  }, []);

  const handleSubmit = ({ _id, ...rest }) => {
    const endpoint = update ? updateSede : createSede;

    console.info('the body', { ...rest, id: _id });
    console.info('the meme', JSON.stringify({ ...rest, id: _id }));

    endpoint({ ...rest, id: _id });
  };

  return (
    <div>
      <button style={{ marginTop: '10em' }} onClick={() => setUpdate(!update)}>
        Clickea para pasar de {update ? "edicion" : "creacion"} a{" "}
        {!update ? "edicion" : "creacion"}
      </button>
      {update && (
        <select onChange={setSelectedSede}>
          {sedes.map(({ _id, town }) => (
            <option value={_id}>{town}</option>
          ))}
        </select>
      )}
      <NewSede {...selectedSede} handleSubmit={handleSubmit} />
    </div>
  );
};

export default Sedes;
