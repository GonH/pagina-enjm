import React from "react"

import ContactMail from "../../Atoms/ContactMail/ContactMail"
import ContactInfo from "../../Molecules/ContactInfo/ContactInfo"
import JoinMailList from "../../Molecules/JoinMailList/JoinMailList"
// import SocialNetworks from "../../Atoms/SocialNetworks/SocialNetworks";
import { saveMessage, joinList } from "../../../apiInteractions/apiCalls"
import "./ContactStyles.css"

const Contact = () => {
  const handleJoinListSubmit = (email) => joinList(JSON.stringify({ email }))

  const handleMessageSubmit = ({ email, name, topic, message }) =>
    saveMessage(JSON.stringify({ email, name, topic, message }))

  return (
    <div className="text-indigo-200 bg-fixed contactContainer pattern-diagonal-lines-lg contactWrapper">
      <div>
        <ContactMail />
      </div>
      <div>
        <ContactInfo handleMessageSubmit={handleMessageSubmit} />
      </div>
      <div className="text-indigo-300 bg-fixed bg-indigo-200 pattern-diagonal-lines-lg">
        <JoinMailList handleSubmit={handleJoinListSubmit} />
      </div>
    </div>
  )
}

export default Contact
