import React, { useEffect, useState } from "react";

import { getEvents } from "../../../apiInteractions/apiCalls";
import EventContainer from "../EventContainer/EventContainer";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import MexicanSchedule from "../../../mexicanSchedule";
import "./CronogramaStyles.css";

const BASE_QUERY_STRING = `?current=1`;

const Cronograma = () => {
  const [queryString, setQueryString] = useState(BASE_QUERY_STRING);
  const [startDate, setStartDate] = useState(new Date());
  const [selectedType, setSelectedType] = useState(0);
  const [selectedEvent, setSelectedEvent] = useState();
  const [events, setEvents] = useState({});
  const [types, setTypes] = useState([]);
  const [loading, setLoading] = useState(true);

  const info = getTextByComposedKeys("cronograma.subtitle")
    .split("\n")
    .map((info, index) => <h6 key={index}>{info}</h6>);

  useEffect(() => {
    getEvents(queryString)
      .then(({ events: eventsResponse, tipos }) => {
        setTypes(tipos);
        setEvents(eventsResponse);
      })
      .finally(() => setLoading(false));
  }, [queryString]);

  const enjmStart = new Date();
  enjmStart.setHours(0, 0, 0);
  const endDate = new Date();
  endDate.setDate(endDate.getDate() + 1);

  const handleFilterSubmit = ({ type, day, category }) => {
    let newQueryString = BASE_QUERY_STRING;

    if (type && type !== "0") newQueryString += `&type=${type}`;
    if (day) newQueryString += `&day=${day}`;
    if (category) newQueryString += `&currentEventType=${category}`;

    setQueryString(newQueryString);
  };

  const eventSelect = (event) => {
    setSelectedEvent(event);
  };

  const toggleModal = () => {
    setSelectedEvent();
  };

  const props = {
    info,
    handleFilterSubmit,
    eventSelect,
    toggleModal,
    startDate,
    endDate,
    selectedType,
    title: getTextByComposedKeys("cronograma.title"),
    selectedEvent,
    types,
    events,
    loading,
    extraSchedule: MexicanSchedule,
    previousYears: getTextByComposedKeys("cronograma.previousYears")
  };

  return <EventContainer className="container" isCronograma {...props} />;
};

export default Cronograma;
