import React, { useEffect, useState } from "react";

import { getGameProposals } from "../../../apiInteractions/apiCalls";
import EventContainer from "../EventContainer/EventContainer";
import { getTextByComposedKeys } from "../../../utils/constants/text";
// import "./CronogramaStyles.css";

const CronogramaLudico = () => {
  const [queryString, setQueryString] = useState();
  const [startDate, setStartDate] = useState(new Date());
  const [selectedType, setSelectedType] = useState(0);
  const [selectedEvent, setSelectedEvent] = useState();
  const [events, setEvents] = useState({});
  const [types, setTypes] = useState([]);
  const [loading, setLoading] = useState(true);

  const info = getTextByComposedKeys("gameProposal.subtitle")
    .split("\n")
    .map((info) => <h6 key={info}>{info}</h6>);

  useEffect(() => {
    const queryStringToUse = queryString || "";

    getGameProposals(queryStringToUse)
      .then(({ events: eventsResponse, tipos }) => {
        setTypes(tipos);
        setEvents(eventsResponse);
      })
      .finally(() => setLoading(false));
  }, [queryString]);

  const enjmStart = new Date();
  enjmStart.setHours(0, 0, 0);
  const endDate = new Date();
  endDate.setDate(endDate.getDate() + 1);

  const handleFilterSubmit = ({ type, day, category }) => {
    let newQueryString = `?iHopeNoOneSeesThis=1`;

    if (type && type !== "0") newQueryString += `&type=${type}`;
    if (day) newQueryString += `&day=${day}`;
    if (category) newQueryString += `&currentEventType=${category}`;

    setQueryString(newQueryString);
  };

  const eventSelect = (event) => {
    setSelectedEvent(event);
  };

  const toggleModal = () => {
    setSelectedEvent();
  };

  const props = {
    info,
    handleFilterSubmit,
    eventSelect,
    toggleModal,
    startDate,
    endDate,
    selectedType,
    title: getTextByComposedKeys("gameProposal.title"),
    selectedEvent,
    types,
    events,
    loading,
  };

  return <EventContainer isGameProposal isCronograma {...props} />;
};

export default CronogramaLudico;
