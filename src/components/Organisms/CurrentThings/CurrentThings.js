import React, { useState, useEffect } from "react";

import { openLinkOnNewTab } from "../../../utils/helpers/uglyHelpers";
import TwitchEmbed from "../../Atoms/TwitchEmbed/TwitchEmbed";
import YoutubeEmbed from "../../Atoms/YoutubeEmbed/YoutubeEmbed";
import { getRightNow } from "../../../apiInteractions/apiCalls";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import { getLinkByComposedKeys } from "../../../utils/constants/links";
import "./CurrentThings.css";

const getPaddingBottom = (youtubeInfo, twitchInfo, juegos) => {
  const existsYoutube = youtubeInfo && youtubeInfo.src;
  const existsTwitch = twitchInfo && twitchInfo.src;
  const { length } = juegos;

  const emForPadding =
    Number(!existsYoutube) * 5 +
    Number(!existsTwitch) * 5 +
    Number(!length) * 2;

  return `${emForPadding * 4}em`;
};

const CurrentThings = () => {
  const [currentGames, setCurrentGames] = useState([]);
  const [twitchProps, setTwitchProps] = useState();
  const [youtubeProps, setYoutubeProps] = useState();

  useEffect(() => {
    getRightNow().then(({ juegos = [], twitchInfo, youtubeInfo }) => {
      setCurrentGames(juegos);
      setTwitchProps(twitchInfo);
      setYoutubeProps(youtubeInfo);
    });
  }, []);

  const paddingBottom = getPaddingBottom(
    youtubeProps,
    twitchProps,
    currentGames
  );

  let style = {};
  const { innerWidth } = window;

  if (innerWidth > 700) {
    const width = innerWidth * 0.6;
    const height = width * 0.55;

    style = {
      width,
      height,
    };
  }

  return (
    <div className="container currentThingsContainer" style={{ paddingBottom }}>
      <h1>{getTextByComposedKeys("currentThings.title")}</h1>
      <label className="currentThingsSubtitle">
        {getTextByComposedKeys("currentThings.subtitle")}
      </label>
      {currentGames && !!currentGames.length && (
        // TODO add onclick stolen from my code in centricity
        <div
          onClick={() =>
            openLinkOnNewTab(getLinkByComposedKeys("platforms.discord"))
          }
          className="currentGamesContainer"
          id="gamesBeingPlayed"
        >
          <label className="currentGamesTitle">
            {getTextByComposedKeys("currentThings.games")}
          </label>
          <div className="currentGamesGrid">
            {currentGames.map((game) => (
              <label>{`• ${game}`}</label>
            ))}
          </div>
        </div>
      )}
      {youtubeProps && youtubeProps.src && (
        <div className="currentLiveContainer">
          <label>{getTextByComposedKeys("currentThings.youtube")}</label>
          <YoutubeEmbed style={style} {...youtubeProps} />
        </div>
      )}
      {twitchProps && twitchProps.src && (
        <div className="currentLiveContainer">
          <label>{getTextByComposedKeys("currentThings.twitch")}</label>
          <TwitchEmbed style={style} {...twitchProps} />
        </div>
      )}
    </div>
  );
};

export default CurrentThings;
