import React from "react";

import DayContainer from "../../Molecules/DayContainer/DayContainer";
import "./CustomCronogramaStyles.css";

const CustomCronograma = ({ cronograma, eventSelect, isGameProposal }) => (
  <div className="customCronogramaContainer">
    {Object.keys(cronograma).map((day) => (
      <DayContainer
        key={day}
        eventSelect={eventSelect}
        day={day}
        dayInfo={cronograma[day]}
        isGameProposal={isGameProposal}
      />
    ))}
  </div>
);

export default CustomCronograma;
