import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./Dropzone.css";

const fileReadHandler = (file, resolve, reject) => {
  const reader = new FileReader();

  reader.onabort = () => reject("aborted");
  reader.onerror = () => reject("error");
  reader.onload = () => {
    const binaryStr = reader.result;

    resolve(binaryStr);
  };

  reader.readAsDataURL(file);
};

const promisifyFileUpload = (file) =>
  new Promise((resolve, reject) => fileReadHandler(file, resolve, reject));

const Dropzone = ({ image, setImage, setLoading }) => {
  const onDrop = useCallback((validFiles, invalidFiles) => {
    if (invalidFiles && invalidFiles.length) return;

    const [file] = validFiles;

    setLoading(true);

    promisifyFileUpload(file)
      .then((fileAs64) => {
        setImage(fileAs64);
      })
      .finally(() => setLoading(false));
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "image/*",
    multiple: false,
  });

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      {
        <p
          className={`entryDropzone ${
            image ? "replacePicture" : "pickPicture"
          }`}
        >
          {getTextByComposedKeys(
            `galleries.sendForm.${
              image ? "pickAnotherOne" : "clickHereForFileUpload"
            }`
          )}
        </p>
      }
      {image && <img className="uploadedImage" src={image} alt="cargar imagen" />}
    </div>
  );
};

export default Dropzone;
