import React, { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import Picture from "../../../images/enjm1.jpg"

import MeetingEvents from "../../Atoms/MeetingEvents/MeetingEvents"
import { getEnjmInfo } from "../../../apiInteractions/apiCalls"
import Enjm1 from "../../../images/1ENJM.png"
import Enjm2 from "../../../images/2ENJM.png"
import Enjm3 from "../../../images/3ENJM.jpg"
import Enjm4 from "../../../images/4ENJM.png"
import Enjm5 from "../../../images/5ENJM.jpg"
import Enjm6 from "../../../images/6ENJM.jpg"
import Enjm7 from "../../../images/7ENJM.jpg"
import Enjm8 from "../../../images/8ENJM.jpg"
import "./EncounterStyles.css"
import { SRLWrapper } from "simple-react-lightbox"
import Fade from "react-reveal/Fade"

const images = [Enjm1, Enjm2, Enjm3, Enjm4, Enjm5, Enjm6, Enjm7, Enjm8]

const Encounter = () => {
  const [enjmInfo, setEnjmInfo] = useState({})
  const [loading, setLoading] = useState(true)
  const { meetingId } = useParams()

  useEffect(() => {
    getEnjmInfo(meetingId)
      .then(setEnjmInfo)
      .finally(() => setLoading(false))
  }, [])

  const {
    title,
    donde,
    dias,
    when,
    where,
    participantes,
    charlasTaller,
    espaciosDisponibles,
    clinicas,
    torneos,
    videoconferencias,
    conversatoriosMesaRedonda,
    presentaciones,
    juegosDeLaMente,
    metajuego,
  } = enjmInfo

  return (
    <div className="encounterContainer">
      <SRLWrapper>
        <div className="introContainer">
          <div className="introText">
            <label className="meetingInfoTitle">{title}</label>
            {donde && <label className="meetingInfo">{donde}</label>}
            {when && <label className="meetingInfo">{when}</label>}
            {dias && <label className="meetingInfo">Duración: {dias}</label>}
            {participantes && <label className="meetingInfo">{participantes}</label>}
          </div>
        </div>
        <Fade delay={200} duration={1200}>
          <img
            className="w-full max-w-xl mx-auto"
            src={images[meetingId - 1]}
            alt={`Encuentro numero ${meetingId}: ${title}, en ${where}`}
          />
        </Fade>
      </SRLWrapper>
      <div className="prose prose-xl meetingText">
        {charlasTaller && <MeetingEvents title="charlasTaller" content={charlasTaller} />}
        {espaciosDisponibles && (
          <MeetingEvents title="espaciosDisponibles" content={espaciosDisponibles} />
        )}
        {clinicas && <MeetingEvents title="clinicas" content={clinicas} />}
        {torneos && <MeetingEvents title="torneos" content={torneos} />}
        {videoconferencias && (
          <MeetingEvents title="videoconferencias" content={videoconferencias} />
        )}
        {conversatoriosMesaRedonda && (
          <MeetingEvents
            title="conversatoriosMesaRedonda"
            content={conversatoriosMesaRedonda}
          />
        )}
        {presentaciones && (
          <MeetingEvents title="presentaciones" content={presentaciones} />
        )}
        {juegosDeLaMente && (
          <MeetingEvents title="juegosDeLaMente" content={juegosDeLaMente} />
        )}
        {metajuego && <MeetingEvents title="metajuego" content={metajuego} />}
      </div>
    </div>
  )
}

export default Encounter
