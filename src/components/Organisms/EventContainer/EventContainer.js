import React from "react"
import { Spinner } from "reactstrap"
import { Alert } from "reactstrap"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import CustomCronograma from "../CustomCronograma/CustomCronograma"
import CronogramaModal from "../../Molecules/CronogramaModal/CronogramaModal"
import PreviousEventsFilter from "../../Molecules/PreviousEventsFilter/PreviousEventsFilter"
import CronogramaFilter from "../../Molecules/CronogramaFilter/CronogramaFilter"
import Fade from "react-reveal/Fade"
import AnchorLink from "react-anchor-link-smooth-scroll"
import { AiOutlineArrowDown } from "react-icons/ai"
import "./EventContainerStyles.css"

const EventContainer = ({
  isCronograma,
  title,
  info,
  handleFilterSubmit,
  eventSelect,
  toggleModal,
  selectedEvent,
  types,
  events,
  loading,
  isGameProposal,
  externalEvent,
  previousYears,
}) => (
  <div className=" cronogramaPage">
    <div className="flex flex-col items-center justify-center w-full px-4 py-48 text-indigo-700 bg-fixed bg-indigo-600 lg:py-12 heroTutorialsWrapper bg-opacity-90 pattern-diagonal-lines-lg">
      {externalEvent && (
        <a className="mb-12 btn btn-warning" href="cronograma">
          Volver al cronograma
        </a>
      )}
      <Fade bottom delay={200} duration={1200}>
        <h3 className="w-full max-w-5xl mb-6 font-mono text-6xl font-bold text-white uppercase md:text-center md:text-7xl ">
          {title}
        </h3>
      </Fade>
      <div className="w-full max-w-5xl mx-auto leading-relaxed text-white eventContainerInfoText">
        <Fade delay={600} duration={1200}>
          <div>{info}</div>
        </Fade>
      </div>

      <Fade bottom delay={1000} duration={1200}>
        <AnchorLink
          href="#filter"
          className="relative anchorLink"
        >
          <AiOutlineArrowDown className="mt-12 text-5xl text-white animate-pulse"/>
        </AnchorLink>
      </Fade>
    </div>
    <CronogramaModal isOpen={selectedEvent} toggle={toggleModal} event={selectedEvent} />
    {previousYears && (
      <Alert color="primary" className="flex justify-center w-full py-6 mb-0">
        <a
          className="btn btn-success"
          href={`${window.location.origin}/cronograma-archivo`}
        >
          {previousYears}
        </a>
      </Alert>
    )}
    <div id="filter" className="pt-12 bg-gray-100">
      {!!Object.keys(events).length && (
        <CustomCronograma
          eventSelect={eventSelect}
          cronograma={events}
          types={types}
          isGameProposal={isGameProposal}
        />
      )}
      {!Object.keys(events).length && (
        <div className="emptyEventContainer">
          {loading ? (
            <Spinner color="dark" className="loadingPonchos" />
          ) : (
            <h2 className="py-24 font-mono text-6xl font-bold text-center uppercase emptyEvents">
              {getTextByComposedKeys(
                `${isCronograma ? "cronograma" : "previousEvents"}.empty`
              )}
            </h2>
          )}
        </div>
      )}

      {previousYears && (
        <Alert color="primary" className="flex justify-center w-full py-6 mb-0">
          <a
            className="btn btn-success"
            href={`${window.location.origin}/cronograma-archivo`}
          >
            {previousYears}
          </a>
        </Alert>
      )}
    </div>
  </div>
)

export default EventContainer
