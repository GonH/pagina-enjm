import React from "react";
import { Table } from "reactstrap";
import _ from "lodash";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./EventTableStyles.css";

const HOUR_IN_MS = 60 * 60 * 1000;
const HOURS_PER_DAY = 24;

const getColumnNameByKey = (key) =>
  getTextByComposedKeys(`cronograma.types.${key}`);

const EventTable = ({ data, endDate, startDate, selectedType, eventSelect, types }) => {
  const columns = types.map(getColumnNameByKey);

  const totalRows = [];

  const getFullRows = () => {
    const enjmsEndDate = endDate.getTime() + 3 * HOUR_IN_MS;

    const nowTime = startDate.getTime();

    const halfHours = (enjmsEndDate - nowTime) / HOUR_IN_MS;
    const currentHour = startDate.getHours();
    let isFirstHalf = startDate.getMinutes() < 30;
    let i = currentHour;

    while (i < halfHours) {
      const parsedHour = `${i % HOURS_PER_DAY}:${isFirstHalf ? "00" : "30"}`;
      isFirstHalf = !isFirstHalf;
      totalRows.push(parsedHour);
      i += isFirstHalf;
    }
  };

  getFullRows();

  const fillRow = (rowType, rowInfo) =>
    _.find(rowInfo, ({ type }) => type === rowType) || {};

  const generateRow = (hour, groups) => {
    const group = groups[hour];

    const filledColumns = types.map((type) => {
      const row = fillRow(type, group);
      const { name } = row

      return !selectedType || type === selectedType ? (
        <td
          onClick={name ? () => eventSelect(row) : () => {}}
          className={name ? `${type}Col` : ""}
        >
          {name}
        </td>
      ) : (
        <td />
      );
    });

    return (
      <tr className="eventsRow">
        <th scope="row">{hour}</th>
        {filledColumns}
      </tr>
    );
  };

  const groupedData = _.groupBy(data, ({ when }) => {
    const whenAsDate = new Date(when);
    const minutes = whenAsDate.getMinutes() ? "30" : "00";
    const formattedDate = `${whenAsDate.getHours()}:${minutes}`

    return formattedDate;
  });

  const rows = totalRows.map((hour) => generateRow(hour, groupedData));
  // no me gusto que no hubiera consecuencias por fallar en el plan
  // me gusto que hayamos estado de acuerdo en hacer un plan y que uno del equpo haya propuesto mision
  // en algun futuro me gustaria que sin querer la mision de alguno choque con la de otro

  return (
    <div className="cronogramaTable">
      <Table>
        <thead>
          <tr>
            <th />
            {columns.map((column) => (
              <th className="columnName">{column}</th>
            ))}
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </div>
  );
};

export default EventTable;
