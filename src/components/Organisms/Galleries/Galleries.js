import React, { useEffect, useState } from "react"
import { Button } from "reactstrap"
import { useHistory } from "react-router-dom"
import { Spinner } from "reactstrap"

import { getABitFromEachGallery } from "../../../apiInteractions/apiCalls"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import GalleryCarousel from "../../Molecules/GalleryCarousel/GalleryCarousel"
import Fade from "react-reveal/Fade"

import "./GalleriesStyles.css"

const getCategoryName = (categoryName) => categoryName.split(" ").join("_")

const Galleries = () => {
  const [allSources, setAllSources] = useState([])
  const [currentIndex, setCurrentIndex] = useState(0)
  const [loading, setLoading] = useState(true)

  const history = useHistory()

  const redirectToGalleryCategoryPage = (categoryName) =>
    history.push(`/galerias/${getCategoryName(categoryName)}`)

  const redirectToPictureImage = () => {
    const imageInfo = allSources[currentIndex]
    const { galleryType } = imageInfo

    history.push(`/galerias/${galleryType}`)
  }

  useEffect(() => {
    getABitFromEachGallery()
      .then(setAllSources)
      .finally(() => setLoading(false))
  }, [])

  const { innerWidth } = window

  const classNames = ["", "gridAtTwo", "gridAtThree"]
  const galleryCategoriesLen = getTextByComposedKeys("galleries.categories").length
  const classNameForLast = classNames[galleryCategoriesLen % 3]

  return (
    <div className="">
      <div className="flex flex-col items-center justify-center w-full px-4 text-center text-indigo-700 bg-fixed bg-indigo-600 pb-52 heroTutorialsWrapper pt-52 bg-opacity-90 pattern-diagonal-lines-lg">
        <Fade bottom delay={200} duration={1200}>
          <h3 className="w-full max-w-5xl mb-6 font-mono text-white uppercase md:text-center text-8xl ">
            {getTextByComposedKeys("galleries.title")}
          </h3>
        </Fade>
        <div className="max-w-5xl mx-auto text-xl leading-relaxed text-white eventContainerInfoText">
          <Fade delay={600} duration={1200}>
            <div>{getTextByComposedKeys("galleries.howToParticipate")}</div>
            <div>{getTextByComposedKeys("galleries.subtitles")}</div>
          </Fade>
        </div>
        <div className="mt-8 galleriesCategories">
          <div className=" galleriesCategoriesContainer">
            {getTextByComposedKeys("galleries.categories").map((category, index) => (
              <button
                key={category}
                className="galleriesCategoryButton btn btn-info"
                onClick={() => redirectToGalleryCategoryPage(category)}
              >
                {category}
              </button>
            ))}
          </div>
          <h3 className="hidden mt-2 font-serif text-xl text-center text-white uppercase">
            {getTextByComposedKeys("galleries.categoriesTitle")}
          </h3>
        </div>
      </div>

      <div className="flex items-center justify-center w-full min-h-screen mx-auto text-indigo-800 bg-indigo-900 pattern-diagonal-lines-lg">
        {loading && <Spinner color="light" className="loadingPonchos" />}
        {!!allSources.length && (
          <GalleryCarousel
            allSources={allSources}
            setCurrentIndex={setCurrentIndex}
            onClick={redirectToPictureImage}
          />
        )}
      </div>
    </div>
  )
}

export default Galleries
