import React, { useEffect, useState } from "react";
import { Spinner } from "reactstrap";

import { getGames } from "../../../apiInteractions/apiCalls";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import GameList from "../GameList/GameList";
import GameLibraryFilter from "../../Molecules/GameLibraryFilter/GameLibraryFilter";
import "./GameLibraryStyles.css";

const GameLibrary = () => {
  const [games, setGames] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getGames()
      .then((filteredGames) => {
        setGames(filteredGames);
      })
      .finally(() => setLoading(false));
  }, []);

  const handleFilter = ({
    minPlayerNumber,
    maxPlayerNumber,
    minPlayTime,
    maxPlayTime,
    gameType,
  }) => {
    let queryString = "";
    if (minPlayerNumber) queryString += `&minPlayerNumber=${minPlayerNumber}`;
    if (maxPlayerNumber) queryString += `&maxPlayerNumber=${maxPlayerNumber}`;
    if (minPlayTime) queryString += `&minPlayTime=${minPlayTime}`;
    if (maxPlayTime) queryString += `&maxPlayTime=${maxPlayTime}`;
    if (gameType) queryString += `&gameType=${gameType}`;

    queryString = queryString.replace("&", "?");

    getGames(queryString).then((filteredGames) => {
      setGames(filteredGames);
    });
  };

  const parsedDescription = getTextByComposedKeys("gameLibrary.description")
    .split("\n")
    .map((paragraph) => <label>{paragraph}</label>);

  return (
    <div className="container gameLibraryContainer">
      <h1>{getTextByComposedKeys("gameLibrary.title")}</h1>
      <div className="gameLibraryDescrption">{parsedDescription}</div>
      <GameLibraryFilter handleSubmit={handleFilter} />
      {loading ? (
        <Spinner color="dark" className="loadingGameLibrary" />
      ) : (
        <GameList games={games} />
      )}
    </div>
  );
};

export default GameLibrary;
