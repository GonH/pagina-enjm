import React from "react";

import Game from "../../Molecules/Game/Game";

const GameList = ({ games }) => games.map((game) => <Game game={game} />);

export default GameList;
