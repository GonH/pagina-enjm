import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";
// import HomeBanner from "../../Molecules/HomeBanner/HomeBanner"
import RightNowHome from "../../Molecules/RightNowHome/RightNowHome"
import Shops from "../../Molecules/Shops/Shops"
import Sponsors from "../../Organisms/Sponsors/Sponsors"
import Team from "../../Organisms/Team/Team"
import GenderForm from "../../Atoms/GenderForm/GenderForm";
import "./HomeStyles.css";
import { DISCORD_LINK } from "../../../utils/constants/constants";
import HomeEnjmLogo from "../../../images/enjm10/logo-enjm10.png";
import HomeEnjmLogoNoTotem from "../../../images/enjm10/logo-enjm10-nototem.png";
import HomeEnjmTablero from "../../../images/enjm10/enjm10-tablero.jpg";
import HomeMiscArrow from "../../../images/enjm10/homearrow.png";
import HomeMiscInfinite from "../../../images/enjm10/homeinfinito.png";
import Instagram from "../../../images/svgs/Instagram";
import Facebook from "../../../images/svgs/Facebook";
import Youtube from "../../../images/svgs/Youtube";
import Discord from "../../../images/svgs/Discord";
// import Twitter from "../../../images/svgs/Twitter";
import SocialButtonIcon from "../../Molecules/SocialButtonIcon/SocialButtonIcon";
import { Helmet } from "react-helmet";

const GMAPS_LINK = "https://www.google.com/maps/place/Municipal+House+of+Culture+Almirante+Brown/@-34.7989009,-58.3900116,15z/data=!4m2!3m1!1s0x0:0xee19efcf4102bc0e?sa=X&ved=2ahUKEwj9gNqMx_jzAhVMp5UCHQmqA94Q_BJ6BAg_EAU";

const Home = () => {
  const [currentModal, setCurrentModal] = useState("");

  const closeModal = () => setCurrentModal("");

  return (
    <div className="linkTree">
      <Helmet>
        <body className="HomePage" />
      </Helmet>
        <RightNowHome />
      <div className="container encounterContainer" style={{ display: "none" }}>
        {currentModal && (
          <Modal centered isOpen={currentModal} toggle={closeModal}>
            <ModalHeader toggle={closeModal}>
              <h2>
                {getTextByComposedKeys(`home.join.${currentModal}.title`)}
              </h2>
            </ModalHeader>
            <ModalBody>
              <div className="joinModalBody">
                <a
                  className="alreadyKnow"
                  href={DISCORD_LINK}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {getTextByComposedKeys("home.join.alreadyKnow")}
                </a>
                <a className="dontKnow" href={"/tutoriales"}>
                  {getTextByComposedKeys("home.join.dontKnow")}
                </a>
              </div>
            </ModalBody>
          </Modal>
        )}
        <div className="pageExplanation">
          {getTextByComposedKeys("home.pageExplanation")
            .split("\n")
            .map((paragraph, index) => (
              <label key={index}>{paragraph}</label>
            ))}
        </div>
        <Button color="success" onClick={() => setCurrentModal("gamingPlace")}>
          <i style={{ marginRight: "1em" }} className="fab fa-discord" />
          {getTextByComposedKeys("home.link.gamingPlace")}
        </Button>
        <GenderForm />
      </div>

      <div className="HomeHero">
        <div className="homeEnjmLogoContainer">
          <img alt="ENJM Logo" className="homeEnjmLogo" src={HomeEnjmLogo} />
          <img
            alt="ENJM Logo"
            className="homeEnjmLogoNoTotem"
            src={HomeEnjmLogoNoTotem}
          />
          <img
            alt="ENJM Tablero historia"
            className="homeEnjmTablero"
            src={HomeEnjmTablero}
          />
          <img alt="Arrow" className="homeMiscArrow" src={HomeMiscArrow} />
        </div>
        <h1>La Historia cuenta un Juego</h1>
        <h2>
          4, 5, 6 y 7 de Noviembre. <br />
          Casa de la Cultura, Alte. Brown <br />
          <a href={GMAPS_LINK} target="_blank" rel="noreferrer">Esteban Adrogué 1224</a>
        </h2>
        <a
          href="https://docs.google.com/forms/d/e/1FAIpQLSfA54fh1d7rvgZpYgZL7sj05NqDMKDlwFwOj0upLM93bbaJZw/viewform"
          target="_blank"
          className="btn-form"
          rel="noopener noreferrer"
        >
          Quiero inscribirme a los torneos
        </a>
        <a
          href="https://docs.google.com/forms/d/e/1FAIpQLSdVNip__4jT4LxIIBPMyvi-mNFCfA-Tn5SbKv6C8VSvG6Pkdw/viewform"
          target="_blank"
          className="btn-form"
          rel="noopener noreferrer"
        >
          Quiero inscribir una escuela
        </a>
      </div>
      <div>
        <Shops inHome />
        <Sponsors inHome onlySponsors />
        <Team inHome />
      </div>
      <div className="socialButtonsContainer pattern-grid-xl">
        <div className="socialButtons">
          <SocialButtonIcon name="instagram" Svg={Instagram} />
          {/* Inactivo <SocialButtonIcon name="twitter" Svg={Twitter} /> */}
          <SocialButtonIcon name="discord" Svg={Discord} />
          <SocialButtonIcon name="youtube" Svg={Youtube} />
          <SocialButtonIcon name="facebook" Svg={Facebook} />
        </div>
        <img
          className="homeMiscInfinite"
          alt="infinito"
          src={HomeMiscInfinite}
        />
      </div>
    </div>
  );
};

export default Home;
