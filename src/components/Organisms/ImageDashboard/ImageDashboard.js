import React from "react";

import DashboardLogos from "../../Molecules/DashboardLogos/DashboardLogo";
import ImageDashboardTitle from "../../Atoms/ImageDashboardTitle/ImageDashboardTitle";
import ImageDashboardText from "../../Atoms/ImageDashboardText/ImageDashboardText";
import "./ImageDashboardStyles.css";

const getClassNameByLength = (length) => {
  const { innerWidth } = window;

  if (innerWidth < 700) return "";

  const classNames = ["", "gridAtTwo", "gridAtThree"];
  return classNames[length % 3];
};

const ImageDashboard = ({ infoList, type, className = "" }) => (
  <div className={className}>
    {type && <ImageDashboardTitle type={type} />}
    {type && <ImageDashboardText type={type} />}
    <div className={infoList.length ? "logosGrid" : "emptyGrid"}>
      {infoList.map(({ _id, name, image, link, noBackground }, index) => (
        <DashboardLogos
          index={index}
          noBackground={noBackground}
          _id={_id}
          key={_id}
          team={name}
          image={image}
          type={type}
          link={link}
          className={
            infoList.length - 1 === index
              ? getClassNameByLength(infoList.length)
              : ""
          }
        />
      ))}
    </div>
  </div>
);

export default ImageDashboard;
