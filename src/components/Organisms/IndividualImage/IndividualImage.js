import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Spinner } from "reactstrap";

import GoBackFloatingButton from "../../Atoms/GoBackFloatingButton/GoBackFloatingButton";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import { getGalleryType } from "../../../utils/helpers/constantHelpers";
import { getRandomId } from "../../../utils/helpers/deviceHelpers";
import ImageLikesButton from "../../Atoms/ImageLikesButton/ImageLikesButton";
import ImageComments from "../../Molecules/ImageComments/ImageComments";
import {
  getGalleryPictureById,
  toggleLikeStatus,
  submitComment,
} from "../../../apiInteractions/apiCalls";
import "./IndividualImageStyles.css";

const IndividualImage = () => {
  const [imageInfo, setImageInfo] = useState({});
  const [loadingCommentSubmit, setLoadingCommentSubmit] = useState(false);
  const [loadingLike, setLoadingLike] = useState(false);
  const [newComment, setNewComment] = useState("");
  const [isLiked, setIsLiked] = useState(false);
  const [likeCount, setLikeCount] = useState("-");
  const [imageComments, setImageComments] = useState([]);
  const [loading, setLoading] = useState(true);

  const { galleryType, id } = useParams();

  useEffect(() => {
    if (!imageInfo || !Object.keys(imageInfo).length) return;

    const { likes, isLikedById, comments } = imageInfo;

    setImageComments(comments);
    setLikeCount(likes);
    setIsLiked(!!isLikedById);
  }, [imageInfo]);

  useEffect(() => {
    const browserId = getRandomId();

    getGalleryPictureById({
      galleryType: getGalleryType(galleryType),
      id,
      browserId,
    })
      .then((response) => setImageInfo(response))
      .finally(() => setLoading(false));
  }, []);

  const handleCommentSubmit = (comment) => {
    if (!comment || loadingCommentSubmit) return;

    setLoadingCommentSubmit(true);

    submitComment({ galleryType: getGalleryType(galleryType), id, comment })
      .then(() => {
        setImageComments([...imageComments, comment]);
        setNewComment("");
      })
      .finally(() => {
        setLoadingCommentSubmit(false);
      });
  };

  const likeImage = () => {
    if (loadingLike) return;

    const isNotLiked = !isLiked;
    const newLikeCount = likeCount + isNotLiked - !isNotLiked;

    setIsLiked(isNotLiked);
    setLikeCount(newLikeCount);
    setLoadingLike(true);

    const browserId = getRandomId();

    toggleLikeStatus({
      galleryType: getGalleryType(galleryType),
      id,
      browserId,
    }).then(() => setLoadingLike(false));
  };

  const { submitterName, imageName, additionalInfo, image } = imageInfo;

  const goBackButtonText = `${getTextByComposedKeys(
    "galleries.specificImage.backToGallery"
  )} ${galleryType.split("_").join(" ")}`;

  if (loading)
    return (
      <div className="loadingSpecificImageContainer">
        <Spinner color="dark" className="loadingSpecificImage" />
      </div>
    );

  return (
    <div className="container individualImageContainer">
      <GoBackFloatingButton
        buttonText={goBackButtonText}
        redirectTo={`/galerias/${galleryType}`}
      />
      {imageName && <h1>{imageName}</h1>}
      {submitterName && <h2>- {submitterName}</h2>}
      <img className="individualImage" src={image} alt="Imagen individual"/>
      <div className="likesContainer">
        <ImageLikesButton isLiked={isLiked} onClick={likeImage} />
        {!!likeCount && <label>{likeCount}</label>}
      </div>
      {additionalInfo && (
        <div className="imageDescriptionContainer">
          {additionalInfo.split("\n").map((paragraph) => (
            <label>{paragraph}</label>
          ))}
          <label className="imageDescriptionSignature">
            - {submitterName || "Autor"}
          </label>
        </div>
      )}
      <ImageComments
        comments={imageComments}
        handleCommentSubmit={handleCommentSubmit}
        loadingSubmit={loadingCommentSubmit}
        newComment={newComment}
        setNewComment={setNewComment}
      />
    </div>
  );
};

export default IndividualImage;
