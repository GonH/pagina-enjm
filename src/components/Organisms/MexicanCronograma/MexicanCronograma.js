import React, { useState } from "react";

import EventContainer from "../EventContainer/EventContainer";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import MexicanSchedule from "../../../mexicanSchedule";
import "./CronogramaStyles.css";

const MexicanCronograma = () => {
  const [selectedEvent, setSelectedEvent] = useState();

  const info = getTextByComposedKeys("cronograma.externalSubtitle")
    .split("\n")
    .map((info) => <h6>{info}</h6>);

  const eventSelect = (event) => {
    setSelectedEvent(event);
  };

  const toggleModal = () => {
    setSelectedEvent();
  };

  const props = {
    info,
    eventSelect,
    toggleModal,
    title: getTextByComposedKeys("cronograma.title"),
    selectedEvent,
    events: MexicanSchedule,
  };

  return <EventContainer className="container" externalEvent {...props} />;
};

export default MexicanCronograma;
