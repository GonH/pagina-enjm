import React, { useEffect, useState } from "react";

import { getEvents } from "../../../apiInteractions/apiCalls";
import OldEventContainer from "../OldEventContainer/OldEventContainer";
import { ALL_YEARS } from "../../../utils/constants/constants";

const Cronograma = () => {
  const [selectedEvent, setSelectedEvent] = useState();
  const [events, setEvents] = useState({});
  const [types, setTypes] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchEvents = async () => {
      try {
        setLoading(true);
        const baseEvents = {};
        const baseTypes = [];

        for (let i = 0; i < ALL_YEARS.length; i++) {
          const year = ALL_YEARS[i];

          const { events: eventsResponse, types: tipos } = await getEvents(
            `?year=${year}`
          );

          if (tipos) {
            tipos.forEach((type) => {
              if (!types.includes(type)) types.push(type);
            });
          }

          if (eventsResponse) baseEvents[year] = eventsResponse;
        }

        setTypes(baseTypes);
        setEvents(baseEvents);
      } finally {
        setLoading(false);
      }
    };

    fetchEvents();
  }, []);

  const eventSelect = (event) => {
    setSelectedEvent(event);
  };

  const props = {
    eventSelect,
    toggleModal: () => setSelectedEvent(),
    selectedEvent,
    types,
    events,
    loading,
  };

  return <OldEventContainer {...props} />;
};

export default Cronograma;
