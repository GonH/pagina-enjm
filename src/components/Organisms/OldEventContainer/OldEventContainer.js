import React from "react"
import { Spinner } from "reactstrap"

import { getTextByComposedKeys } from "../../../utils/constants/text"
import CustomCronograma from "../CustomCronograma/CustomCronograma"
import CronogramaModal from "../../Molecules/CronogramaModal/CronogramaModal"
import Fade from "react-reveal/Fade"
import AnchorLink from "react-anchor-link-smooth-scroll"
import { AiOutlineArrowDown } from "react-icons/ai"
import "./OldEventContainerStyles.css"

const OldEventContainer = ({
  eventSelect,
  toggleModal,
  selectedEvent,
  types,
  events,
  loading,
}) => (
  <div className=" cronogramaPage">
    <div className="flex flex-col items-center justify-center w-full px-4 py-48 text-indigo-700 bg-fixed bg-indigo-600 lg:py-40 heroTutorialsWrapper bg-opacity-90 pattern-diagonal-lines-lg">
      <div className="w-full max-w-5xl mx-auto leading-relaxed text-white eventContainerInfoText">
        <Fade bottom delay={1000} duration={1200}>
          <a
            className="mb-6 btn btn-warning"
            href={`${window.location.origin}/cronograma`}
          >
            {getTextByComposedKeys("previousEvents.back")}
          </a>
        </Fade>
      </div>
      <Fade bottom delay={300} duration={1200}>
        <h3 className="w-full max-w-5xl mb-6 font-mono text-6xl font-bold text-white uppercase md:text-center md:text-8xl ">
          {getTextByComposedKeys("previousEvents.title")}
        </h3>
      </Fade>
      <div className="w-full max-w-5xl mx-auto leading-relaxed text-white eventContainerInfoText">
        <Fade bottom delay={600} duration={1200}>
          <div className="text-2xl">{getTextByComposedKeys("previousEvents.info")}</div>
        </Fade>
      </div>
      <Fade bottom delay={1000} duration={1200}>
        <AnchorLink
          href="#filter"
          className="relative anchorLink"
        >
          <AiOutlineArrowDown className="mt-12 text-5xl text-white animate-pulse"/>
        </AnchorLink>
      </Fade>
    </div>
    <CronogramaModal isOpen={selectedEvent} toggle={toggleModal} event={selectedEvent} />
    <div id="filter" className="pt-24">
      {!!Object.keys(events).length &&
        Object.keys(events).map((year) => (
          <div className="oldEventYearContainer">
            <em>Eventos del {year}</em>
            <CustomCronograma
              eventSelect={eventSelect}
              cronograma={events[year]}
              types={types}
              isGameProposal={false}
            />
          </div>
        ))}
      {loading && (
        <div className="flex items-center justify-center py-24 emptyOldEventContainer ">
          <Spinner className="loadingPonchos" />
        </div>
      )}
    </div>
  </div>
)

export default OldEventContainer
