import React, { useEffect, useState } from "react";

import { getEvents } from "../../../apiInteractions/apiCalls";
import EventContainer from "../EventContainer/EventContainer";
import PastEvent from "../../Molecules/PastEvent/PastEvent";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./PastEventsStyles.css";

const Cronograma = () => {
  const baseQueryString = `?to=${new Date().getTime()}`;

  const [selectedType, setSelectedType] = useState(0);
  const [selectedEvent, setSelectedEvent] = useState();
  const [events, setEvents] = useState([]);
  const [loading, setLoading] = useState(true);
  const [types, setTypes] = useState([]);
  const [queryString, setQueryString] = useState(baseQueryString);

  const info = getTextByComposedKeys("previousEvents.subtitle")
    .split("\n")
    .map((info) => <h6>{info}</h6>);

  const endDate = new Date();

  useEffect(() => {
    getEvents(queryString)
      .then(({ eventos: eventsResponse = {}, tipos = [] }) => {
        setTypes(tipos);
        setEvents(eventsResponse);
      })
      .finally(() => setLoading(false));
  }, [queryString]);

  useEffect(() => {
    const newQueryString = `${baseQueryString}${
      selectedType || selectedType === "0" ? `&type=${selectedType}` : ""
    }`;

    setQueryString(newQueryString);
  }, [selectedType]);

  const handleFilterSubmit = ({ type }) => {
    setSelectedType(type);
  };

  const eventSelect = (event) => {
    setSelectedEvent(event);
  };

  const startDate = new Date();
  startDate.setHours(0, 0, 0);

  const props = {
    info,
    handleFilterSubmit,
    eventSelect,
    startDate,
    endDate,
    selectedType,
    title: getTextByComposedKeys("previousEvents.title"),
    selectedEvent,
    handleFilterSubmit: () => {},
    types,
    events,
    loading,
  };

  return selectedEvent ? (
    <PastEvent {...selectedEvent} goBack={() => setSelectedEvent()} />
  ) : (
    <EventContainer {...props} />
  );
};

export default Cronograma;
