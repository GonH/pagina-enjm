import React, { useState, useEffect } from "react"
import _ from "lodash"

import ClearableItemList from "../../Molecules/ClearableItemList/ClearableItemList"
import PonchoPrizeFilter from "../../Molecules/PonchoPrizeFilter/PonchoPrizeFilter"
import YearPrizes from "../../Molecules/YearPrizes/YearPrizes"
import {
  currentYearNomination,
  getPonchoPrizes,
  getPremiosPonchoYears,
  getPremiosPonchoCategories,
} from "../../../apiInteractions/ponchoInteractions"
import CurrentYearNominees from "../../Molecules/CurrentYearNominees/CurrentYearNominees"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./PonchoPrizesStyles.css"
import Fade from "react-reveal/Fade"
import { SRLWrapper } from "simple-react-lightbox"
const addSwapItemBetweenLists = ({ toRemove, deleteFrom, insertInto }) => {
  const withInserted = [...insertInto, toRemove]

  _.remove(deleteFrom, (toDelete) => toDelete == toRemove)

  const withoutRemoved = [...deleteFrom]

  return { withInserted, withoutRemoved }
}

const PonchoPrizes = () => {
  const [premios, setPremios] = useState({})
  const [selectableYears, setSelectableYears] = useState([])
  const [years, setYears] = useState([])
  const [selectableCategories, setSelectableCategories] = useState([])
  const [categories, setCategories] = useState([])

  const setInitialState = () => {
    setPremios(getPonchoPrizes({ years: [], categories: [] }))
    setSelectableYears(getPremiosPonchoYears())
    setSelectableCategories(getPremiosPonchoCategories())
  }

  useEffect(() => {
    setInitialState()
  }, [])

  const addYear = (yearToAdd) => {
    const { withInserted, withoutRemoved } = addSwapItemBetweenLists({
      toRemove: yearToAdd,
      deleteFrom: selectableYears,
      insertInto: years,
    })

    setYears(withInserted)
    setSelectableYears(withoutRemoved)
  }

  const addCategory = (categoryToAdd) => {
    const { withInserted, withoutRemoved } = addSwapItemBetweenLists({
      toRemove: categoryToAdd,
      deleteFrom: selectableCategories,
      insertInto: categories,
    })

    setCategories(withInserted)
    setSelectableCategories(withoutRemoved)
  }

  const removeYear = (yearToRemove) => {
    const { withInserted, withoutRemoved } = addSwapItemBetweenLists({
      toRemove: yearToRemove,
      deleteFrom: years,
      insertInto: selectableYears,
    })

    setSelectableYears(withInserted)
    setYears(withoutRemoved)
  }

  const removeCategory = (categoryToRemove) => {
    const { withInserted, withoutRemoved } = addSwapItemBetweenLists({
      toRemove: categoryToRemove,
      deleteFrom: categories,
      insertInto: selectableCategories,
    })

    setSelectableCategories(withInserted)
    setCategories(withoutRemoved)
  }

  const clearFilters = () => {
    setInitialState()
    setYears([])
    setCategories([])
  }

  const handleSubmit = () => setPremios(getPonchoPrizes({ categories, years }))

  const filterProps = {
    addYear,
    addCategory,
    years: selectableYears,
    categories: selectableCategories,
    handleSubmit,
    clearFilters,
  }

  const subtitle = getTextByComposedKeys("ponchoPrizes.subtitle")
    .split("\n")
    .map((paragraph, index) => <label key={index}>{paragraph}</label>)

  return (
    <div className="flex flex-col ponchoPrizesContainer">
      <div className="relative flex flex-col items-center justify-center pb-40 overflow-hidden text-white bg-indigo-700 pt-52 ponchoStartContainer">
        <div className="relative z-10">
          <h1 className="font-mono uppercase text-8xl">
            {getTextByComposedKeys("ponchoPrizes.title")} 2021
          </h1>
        </div>
        <div className="opacity-5">
          <Fade delay={600} duration={1200}>
            <img
              src="https://i.imgur.com/BbXoXcQ.jpg"
              alt="Banner de los premios poncho"
              className="absolute inset-0 object-cover w-full min-h-screen"
            />
          </Fade>
        </div>
      </div>
      {/* <div className="text-gray-800 ponchoPrizeInitialSubtitles">
        <p>Estos son los nominados para cada categoría de los Premios Poncho 2021.</p>
        <p>
          La premiación va a ser el domingo a las 20 horas y se va a transmitir por el
          canal de youtube del ENJM.
        </p>
      </div> */}
      {/* <CurrentYearNominees nominees={currentYearNomination} /> */}
      <div className="text-indigo-300 bg-indigo-200 pattern-diagonal-lines-lg">
        <div className="ponchoPrizesSubtitles">{subtitle}</div>
        <div className="max-w-6xl p-3 mx-auto bg-gray-100 border-b border-gray-300">
          <p  className="w-full px-1 pb-3 font-serif text-sm font-bold text-center text-gray-700 ">Selecciona multiples filtros y hacer click en el botón</p>
          <PonchoPrizeFilter {...filterProps} />
          <div className="flex flex-col items-center justify-center pt-1 pb-3 mx-auto">
            <ClearableItemList
              itemList={years}
              handleItemRemoval={removeYear}
              type="Year"
            />
            <ClearableItemList
              type="Category"
              itemList={categories}
              handleItemRemoval={removeCategory}
            />
          </div>
        </div>
        <SRLWrapper>
          {
            <div className="text-indigo-900 ponchoPrizesByYear">
              {Object.keys(premios).length ? (
                Object.keys(premios).map((key) => (
                  <YearPrizes key={key} year={key} yearInfo={premios[key]} />
                ))
              ) : (
                <label className="noPrizes">
                  {getTextByComposedKeys("ponchoPrizes.noResults")}
                </label>
              )}
            </div>
          }
        </SRLWrapper>
      </div>
    </div>
  )
}

export default PonchoPrizes
