import React from "react"
import Fade from "react-reveal/Fade"

import PreviousIterationSummary from "../../Atoms/PreviousIterationSummary/PreviousIterationSummary"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./PreviousIterationsStyles.css"

import Enjm1 from "../../../images/1ENJM.png"
import Enjm2 from "../../../images/2ENJM.png"
import Enjm3 from "../../../images/3ENJM.jpg"
import Enjm4 from "../../../images/4ENJM.png"
import Enjm5 from "../../../images/5ENJM.jpg"
import Enjm6 from "../../../images/6ENJM.jpg"
import Enjm7 from "../../../images/7ENJM.jpg"
import Enjm8 from "../../../images/8ENJM.jpg"

// TODO agregar imagen falopa al ppo de la pag

const enjmPictures = {
  Enjm1,
  Enjm2,
  Enjm3,
  Enjm4,
  Enjm5,
  Enjm6,
  Enjm7,
  Enjm8,
}

const PreviousIterations = () => {
  const previousIterationList = [1, 2, 3, 4, 5, 6, 7, 8]

  const enjms = previousIterationList.map((id) => (
    <PreviousIterationSummary key={id} id={id}  src={enjmPictures[`Enjm${id}`]} />
  ))

  return (
    <section>
      <div className="iterationsContainer">
        <div className="flex flex-col items-center justify-center w-full px-4 py-40 text-indigo-700 bg-fixed bg-indigo-600 heroTutorialsWrapper bg-opacity-90 pattern-diagonal-lines-lg">
          <Fade delay={200} duration={1200}>
            <h3 className="w-full max-w-5xl mb-6 font-mono text-center text-white uppercase text-8xl ">
              {getTextByComposedKeys("previousIteration.title")}
            </h3>
          </Fade>
          <div className="w-full max-w-5xl mx-auto leading-relaxed text-white ">
            <Fade delay={600} duration={1200}>
              <div className="text-2xl">{getTextByComposedKeys("previousIteration.subtitle")}</div>
            </Fade>
          </div>
        </div>
        <div className="imagesGrid">{enjms}</div>
      </div>
    </section>
  )
}

export default PreviousIterations
