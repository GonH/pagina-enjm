import React, { useEffect, useState } from "react";
import { Spinner } from "reactstrap";

import ImageDashboard from "../ImageDashboard/ImageDashboard";
import { getPublishers } from "../../../apiInteractions/apiCalls";
import { getTextByComposedKeys } from "../../../utils/constants/text";
import "./PublisherStyles.css";

const Publishers = ({ inHome }) => {
  const [publishersInfo, setPublishersInfo] = useState([]);
  const [loading, setLoading] = useState(true);

  const finishLoading = () => setLoading(false);

  useEffect(() => {
    getPublishers()
      .then((team) => {
        setPublishersInfo(team);
      })
      .finally(finishLoading);
  }, []);

  const rows = !inHome && Math.ceil(publishersInfo.length / 3);

  if (loading) return <Spinner color="dark" className="loadingParticipants" />;

  return (
    <div className="container publisherContainer">
      <h1>{getTextByComposedKeys("publishers.title")}</h1>
      {!!publishersInfo.length && (
        <ImageDashboard
          infoList={publishersInfo}
          type=""
          className={`lonelyDashboard ${
            inHome ? "" : "container"
          } dashboard rows${rows}`}
        />
      )}
    </div>
  );
};

export default Publishers;
