import React, { useState, useEffect } from "react";
import Fade from "react-reveal/Fade";

import Sede from "../../Molecules/Sede/Sede";
import { getSedes } from "../../../apiInteractions/apiCalls";
import { getTextByComposedKeys } from "../../../utils/constants/text";

const Sedes = () => {
  const [sedes, setSedes] = useState([]);

  useEffect(() => {
    getSedes().then(setSedes);
  }, []);

  return (
    <div className="cronogramaPage">
      <div className="flex flex-col items-center justify-center w-full px-4 py-48 text-indigo-700 bg-fixed bg-indigo-600 lg:py-40 heroTutorialsWrapper bg-opacity-90 pattern-diagonal-lines-lg">
        <Fade delay={200} duration={1200}>
          <h3 className="w-full max-w-5xl mb-6 font-mono text-6xl font-bold text-white uppercase md:text-center md:text-8xl ">
            {getTextByComposedKeys("sedes.title")}
          </h3>
        </Fade>
        <div className="w-full max-w-5xl mx-auto leading-relaxed text-white eventContainerInfoText">
          <Fade delay={600} duration={1200}>
            <div>{getTextByComposedKeys("sedes.subtitle")}</div>
          </Fade>
        </div>
      </div>
      {sedes
        .filter(({ town }) => town)
        .map((sedeInfo, index) => (
          <Sede {...sedeInfo} index={index} />
        ))}
    </div>
  );
};

export default Sedes;
