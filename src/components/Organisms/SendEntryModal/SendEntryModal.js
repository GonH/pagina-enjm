import React, { useEffect, useState } from "react";
import {
  Button,
  Label,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import Dropzone from "../Dropzone/Dropzone";
import "./SendEntryModalStyles.css";

const SendEntryModal = ({ isOpen, toggle, title, handleNewEntrySubmit }) => {
  const [submitterName, setSubmitterName] = useState();
  const [submitterEmail, setSubmitterEmail] = useState();
  const [imageName, setImageName] = useState();
  const [additionalInfo, setAdditionalInfo] = useState();
  const [image, setImage] = useState();
  const [isInvalidForm, setIsInvalidForm] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (image && isInvalidForm) setIsInvalidForm(false);
  }, [image]);

  const innerToggle = () => {
    setSubmitterName();
    setSubmitterEmail();
    setImageName();
    setAdditionalInfo();
    setImage();

    toggle();
  };

  const handleSubmit = () => {
    if (isInvalidForm || !image) {
      setIsInvalidForm(true);
      return;
    }

    innerToggle();

    if (handleNewEntrySubmit)
      handleNewEntrySubmit({
        submitterName,
        submitterEmail,
        imageName,
        additionalInfo,
        image,
      });
  };

  return (
    <Modal centered isOpen={isOpen} toggle={innerToggle}>
      <ModalHeader toggle={innerToggle}>
        <h2>
          {getTextByComposedKeys("galleries.sendNew")}
          {title}
        </h2>
      </ModalHeader>
      <ModalBody className="sendEntryModalBody">
        <div>
          <Button
            className="sendEntryButton"
            onClick={handleSubmit}
            disabled={loading || isInvalidForm}
            color={isInvalidForm ? "danger" : "success"}
          >
            {getTextByComposedKeys(
              `galleries.sendForm.${
                isInvalidForm ? "imageRequired" : "sendForm"
              }`
            )}
          </Button>
          <div className="sendEntryCombo">
            <Label for="submitterName">
              {getTextByComposedKeys("galleries.sendForm.labels.submitterName")}
            </Label>
            <Input
              id="submitterName"
              value={submitterName}
              onChange={({ target: { value } }) => setSubmitterName(value)}
              placeholder={getTextByComposedKeys(
                "galleries.sendForm.placeholders.submitterName"
              )}
            />
          </div>
          <div className="sendEntryCombo">
            <Label for="submitterEmail">
              {getTextByComposedKeys(
                "galleries.sendForm.labels.submitterEmail"
              )}
            </Label>
            <Input
              id="submitterEmail"
              value={submitterEmail}
              onChange={({ target: { value } }) => setSubmitterEmail(value)}
              placeholder={getTextByComposedKeys(
                "galleries.sendForm.placeholders.submitterEmail"
              )}
            />
          </div>
          <div className="sendEntryCombo">
            <Label for="imageName">
              {getTextByComposedKeys("galleries.sendForm.labels.imageName")}
            </Label>
            <Input
              id="imageName"
              value={imageName}
              onChange={({ target: { value } }) => setImageName(value)}
              placeholder={getTextByComposedKeys(
                "galleries.sendForm.placeholders.imageName"
              )}
            />
          </div>
          <div className="sendEntryCombo">
            <Label for="additionalInfo">
              {getTextByComposedKeys(
                "galleries.sendForm.labels.additionalInfo"
              )}
            </Label>
            <Input
              id="additionalInfo"
              value={additionalInfo}
              type="textarea"
              onChange={({ target: { value } }) => setAdditionalInfo(value)}
              placeholder={getTextByComposedKeys(
                "galleries.sendForm.placeholders.additionalInfo"
              )}
            />
          </div>
          <div>
            <Label for="image">
              {getTextByComposedKeys("galleries.sendForm.labels.image")}
            </Label>
            <Dropzone
              image={image}
              setImage={setImage}
              setLoading={setLoading}
            />
          </div>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default SendEntryModal;
