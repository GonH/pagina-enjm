import React, { useEffect, useState } from "react";
import { Spinner } from "reactstrap";

import { getTextByComposedKeys } from "../../../utils/constants/text";
import ImageDashboardTitle from "../../Atoms/ImageDashboardTitle/ImageDashboardTitle";
import ImageDashboardText from "../../Atoms/ImageDashboardText/ImageDashboardText";
import ImageDashboard from "../ImageDashboard/ImageDashboard";
import PublicityBanner from "../../Molecules/PublicityBanner/PublicityBanner";
import { getDiscounts, getShops } from "../../../apiInteractions/apiCalls";
import "./ShopsStyles.css";

const Shops = ({ inHome }) => {
  const [shops, setShops] = useState([]);
  const [discountImages, setDiscountImages] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentShopName, setCurrentShopName] = useState("");

  useEffect(() => {
    getShops().then(setShops);

    getDiscounts()
      .then(setDiscountImages)
      .finally(() => setLoading(false));
  }, []);

  const rows = !inHome && Math.ceil(shops.length / 3);

  if (loading) return <Spinner color="dark" className="loadingParticipants" />;

  const updateCurrentShopName = (currentLink) => {
    for(let i = 0; i < shops.length; i++) {
      const { link, name } = shops[i];
      if (currentLink === link) return setCurrentShopName(name);
    }

    setCurrentShopName('');
  };

  return (
    !!shops.length && (
      <div className={`container shopsContainer rows${rows}`}>
        <div className="dashboard shopDashboard">
          <ImageDashboardTitle type="shops" />
          <ImageDashboardText type="shops" />
          <ImageDashboard infoList={shops} />
        </div>
        {!!discountImages.length && (
          <h2>{getTextByComposedKeys("shops.discounts")}</h2>
        )}
        {!!discountImages.length && (
          <h3>
            Hacé click sobre el banner para ir a la tienda y ver los descuentos.
            {currentShopName && ` La tienda actual es ${currentShopName}`}
          </h3>
        )}
        {!!discountImages.length && (
          <PublicityBanner publicity={discountImages} updateCurrentShopName={updateCurrentShopName} />
        )}
      </div>
    )
  );
};

export default Shops;
