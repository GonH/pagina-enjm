import React, { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
// import { Button } from "reactstrap";
import { Spinner } from "reactstrap"

import { getGalleryPictures, handleGalleryEntry } from "../../../apiInteractions/apiCalls"
import { esce, mini } from "../../../apiInteractions/galleryWinners"
import GoBackFloatingButton from "../../Atoms/GoBackFloatingButton/GoBackFloatingButton"
import GalleryWinners from "../../Molecules/GalleryWinners/GalleryWinners"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import { getGalleryType } from "../../../utils/helpers/constantHelpers"
import SendEntryModal from "../../Organisms/SendEntryModal/SendEntryModal"
import "./SpecificGalleryStyles.css"
import { SRLWrapper } from "simple-react-lightbox"
import { LazyLoadImage } from "react-lazy-load-image-component"
import "react-lazy-load-image-component/src/effects/blur.css"
const SpecificGallery = ({ winners }) => {
  const { galleryType } = useParams()
  const parsedGalleryType = getGalleryType(galleryType)
  const isCompetencia = parsedGalleryType === "Competencia_de_Arte"

  const [images, setImages] = useState([])
  const [isOpen, setIsOpen] = useState(false)
  const [loading, setLoading] = useState(true)
  const [partialLoading, setPartialLoading] = useState(true)

  const recursiveGalleryFetch = (ids = [], oldImages = []) => {
    // TODO: Refactorizar esto, se que es una cosa desesperada pero necesito
    // que no tarde eones en cargar la pag
    const joinedIds = ids.join("@")
    const idsQueryParams = joinedIds ? `&ids=${joinedIds}` : ""
    getGalleryPictures(parsedGalleryType, idsQueryParams).then((newImages) => {
      setLoading(false)
      if (!newImages.length) {
        setPartialLoading(false)
        return
      }

      const imagesToSave = [...oldImages, ...newImages]
      const newIds = newImages.map(({ _id }) => _id)
      const allIdsToAvoid = [...ids, ...newIds]

      setImages(imagesToSave)
      recursiveGalleryFetch(allIdsToAvoid, imagesToSave)
    })
  }

  useEffect(() => {
    recursiveGalleryFetch()
  }, [])

  const toggle = () => setIsOpen(!isOpen)
  const title = getTextByComposedKeys(`galleries.${parsedGalleryType}.title`)

  const handleNewEntrySubmit = (fields) => {
    if (isCompetencia) return

    handleGalleryEntry(JSON.stringify({ galleryType: parsedGalleryType, ...fields }))
      .then((savedImageId) => setImages([...images, { ...fields, _id: savedImageId }]))
      .catch(console.error)
  }

  const noImages = !images.length

  return (
    <div
      className={`pt-32 mt-0 max-w-7xl mx-auto min-h-screen ${
        noImages ? "emptySpecificGalleryContainer" : "specificGalleryContainer"
      }`}
    >
      <GoBackFloatingButton
        buttonText={getTextByComposedKeys("galleries.backToGalleries")}
        redirectTo={`/galerias`}
      />
      <h1 className="font-mono text-6xl font-bold uppercase md:text-9xl">{title}</h1>
      {isCompetencia && (
        <h3>
          El Equipo de Curaduría de este año seleccionó algunas ilustraciones para la
          muestra de Arte. Estas imágenes son ilustraciones o parte de componentes de
          juegos presentadas durante 2019 y 2020.
        </h3>
      )}
      {isCompetencia && (
        <h3>
          Disculpá las demoras, pero son muchas imágenes en buena calidad y no
          consideramos apropiado reducir su resolución para que fueran fieles a lo que el
          artista creó y el juzgado evaluó. Las imágenes se irán cargando por grupos de 4.
        </h3>
      )}
      {galleryType === "Espacio_Abierto" && (
        <h3>
          Esta es una exposición libre de arte donde cualquiér persona puede subir su obra
          para exponer. Si estás buscando la muestra que pasó por la curaduría tenés que
          ir a<a href="/galerias/Muestra_de_Arte"> esta página</a>, donde esas obras están
          expuestas.
        </h3>
      )}
      {!isCompetencia && (
        <SendEntryModal
          isOpen={isOpen}
          toggle={toggle}
          title={title}
          handleNewEntrySubmit={handleNewEntrySubmit}
        />
      )}
      <SRLWrapper>
        {title === "Miniaturas de Wargames" && (
          <GalleryWinners winners={esce} title="escenografía" />
        )}
        {title === "Miniaturas de Wargames" && (
          <GalleryWinners winners={mini} title="miniaturas" />
        )}
        {noImages && !(loading || isCompetencia) && (
          <label className="noEntriesText">
            {getTextByComposedKeys("galleries.noEntries")}
          </label>
        )}

        <div className="specificGalleryImages">
          {images.map(({ image, _id, imageName = "Esta imagen no tiene nombre" }) => (
            <div key={_id} className="bg-gray-100 cursor-pointer">
              <LazyLoadImage
                className="object-cover w-full h-72 "
                alt={`Foto de ${imageName}`}
                effect="blur"
                src={image}
              />
            </div>
          ))}
        </div>
      </SRLWrapper>
      {partialLoading && <Spinner color="dark" className="loadingGallery" />}
    </div>
  )
}

export default SpecificGallery
