import React, { useEffect, useState } from "react";
import { Spinner } from "reactstrap";

import ImageDashboardTitle from "../../Atoms/ImageDashboardTitle/ImageDashboardTitle";
import ImageDashboardText from "../../Atoms/ImageDashboardText/ImageDashboardText";
import ImageDashboard from "../ImageDashboard/ImageDashboard";
import { getSponsors } from "../../../apiInteractions/apiCalls";
import "./SponsorsStyles.css";

const Sponsors = ({ onlySponsors, inHome }) => {
  const [sponsors, setSponsors] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getSponsors()
      .then((sponsors) => {
        const byTier = {};
        sponsors.forEach((sponsor) => {
          const { tier } = sponsor;
          const trueTier = tier || "shop";

          if (!byTier[trueTier]) byTier[trueTier] = [];

          byTier[trueTier].push(sponsor);
        });

        setSponsors(byTier);
      })
      .finally(() => setLoading(false));
  }, []);

  if (loading) return <Spinner color="dark" className="loadingParticipants" />;

  return (
    <div className={`${inHome ? "" : "container"} allSponsorsContainer`}>
      <div className="dashboard">
        <ImageDashboardTitle type="sponsors" />
        <ImageDashboardText type="sponsors" />
      </div>
      {Object.keys(sponsors).map((key) => {
        if (!sponsors[key]) return <div />;

        return (
          <ImageDashboard
            infoList={sponsors[key]}
            className="sponsorsContainer"
          />
        );
      })}
    </div>
  );
};

export default Sponsors;
