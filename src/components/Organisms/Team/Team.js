import React, { useEffect, useState } from "react";
import { Spinner } from "reactstrap";

import ImageDashboard from "../ImageDashboard/ImageDashboard";
import { getTeam } from "../../../apiInteractions/apiCalls";

const Teams = ({ inHome }) => {
  const [teamInfo, setTeamInfo] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getTeam().then((team) => {
      setTeamInfo(team);
    }).finally(() => setLoading(false));
  }, []);

  const rows = !inHome && Math.ceil(teamInfo.length / 3);

  if (loading) return <Spinner color="dark" className="loadingParticipants" />;

  return (
    !!teamInfo.length && (
      <ImageDashboard
        infoList={teamInfo}
        type="teams"
        className={`lonelyDashboard ${inHome ? "" : "container"} dashboard rows${rows}`}
      />
    )
  );
};

export default Teams;
