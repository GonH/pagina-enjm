import React, { useState } from "react"
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,

} from "reactstrap"
import classnames from "classnames"
import YoutubeEmbed from "../../Atoms/YoutubeEmbed/YoutubeEmbed"
import { DISCORD_LINK } from "../../../utils/constants/constants"
import { getTextByComposedKeys } from "../../../utils/constants/text"
import "./TutorialsStyles.css"
import Fade from "react-reveal/Fade"

const webTutorial = {
  tutorialType: "web",
  link: "https://www.youtube.com/embed/_DpQeNVzVpI",
}
const windowsTutorial = {
  tutorialType: "windows",
  link: "",
}
const androidTutorial = {
  tutorialType: "android",
  link: "https://www.youtube.com/embed/Pv_Jf2p42UI",
}

const tutorials = {
  web: webTutorial,
  window: windowsTutorial,
  android: androidTutorial,
}

const handleArrayInsertion = (toCheckOS, currentTutorials) => {
  const isOs = window.navigator.userAgent.toLowerCase().includes(toCheckOS)
  const toInsert = tutorials[toCheckOS]

  if (isOs) return [toInsert, ...currentTutorials]

  return [...currentTutorials, toInsert]
}

const Tutorials = () => {
  let tutorials = [webTutorial]
  tutorials = handleArrayInsertion("window", tutorials)
  tutorials = handleArrayInsertion("android", tutorials)

  const [activeTab, setActiveTab] = useState("web")

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab)
  }

  return (
    <div className="w-full tutorialsWrapper ">
      <div className="flex flex-col items-center justify-center w-full px-4 pt-64 pb-32 text-indigo-700 bg-fixed bg-indigo-600 heroTutorialsWrapper bg-opacity-90 pattern-diagonal-lines-lg">
        <Fade delay={200} duration={1200}>
          <h3 className="mb-6 font-mono text-center text-white uppercase text-8xl ">
            {getTextByComposedKeys(`tutorials.title`)}
          </h3>
        </Fade>
        <div className="flex items-center justify-center max-w-5xl mx-auto font-serif text-xl leading-relaxed text-center text-white">
          <Fade delay={600} duration={1200}>
            <p className="">{getTextByComposedKeys(`tutorials.subtitle`)}</p>
          </Fade>
        </div>
        <div className="mx-auto text-center action ">
          <Fade delay={600} duration={1200}>
            <a
              className="btn btn-success"
              href={DISCORD_LINK}
              target="_blank"
              rel="noopener noreferrer"
            >
              Unirme al servidor
            </a>
          </Fade>
        </div>
      </div>

      <div className="w-full max-w-4xl py-12 mx-auto ">
        <Nav tabs>
          {tutorials.map(({ tutorialType, link }) => {
            if (link)
              return (
                <NavItem key={tutorialType}>
                  <NavLink
                    className={classnames({ active: activeTab === tutorialType })}
                    onClick={() => {
                      toggle(tutorialType)
                    }}
                  >
                    {getTextByComposedKeys(`tutorial.${tutorialType}.title`)}
                  </NavLink>
                </NavItem>
              )
          })}
        </Nav>
        <TabContent activeTab={activeTab}>
          {tutorials.map(({ tutorialType, link }) => {
            if (link)
              return (
                <TabPane key={tutorialType} tabId={tutorialType}>
                  <YoutubeEmbed src={link} />
                </TabPane>
              )
          })}
        </TabContent>
      </div>

      <div className="hidden w-full gap-6 mx-auto md:grid-cols-2 max-w-7xl">
        {tutorials.map(({ tutorialType, link }) => {
          if (link)
            return (
              <div key={link} className="tutorialCombo">
                <label className="tutorialLabel">
                  {getTextByComposedKeys(`tutorial.${tutorialType}.title`)}
                </label>
                <YoutubeEmbed src={link} />
              </div>
            )
        })}
      </div>
    </div>
  )
}

export default Tutorials
