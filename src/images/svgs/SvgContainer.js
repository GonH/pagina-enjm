import React from 'react';

const SvgContainer = ({ d }) => (<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path fill="none" d="M0 0h24v24H0z" />
  <path
    fill="#062f6f"
    d={d}
  />
</svg>)

export default SvgContainer;
