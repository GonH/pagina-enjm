import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Header from "./components/Molecules/Header/Header";
import About from "./components/Molecules/About/About";
import Offcanvas from "./components/Molecules/Offcanvas/Offcanvas";
import Team from "./components/Organisms/Team/Team";
import ScrollIntoView from "./components/Atoms/ScrollIntoView/ScrollIntoView";
import Contact from "./components/Organisms/Contact/Contact";
import Cronograma from "./components/Organisms/Cronograma/Cronograma";
import Sponsors from "./components/Organisms/Sponsors/Sponsors";
import Shops from "./components/Organisms/Shops/Shops";
import PreviousIterations from "./components/Organisms/PreviousIterations/PreviousIterations";
import Encounter from "./components/Organisms/Encounter/Encounter";
import Home from "./components/Organisms/Home/Home";
import Tutorials from "./components/Organisms/Tutorials/Tutorials";
import GameLibrary from "./components/Organisms/GameLibrary/GameLibrary";
import PastEvents from "./components/Organisms/PastEvents/PastEvents";
import PonchoPrizes from "./components/Organisms/PonchoPrizes/PonchoPrizes";
import CurrentThings from "./components/Organisms/CurrentThings/CurrentThings";
import Galleries from "./components/Organisms/Galleries/Galleries";
import SpecificGallery from "./components/Organisms/SpecificGallery/SpecificGallery";
import IndividualImage from "./components/Organisms/IndividualImage/IndividualImage";
import { getTextByComposedKeys } from "./utils/constants/text";
import AdminForm from "./components/Organisms/AdminForm2/AdminForm2";
import AdminPanel from "./components/Organisms/AdminPanel/AdminPanel";
import AdminEditForm from "./components/Organisms/AdminEditForm/AdminEditForm";
import NextUp from "./components/Molecules/NextUp/NextUp";
import AdminEditNextUp from "./components/Organisms/AdminEditNextUp/AdminEditNextUp";
import AdminContact from "./components/Organisms/AdminContact/AdminContact";
import AdminMensajes from "./components/Organisms/AdminMensajes/AdminMensajes";
import AdminDiscounts from "./components/Organisms/AdminDiscounts/AdminDiscounts";
import Footer from "./components/Molecules/Footer/Footer";
import CronogramaLudico from "./components/Organisms/CronogramaLudico/CronogramaLudico";
import MexicanCronograma from "./components/Organisms/MexicanCronograma/MexicanCronograma";
import Publishers from "./components/Organisms/Publishers/Publishers";
import OldCronograma from "./components/Organisms/OldCronograma/OldCronograma";
import Sedes from './components/Organisms/Sedes/Sedes';
import AdminSedes from './components/Organisms/AdminSedes/AdminSedes';

import { AiOutlineArrowUp } from "react-icons/ai";
import AnchorLink from "react-anchor-link-smooth-scroll";
import SimpleReactLightbox from "simple-react-lightbox";

const blacklistedRoutes = [
  // "inicio",
  // "about",
  // "team",
  // "sponsors",
  // "shops",
  // "contact",
  // "meeting",
  // "previousIterations",
  // "cronograma",
  // "tutorial",
  "gameLibrary",
  "pastEvents",
  // "ponchoPrizes",
  "currentThings",
  // "galleries",
  // "specificGallery",
  // "home",
  "publishers",
]

const routeNames = [
  { routeName: "inicio", component: Home },
  { routeName: "about", component: About },
  { routeName: "team", component: Team },
  { routeName: "sponsors", component: Sponsors },
  { routeName: "shops", component: Shops },
  { routeName: "contact", component: Contact },
  { routeName: "meeting", component: Encounter },
  { routeName: "previousIterations", component: PreviousIterations },
  { routeName: "cronograma", component: Cronograma },
  { routeName: "tutorial", component: Tutorials },
  { routeName: "gameLibrary", component: GameLibrary },
  { routeName: "pastEvents", component: PastEvents },
  { routeName: "ponchoPrizes", component: PonchoPrizes },
  { routeName: "currentThings", component: CurrentThings },
  { routeName: "galleries", component: Galleries },
  { routeName: "individualImage", component: IndividualImage },
  { routeName: "specificGallery", component: SpecificGallery },
  { routeName: "adminForm", component: AdminForm },
  { routeName: "genericAdminForm", component: AdminForm },
  { routeName: "adminPanel", component: AdminPanel },
  { routeName: "adminEdit", component: AdminEditForm },
  { routeName: "nextUp", component: NextUp },
  { routeName: "editNextUp", component: AdminEditNextUp },
  { routeName: "mailing", component: AdminContact },
  { routeName: "messaging", component: AdminMensajes },
  { routeName: "discounts", component: AdminDiscounts },
  { routeName: "gameProposals", component: CronogramaLudico },
  { routeName: "mexicanCronograma", component: MexicanCronograma },
  { routeName: "publishers", component: Publishers },
  { routeName: "oldEvents", component: OldCronograma },
  { routeName: "sedes", component: Sedes },
  { routeName: "adminSedes", component: AdminSedes },
  { routeName: "", component: Home },
]

const routes = routeNames.map(
  ({ routeName, component }) =>
    !blacklistedRoutes.includes(routeName) && (
      <Route
        key={routeName}
        exact
        path={getTextByComposedKeys(`header.links.${routeName}`)}
        component={component}
      />
    )
)

const getHeaderStyles = () => {
  const { userAgent } = navigator
  const lowerCasedUserAgent = userAgent.toLowerCase()

  const isBrowser = (browserName) => lowerCasedUserAgent.includes(browserName)

  const isSafari = isBrowser("safari")
  const isChrome = isBrowser("chrome")
  const isFirefox = isBrowser("firefox")
  const isOpera = isBrowser("opera")

  const isOnlySafari = isSafari && !(isChrome || isFirefox || isOpera)

  console.info({
    isSafari,
    isChrome,
    isFirefox,
    isOpera,
    isOnlySafari,
  })

  return isOnlySafari
}

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <div>
        <ScrollIntoView>
          <div id="top" />
          <Offcanvas />
          <Header blacklist={blacklistedRoutes} notFixed={getHeaderStyles()} />
          <SimpleReactLightbox>
            <Switch>{routes}</Switch>
          </SimpleReactLightbox>
          <AnchorLink
            className="fixed bottom-0 right-0 z-50 p-3 text-xl text-white duration-300 bg-indigo-500 rounded-tl-md hover:bg-indigo-600 hover:text-indigo-300 to-top"
            href="#top"
          >
            <AiOutlineArrowUp />
          </AnchorLink>
          <Footer />
        </ScrollIntoView>
      </div>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
