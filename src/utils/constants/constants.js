export const ANNOUNCEMENT = "Anuncio";
export const TALK = "Charla";
export const GAME_PROPOSAL = "Propuesta Lúdica";
export const WORKSHOP = "Taller";
export const LIVE = "Vivo";
export const PRESENTATION = "Presentación";
export const GAME_PRESENTATION = "Presentación de Juego";
export const CONVERSATION = "Conversatorio";
export const TOURNAMENT = "Torneo";
export const DEBATE = "Mesa Debate";
export const MUESTRA = "Muestra";
export const ROL = "Rol";
export const INTERVIEW = "Entrevista";
export const TYPES = [TALK, WORKSHOP, PRESENTATION, CONVERSATION, TOURNAMENT, ROL, INTERVIEW];
export const WARGAMES = "Wargames";
export const THIRD_AGE = "Adultos Mayores";
export const CHILDISH = "Infantil";
export const TEACHERS = "Docentes";
export const PUBLISHERS = "Editoriales";
export const GRAL = "Público General";
export const EVENT_CATEGORIES = [
  WARGAMES,
  THIRD_AGE,
  CHILDISH,
  ROL,
  TEACHERS,
  PUBLISHERS,
  GRAL,
  PRESENTATION,
  LIVE,
  GAME_PRESENTATION,
  DEBATE
];
export const PLAY_MODAL = "playModal";
export const LIVE_MODAL = "liveModal";
export const MEETINGS_MODAL = "meetingModal";
export const VALID_IMAGE_TYPES = `
image/jpeg, 
image/png, 
image/bmp,`;
export const DISCORD_LINK = "https://discord.gg/Jhv2547";
export const YOUTUBE_LINK = "https://www.youtube.com/embed/5qap5aO4i9A";
export const TWITCH_LINK = "https://player.twitch.tv/?channel=9enjm";
export const YOUTUBE_CHANNEL_LINK =
  "https://www.youtube.com/channel/UCJrEXOh-V8-wgftDkMxM9OQ";
export const TWITCH_CHANNEL_LINK = "https://www.twitch.tv/9enjm";
export const FB_LINK = "https://www.facebook.com/encuentronacionaldejuegosdemesa";
export const TW_LINK = "https://twitter.com/ENJM_arg";
export const IG_LINK = "https://www.instagram.com/encuentronacionaljuegosdemesa/";
export const GENDER_FORM = "https://docs.google.com/forms/d/e/1FAIpQLSdWf0qoDUQirWcvWG6gaij1HVT6_upUtoKfU97Htz8C_MIWIg/viewform";

export const CURRENT_YEAR = new Date().getFullYear();
export const ALL_YEARS = [2020];
