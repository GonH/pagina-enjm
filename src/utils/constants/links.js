import { getItemByComposedKeys } from "../helpers/constantHelpers";

const links = {
  teams: {
    Cantera: "https://www.facebook.com/LaCanteraRecreacion/",
    FRA: "https://www.facebook.com/groups/frenteroleroargentino/",
    Ludicamente: "https://www.facebook.com/ludicamentes/",
    PiedraLibre: "",
    Tribu: "https://www.facebook.com/TRIBUjuegosquejuntan/",
    JTA: "",
    Jugandera: "",
    LVC: "",
    SH: "",
  },
  sponsors: {
    Toyco: "",
    Devir: "",
    Invictus: "",
    Ruibal: "",
    Enjm1: "",
    Enjm2: "",
    Enjm3: "",
    Enjm4: "",
  },
  socialNetworks: {
    twitter: "https://twitter.com/ENJM_arg?lang=es",
    facebook: "https://www.facebook.com/encuentronacionaldejuegosdemesa/",
    instagram: "https://www.instagram.com/encuentronacionaljuegosdemesa/",
    discord: "https://discord.com/invite/Jhv2547",
    youtube: "https://www.youtube.com/channel/UCJrEXOh-V8-wgftDkMxM9OQ"
  },
  tutorials: {
    gamingPlace: "https://www.youtube.com/embed/LOU_fmYZF0o",
    live: "https://www.youtube.com/embed/3YFUFdza21o",
    meetings: "https://www.youtube.com/embed/WcMEdCjt2C4"
  },
  platforms: {
    discord: "https://discord.gg/vGMdnWx"
  }
};

export const getLinkByComposedKeys = (key) => getItemByComposedKeys(links, key);
