import { getItemByComposedKeys } from "../helpers/constantHelpers";

const text = {
  header: {
    menu: "MENU",
    logoAlt: "Logo del Encuentro Nacional de Juegos de Mesa",
    text: {
      home: "Inicio",
      about: "Info",
      team: "El equipo",
      contact: "Contacto",
      sponsors: "Sponsors",
      shops: "Tiendas",
      inicio: "Inicio",
      cronograma: "Cronograma",
      previousIterations: "Encuentros Anteriores",
      tutorial: "Tutoriales",
      gameLibrary: "Ludoteca",
      pastEvents: "Eventos Previos",
      ponchoPrizes: "Premios Poncho",
      currentThings: "Eventos en vivo",
      galleries: "Muestras",
      gameProposals: "Propuestas Lúdicas",
      publishers: "Editoriales",
    },
    links: {
      home: "/",
      about: "/acerca-del-enjm",
      team: "/el-equipo",
      contact: "/contacto",
      sponsors: "/sponsors",
      shops: "/tiendas",
      inicio: "/inicio",
      cronograma: "/cronograma",
      previousIterations: "/encuentros-anteriores",
      meeting: "/encuentro/:meetingId",
      tutorial: "/tutoriales",
      gameLibrary: "/ludoteca",
      pastEvents: "/eventos-previos",
      ponchoPrizes: "/premios-poncho",
      currentThings: "/ahora-mismo",
      galleries: "/galerias",
      specificGallery: "/galerias/:galleryType",
      individualImage: "/galerias/:galleryType/:id",
      adminForm: "/formularios/admin",
      genericAdminForm: "/formularios/admin/:infoType",
      adminPanel: "/panel-de-control/admin",
      adminEdit: "/formularios/admin-edit",
      nextUp: "/a-continuacion",
      editNextUp: "/editar-a-continuacion",
      mailing: "/mailing",
      messaging: "/mensajes",
      discounts: "/descuentos",
      gameProposals: "/propuestas-ludicas",
      mexicanCronograma: "/cronograma-expo-games",
      oldEvents: "/cronograma-archivo",
      publishers: "/editoriales",
      sedes: '/sedes',
      adminSedes: '/admin-sedes'
    },
  },
  genderForm: {
    start: "Te invitamos a completar este formulario anónimo sobre géneros en el marco de los juegos de mesa Tus respuestas nos ayudan a mejorar el ENJM para que todos tengan una mejor experiencia 🤗",
    middle: "formulario anónimo sobre géneros en el marco de los juegos de mesa",
    end: ". Tus respuestas nos ayudan a mejorar el ENJM para que todos tengan una mejor experiencia 🤗"
  },
  about: {
    intro:
      "El 9º Encuentro Nacional de Juegos de Mesa (#9ENJM) se llevará a cabo durante los días 20, 21 y 22 de noviembre. La novedad de su edición 2020 es que en el actual contexto de emergencia sanitaria funcionará íntegramente de modo virtual. El Encuentro se desarrolla anualmente como un espacio federal en el que diseñadores, productores, difusores, comunidades y jugadores además de jugar, comparten experiencias, ideas, y proyectos. Surgido en 2012 por la iniciativa de la Asociación Civil La Cantera, quienes sumando diferentes asociaciones, clubes y agrupaciones vinculadas al mundo del juego de mesa en Argentina; han consolidado una propuesta que hace particular énfasis en lo vincular, favoreciendo la creación de lazos entre los diferentes mundos que hacen al universo lúdico nacional. Así quienes crean, diseñan, ilustran, testean, comparten y difunden juegos de mesa por el país pueden ver a  editoriales, tiendas, clubes y asociaciones y sus propios jugadores, conocerse las caras, escuchar las novedades, y celebrarnos como parte de un mismo apasionante ámbito.",
    imaginandoTitle: "IMAGINANDO",
    imaginandoText:
      "Allá por 2012 nos imaginamos que era posible encontrarnos a quienes hacíamos al mundo del juego de mesa. Por aquella época no había tantos grupos, editoriales independientes, páginas especializadas, ludotecas escolares ni muchos juegos de autor. Aquel Encuentro fue de un día intenso, pero que dió inicio a una partida que aún se sigue jugando.\nCada nuevo encuentro fue ampliándonos la mirada, pasando a durar un poco más año a año y aumentando el caudal de propuestas y la diversidad de participantes, dándonos algunos gustitos como entrevistar a Roberto Fraga (Francia), Ketty Galleguillos (Chile), Oriol Comas i Comas (España) y Reiner Knizia (Alemania) entre muchas personas claves en el mundo de los juegos.\nY no sólo fue creciendo en caudal de asistentes, sino que con cada encuentro, más editoriales, tiendas, podcast, clubes y asociaciones se fueron sumando a participar, extendiendo los límites a más y más puntos del país, incluso atravesando fronteras.\nEn esta hermosa construcción, entendimos que si el Encuentro Nacional quería ser realmente federal, además de invitar a más personas y organizaciones de distintos lugares, hacía falta llevar el evento a diferentes puntos del país. Así fue que el 5ENJM se desarrolló en la ciudad de Córdoba, y luego visitó Mendoza, Bahía Blanca y Villa María.\nAquel primer encuentro de poco más de 100 personas, se convirtió en uno de los eventos más grandes del mundo del juego de mesa en Latinoamérica. Y esto permite seguir soñando y seguir creciendo.",
    tableroTitle: "TABLEROS",
    tableroText:
      "El ENJM es un encuentro, donde vamos principalmente a vernos, a compartir ideas y ganas, a contactar con gente que no conocíamos pero que está en la misma. Y es un encuentro porque nos parece importante que se centre en lo vincular. Que la gente que difunde pueda conocer a quienes crean los juegos para comentarles cómo repercuten en sus encuentros, que tomen forma esos nombres que suelen aparecer como artistas, testers o ilustradores. Y que los juegos sean nuestro medio para ese encuentro.\nDentro de las propuestas, nos parece importante poder contar con espacios de relatos de experiencias, para aprender de los recorridos de otras y otros; así como también talleres de quienes han logrado sintetizar parte de ese recorrido de una manera didáctica.\nBuscamos que sea un espacio de crecimiento en busca de profesionalizar los roles. Es así que nos ponemos en contacto con referentes de los juegos en otros lugares del mundo, para que nos ayuden a pensar lo local con estrategias y formas que van dando resultados en sus lugares de trabajo. Hemos entrevistado a gente de Brasil, Chile, México, Uruguay, Estados Unidos, España, Francia, Alemania e Inglaterra. Desde el diseño, la promoción, la ilustración y las partidas nos han compartido vía videoconferencia, algunas ideas y certezas en torno a sus campos de referencia.\nHablamos de un Encuentro Nacional, por lo que nos parece sumamente importante que se escuchen voces y se vean juegos de distintos puntos del país. Que cuenten de sus realidades y compartan los diseños realizados, compartiendo partidas que puedan hacer posible que lleguen a nuevos hogares, hasta otras latitudes. En el Encuentro de 2019, 16 provincias se trasladaron para encontrarnos a compartir esa fiesta que se dio en la Biblioteca Municipal de Villa María.\nEs interesante rescatar del encuentro la convivencia de una gran multiplicidad de juegos y jugadoras. Uno de los objetivos de los ENJM es su carácter inclusivo y eso se cumple edición tras edición. Podemos ver sobre las mesas desde juegos abstractos como Go, Othello y Mahjong, hasta juegos clásicos como el TEG o nuevos juegos recién premiados en Alemania o España. Pueden encontrarse espacios con partidas de rol, mesas gigantes de wargames, juegos modernos, clásicos, con miniaturas, fichas, cartas, dados y en las versiones que estén. Hasta se pueden encontrar juegos gigantes en madera, recreaciones cual obras de arte con grandes producciones o simples impresiones en blanco y negro para marcar con lápiz.",
    infinitoTitle: "INFINITOS",
    infinitoText:
      "En este 2020 tan atípico, el juego de mesa ha llegado como aliado a las casas, como recurso a través de propuestas docentes y como alternativa de las editoriales que fueron liberando juegos a través de las redes o, como novedad, a través de plataformas y apps. A pesar de la imposibilidad de encontrarnos físicamente, los juegos han logrado mantenernos cerca, invitándonos a dedicarle tiempo al disfrute. Incluso en este contexto se han publicado juegos nuevos de creación nacional y con licencias que han mantenido viva la pasión por jugar. Pero además, se han colgado muchos juegos nuevos a las plataformas online, lo cual viene generando una mayor visibilidad de las ideas locales. Y junto a esto, se ha permitido poder conocer y testear, aunque de formas diferentes, una amplia gama de proyectos.\nEn este contexto nos animamos a imaginar un Encuentro Nacional como una nueva invitación a poder vernos, de una manera distinta, entre quienes habitamos y hacemos este mundo de los juegos. A diferencia de las ediciones anteriores, y por primera vez, este Encuentro será 100% virtual, pero con las mismas ganas y esfuerzo.\nLas más de 20 organizaciones que estamos organizando este Encuentro creemos que, este año, los tableros se extenderán a lo largo de las pantallas y, podremos jugar, quienes habitualmente transitamos los ENJM, pero que además, seguro se sumará un mayor caudal de personas que están a un click de conocer toda la magia y las novedades que este mundo tiene para ofrecerles.\nNos convocamos entonces a participar, construir y soñar con un particular Encuentro este año, completamente Virtual pero buscando sostener la mirada atenta y la cercanía amorosa que creemos tan necesaria, Imaginando Tableros Infinitos.",
  },
  footer: {
    name: "7º Encuentro Nacional de Juegos de Mesa",
    info: "16,17 y 18 Noviembre - Bahía Blanca",
  },
  whatIsENJM: {
    options: {
      basesIlustracion: "Bases para la muestra de Ilustración",
      boletin2: "Boletín",
      rumbo: "Rumbo al ENJM",
      escuelasForm: "Formulario para docentes y escuelas",
      escuelasPropuesta: "Propuesta para escuelas",
    },
    Banner:
      "Imagen: Encuentro nacional de juegos de mesa. 16, 17 y 18 de noviembre de 2018 en Bahía Blanca. El origen del juego",
    titles: {
      first: "Las primeras jugadas",
      theGame: "El juego",
      theBoard: "El tablero",
      thePieces: "Las piezas",
      theNextMatch: "La próxima partida",
    },
    texts: {
      first:
        "Los Encuentros Nacionales de Juegos de Mesa o ENJM nacen en 2012 como una iniciativa de la ONG La Cantera que junto a un puñado de jugadores entusiastas. Cuando aún no había en nuestro país tantos grupos, editoriales independientes, páginas especializadas, ludotecas escolares ni juegos de autor. Sin embargo, aquellas instituciones y personas que veníamos invitándonos a las pocas actividades regulares, nos propusimos encontrarnos.\nAquellas ganas iniciales fueron potenciándonos y ya el segundo encuentro contó con dos jornadas de duración y una convocatoria que superó ampliamente nuestras expectativas, sumando charlas y presentaciones de disertantes de 10 ciudades de 4 provincias.\nEl crecimiento fue acompañado de un aumento de espacios de juego, editoriales, propuestas escolares, programas gubernamentales en todo el país y parte de ello se vio reflejado cada en el cronograma de actividades.\nLuego del 4° Encuentro, y con la invitación de clubes de otras provincias es que propusimos ampliar el equipo de organización y darle al ENJM un carácter federal que lo llevara a desarrollarse cada año en una ciudad distinta de nuestro país. Encontrándonos y estableciendo vínculos con una mayor diversidad de grupos que venían desarrollando movidas sobre el juego.\nAsí fue que el 5ENJM se realizó  en la ciudad de Córdoba, el 6ENJM en Mendoza y este año nos encontraremos en Bahía Blanca.",
      theGame:
        "El ENJM es un encuentro, donde vamos principalmente a vernos, a compartir ideas y ganas, a contactar con gente que no conocíamos pero que está en la misma. Y es un encuentro porque nos parece importante que se centre en lo vincular. Que la gente que difunde pueda conocer a quienes crean los juegos para comentarles cómo repercuten estos en sus movidas, que tomen forma esos nombres que suelen aparecer como artistas, testers o ilustradores. Y que los juegos sean nuestro medio para ese encuentro.\nDentro de las propuestas, nos parece importante poder contar con espacios de relatos de experiencias, para aprender de los recorridos de otras y otros; así como también talleres de quienes han logrado sintetizar parte de ese recorrido de una manera didáctica.\nBuscamos que sea un espacio de crecimiento en busca de profesionalizar los roles. Es así que nos ponemos en contacto con referentes de los juegos en otros lugares del mundo, para que nos ayuden a pensar lo local con estrategias y formas que van dando resultados en sus lugares de trabajo. Hemos entrevistado a gente de Chile, México, Estados Unidos, España, Francia, Alemania e Inglaterra. Diseñadores, promotores, escritores y jugadores que nos han compartido vía videoconferencia, algunas ideas y certezas en torno a sus campos de referencia.\nHablamos de un Encuentro Nacional, por lo que nos parece sumamente importante que se escuchen voces y se vean juegos de distintos puntos del país. Que cuenten de sus realidades y compartan los diseños realizados, compartiendo partidas que puedan hacer posible que lleguen a nuevos hogares, en otras latitudes.\nY por último, es un Encuentro Nacional de Juegos de Mesa. Esto quiere decir que pueden encontrarse partidas de rol, de wargames, modernos, clásicos… con miniaturas, fichas, cartas, dados y en las versiones que estén. Se pueden encontrar juegos gigantes en madera, obras de arte con grandes producciones o simples impresiones en blanco y negro.\nEs interesante rescatar del encuentro la convivencia de una gran multiplicidad de juegos y jugadores. Uno de los objetivos de los ENJM es su carácter inclusivo y eso se cumple edición tras edición. Podemos ver sobre las mesas desde juegos tradicionales de los que aún se juegan en casa hasta nuevos juegos recién premiados en Alemania o España",
      theBoard:
        "Creemos que la coyuntura incide en cómo se va desarrollando este mundo de los juegos y es por ello que, cada año, vamos buscando un tema que se desarrolle a lo largo del evento. Este, nos invita a pensar sobre las certezas, ampliar la mirada, compartir acuerdos y desacuerdos y entonces, poder seguir enriqueciéndonos colectivamente.\nPara nosotros el ENJM tiene que ser un evento que trascienda a sus actores porque nos enriquece y motiva para seguir creciendo e investigando. Es por ello que los temas de cada año, abren a preguntas pero que también, quienes habitan el encuentro, lo cargan de sentido y le dan forma.",
      thePieces:
        "Estos encuentros son posibles gracias a que los vamos construyendo junto a sus participantes. Si bien la idea original fue de la Asociación Civil La Cantera. Proyectos en el Campo de la Recreación, la realidad es que este evento va tomando forma gracias al trabajo de un colectivo de organizaciones. En esta ocasión a nivel local, el trabajo lo lleva adelante la Asociación Civil Piedra Libre junto a 10 instituciones locales como Yu-Gi-Oh Bahía, Bahía Rol, Boardgames Bahía Blanca y la Casa Homo Ludens. .A nivel nacional el equipo también está conformado por la comunidad federal del Frente Rolero Argentino y el colectivo de editoriales Tribu. Juegos que Juntan y Lúdicamente. Además, este año se han sumado Conexión Berlín y Jugartmus.\nLa intención es seguir sumando colectivos que fortalezcan la mirada territorial a cada encuentro sin perder de vista una proyección nacional desde una perspectiva federal",
      theNextMatch:
        "Este año nos proponemos pensar en torno a El Origen del Juego. Desde diferentes puntos de vista, nos iremos compartiendo investigaciones y experiencias que presenten formas de diseñar juegos, de llevarlos al aula, de recorrer jugueterías, de habitar los espacios laborales, partiendo de los orígenes. ¿Cómo nacen los juegos para Reiner Knizia, el diseñador de juegos más prolífico de la actualidad con más de 500 juegos editados? ¿De qué manera se puede ir ludificando un espacio laboral o educativo para la bahiense Sandra Vissani? ¿Cómo creó la ludoteca en la biblioteca escolar el profesor Fernando Mazzoni de Neuquén? ¿Cómo le da vida a batallas épicas el ilustrador tucumano César Carrizo? ¿Cómo fueron ganando espacio las mujeres en los juegos de rol según la mirada de la bonaerense Valeria Kinder Roberts?\nTodo esto lo vamos a estar compartiendo junto a más de 20 editoriales, cerca de 30 clubes de juegos de mesa y rol, torneos de Magic, fútbol chapas, zonas de juegos infantiles, una ludoteca con más de 200 juegos de distintos lugares del mundo y más de 2000 participantes con ganas de jugar.\nEl 7° Encuentro Nacional de Juegos de Mesa lo realizaremos en Uno Bahía Club (Lavalle 605) del 16 al 18 de noviembre, de 9 a 20hs.",
    },
  },
  teams: {
    title: "El Equipo",
    text:
      "Conocé a los colectivos que conforman la organización\ndel 10° Encuentro Nacional de Juegos de Mesa.",
  },
  sedes: {
    title: "Sedes",
    subtitle: "Estas son las ciudades donde va a haber encuentros en paralelo al encuentro en Almirante Brown. ¡Podés acercarte a la que quieras!"
  },
  sponsors: {
    title: "Los Sponsors",
    text:
      "Conocé a los sponsors que hicieron posible la organización\ndel 10° Encuentro Nacional de Juegos de Mesa.",
  },
  shops: {
    title: "Las Tiendas",
    text:
      "Conocé a las tiendas que acompañan al encuentro.\nHacé click sobre la foto de una tienda para ir a una tienda\ny ver sus descuentos.",
  },
  shops: {
    title: "Tiendas",
    text:
      "Estas son las tiendas que ayudan a que el ENJM se haga.\nHacé click sobre sus imágenes para ver los descuentos y promociones en sus tiendas.",
    discounts: "¡Ofertas destacadas!",
  },
  publishers: {
    title: "Editoriales y Juegos Argentinos",
    publishers: "Para ver a los juegos hacé click en \"Ver Juegos\"",
    seepublishers: "Ver Editoriales",
    games: "Para ver a las editoriales hacé click en \"Ver Editoriales\"",
    seegames: "Ver Juegos"
  },
  contact: {
    mail: {
      title: "CONTACTANOS",
      mail: "encuentronacionaljuegosdemesa@gmail.com",
      decorator: "__",
    },
    socialNetworks: "Seguinos en nuestras redes",
    sendMail: {
      explanation:
        "Si querés dejarnos un mensaje escribinos lo que quieras y hacé click en enviar",
      name: "Nombre *",
      email: "Email *",
      topic: "Asunto",
      message: "Mensage *",
      send: "Enviar",
    },
    joinMailList: {
      explanation:
        "Si querés unirte a la lista de mails del ENJM, escribí tu dirección de email",
      join: "Unirse",
    },
  },
  gameProposal: {
    title: "Propuestas Lúdicas",
    subtitle:
      'En esta página vas a encontrar las propuestas lúdicas que tenemos para vos: Torneos, Espacio para Prototipos, Salas de Escape, Juegos de Rol y una amplia Ludoteca Virtual.\nEn la tabla podés ver las propuestas que hay disponibles.\nHacele click a la que te interese para ver más información.\nAlgunas requieren preinscripción, ¡así que por las dudas revisá los que te llamen la atención!\nPodés filtrarlas por día.\nPuede que las propuestas que dicen "Hora estimada" cambien de horario. El horario final va a ser confirmado más adelante.',
  },
  cronograma: {
    title: "Cronograma",
    empty: "No hay eventos para esos filtros :(",
    subtitle:
      'En la tabla podés ver los eventos que hay disponibles.\nHacele click al que te interese para ver más información sobre él.\nAlgunos requieren preinscripción, ¡así que por las dudas revisá los que te llamen la atención!\nPodés filtrarlos por tipo, categoría o día.\nPuede que los eventos que dicen "Hora estimada" cambien de horario. El horario final va a ser confirmado más adelante.',
    previousYears: 'Hacé click aca para ver los eventos de años anteriores (2020)',
    externalSubtitle:
      "En la tabla podés ver los eventos de la Expo Games (MX) que hay disponibles.\nHacele click al que te interese para ver más información sobre él.",
    noLink: "Este evento todavía no tiene link",
    takePart: "Usá este link para participar",
    watchEvent: "Mirá el evento con este link",
    types: {
      panels: "Charlas",
      tournaments: "Torneos",
      talks: "Conversatorios",
      specialEvents: "Eventos Especiales",
    },
    day: "Día ",
    filter: {
      startHour: "Hora de inicio",
      endHour: "Hora de fin.",
      startMinute: "Minuto de inicio",
      endMinute: "Minuto de fin.",
      showEverything: "Ver también los eventos que ya pasaron",
      showOnlyNext: "Ver los eventos siguientes",
      from: "Desde (fecha y hora)",
      to: "Hasta (fecha y hora)",
      type: "Tipo de evento",
      filter: "Filtrar",
      defaultType: "Todos los tipos",
      day: "Día",
    },
  },
  home: {
    title: "10° Encuentro Nacional de Juegos de Mesa",
    subtitle: "Imaginando Tableros Infinitos",
    pageExplanation:
      "Desde acá vas a poder acceder a todas las actividades que tenemos para vos.\nEn la sección “Cronograma” están todas las charlas, talleres y entrevistas.\nEn “Propuestas lúdicas” encontrarás toda una ludoteca online, la agenda de mesas de rol, pruebas de juegos con sus creadores, torneos oficiales, etc.\nHay varias secciones más, que te invitamos a investigar y recorrer desde el “Menú”.\nPara ingresar a nuestro “hall central”, hacé click en el botón “Unirse al Discord del encuentro”, que te llevará directo al Discord donde se organizan gran parte de las actividades.",
    link: {
      gamingPlace: "Unirse al Discord del encuentro",
      live: "Mirá las charlas en vivo acá",
      meetings: "Podés acceder a los conversatorios y otras juntadas acá",
    },
    join: {
      alreadyKnow: "¿Ya sabés como usar discord? Hacé click acá",
      dontKnow:
        "Si no sabes usarlo y querés ver tutoriales hace click acá para ir a la página de tutoriales",
      gamingPlace: {
        title: "Info para jugar",
      },
      live: {
        title: "Info para ver transmiciones en vivo",
      },
      meetings: {
        title: "Info para participar de charlas in conversatorios",
      },
    },
    rightNow: {
      nothing:
        "De momento no se está transmitiendo nada,\n¡pero entrá en otro momento ver las transmiciones en vivo!",
      youtube: "Ver vivo de Youtube",
      otherStream: "Ver vivo de otro stream",
      youtubeChannel: "Ir al canal de youtube",
      twitchChannel: "Ir al canal de twitch",
      subtitle: "Esto es lo que está pasando ahora mismo",
    },
  },
  previousIteration: {
    title: "Encuentros Anteriores",
    subtitle:
      "Hacé click sobre la imagen de algún encuentro para ver información mas detallada",
    bannerAlt: "Imagen de introducción a la pagina de encuentros anteriores",
    iterations: {
      1: 1,
    },
  },
  tutorials: {
    title: "Tutoriales",
    subtitle:
      "En esta página vas a encontrar tutoriales para aprender a usar discord en diferentes plataformas. Cuando sientas que ya aprendiste como para usarlo hacé click acá para unirte y disfrutar del ENJM.",
  },
  tutorial: {
    gamingPlace: {
      title: "Salas de juego",
      join: "Si ya tenés cuenta y sabés usar discord hacé click acá",
      link: "https://discordapp.com/",
    },
    live: {
      title: "Transmiciones en vivo",
      join:
        "Si ya sabés usar las transmiciones en vivo de youtube hacé click acá",
      link: "https://youtube.com",
    },
    meetings: {
      title: "Charlas y Conversatorios",
      join: "Si ya tenés cuenta y sabés usar zoom hacé click acá",
      link: "https://zoom.com",
    },
    web: {
      title: "Usar discord en el navegador",
    },
    windows: {
      title: "Usar discord en windows",
    },
    android: {
      title: "Usar discord en android",
    },
  },
  gameLibrary: {
    title: "Ludoteca",
    description:
      "En esta página vas a encontrar los juegos que van a estar disponibles durante el encuentro.\nPodés usar los filtros para mostrar los juegos que sean para una determinada cantidad de jugadores, duren un rango de tiempo u otras opciones.\nPodés hacer click sobre un juego para ver mas información, como tutoriales o las plataformas para jugar.",
    modal: {
      title: "",
      description: "",
      tutorials: "Tutorial:",
      platformSingular: "Plataforma para jugar:",
      platformPlural: "Plataformas para jugar:",
      editorial: "Editorial",
      players: "Cantidad de Jugadores:",
      playTime: "Tiempo de juego:",
      gameType: "",
    },
    filter: {
      playerCount: "Cantidad de jugadores",
      playTime: "Tiempo de partida",
      type: "Tipo de juego",
      defaultType: "Elegí un tipo de juego",
      filter: "Filtrar",
    },
    placeholders: {
      minPlayerNumber: "Min.",
      maxPlayerNumber: "Max.",
      minPlayTime: "Min.",
      maxPlayTime: "Max.",
    },
  },
  previousEvents: {
    goBack: "Ir a la lista de eventos pasados",
    title: "Eventos Previos",
    info: "En la tabla podés ver los eventos de encuentros anteriores.",
    back: "Volver al cronograma",
    subtitle:
      "En la tabla podés ver los eventos que ya pasaron.\nPodés filtrarlos por tipo.",
    empty: "No hay eventos previos para esos filtros :(",
    titles: {
      charlasTaller: "Charlas / Taller",
      espaciosDisponibles: "Espacios Disponibles",
      clinicas: "Clínicas de Juegos",
      torneos: "Torneos",
      videoconferencias: "Videoconferencias",
      conversatoriosMesaRedonda: "Conversatorios / Mesa Redonda",
      presentaciones: "Presentaciones",
      juegosDeLaMente: "Juegos de la Mente",
      metajuego: "Metajuego",
    },
  },
  ponchoPrizes: {
    title: "Premios Poncho",
    subtitle:
      "Acá vas a encontrar a los ganadores de los anteriores años de los premios poncho.\nHacé click sobre el nombre del juego que quieras para ver su página (o la de su editorial).\nTambién podés filtrar a los premios por categoría o año.",
    noResults: "No hay resultados para los filtros enviados",
    filter: {
      filter: "Filtrar",
      year: "Año",
      defaultYear: "Elegír año",
      category: "Categoría",
      defaultCategory: "Elegír categoría",
      filterByYear: "Años",
      filterByCategory: "Categorías",
      clear: "Limpiar filtros",
    },
    prize: {
      publisher: "Editorial:",
      author: "Autor:",
    },
  },
  building: {
    name: "IMAGINANDO TABLEROS INFINITOS",
    date: "20, 21 y 22 de noviembre",
    title: "Estamos trabajando en la página del ENJM",
    subtitle:
      "Queremos que tengas toda la información necesaria para poder participar,\npor eso estamos construyendo una página a la altura.\nCuando ya no veas esta página es que está lista.\n:)",
  },
  currentThings: {
    title: "AHORA MISMO",
    subtitle: "Estas son los vivos y juegos que están pasando ahora mismo.",
    games:
      "Estos son los juegos que se están jugando en nuestro discord, ¡hacé click acá o sobre cualquiera de los juegos para unirte!",
    twitch: "¡Mirá este vivo en twitch!",
    youtube: "¡Mirá este vivo en youtube!",
  },
  galleries: {
    title: "Galerias",
    subtitles:
      "Hay muchas muestras de fotografía e ilustración, podés ver las obras o mandar la tuya.\nEstán las siguientes categorías:",
    categoriesTitle: "Categorías",
    categories: [
      "Muestra de Arte",
      "Miniaturas de Wargames",
      // "Actividades de Escuelas",
      "Espacio Abierto",
    ],
    howToParticipate:
      'Para participar hacé click en el botón de la categoría y en esa página hacé click en "Quiero participar"',
    galleryOf: "Muestra de ",
    participate: "Quiero participar",
    sendNew: "Enviar nueva imagen de ",
    showAs: {
      gallery: "Mostrar en galería",
      carousel: "Mostrar en presentación",
    },
    noEntries:
      "Todavía no hay imágenes subidas, ¡podés ser el primero en mandar una foto o dibujo!",
    goBack: "Volver",
    backToGalleries: "Volver a página de galerias",
    Wargames: {
      title: "Wargames",
    },
    Tapas_de_juegos: {
      title: "Tapas de juegos",
    },
    Tablero_armado: {
      title: "Tableros Armados",
    },
    Miniaturas_de_Wargames: {
      title: "Miniaturas de Wargames",
    },
    Escenografia_de_Wargames: {
      title: "Escenografía de Wargames",
    },
    Actividades_de_Escuelas: {
      title: "Actividades de Escuelas",
    },
    Arte: {
      title: "Espacio Abierto",
    },
    Competencia_de_Arte: {
      title: "Muestra de Arte"
    },
    sendForm: {
      imageRequired:
        "Tenés que adjuntar una imagen para poder enviar el formulario",
      sendForm: "Enviar formulario",
      labels: {
        submitterName: "Nombre:",
        submitterEmail: "Correo electrónico:",
        imageName: "Nombre de la obra:",
        additionalInfo: "Información adicional:",
      },
      placeholders: {
        submitterName: "Tu nombre (si querés)",
        submitterEmail: "Tu correo electrónico (si querés)",
        imageName: "El nombre de la imagen (si querés)",
        additionalInfo: "Info extra sobre la imagen (si querés)",
      },
      image: "Subí la imagen:",
      clickHereForFileUpload: "Hacé click acá para subir la imagen",
      pickAnotherOne: "¿Querés elegir otra foto? Hacé click acá",
    },
    specificImage: {
      newCommentPlaceholder: "Escribí aca un comentario para la foto",
      submitComment: "Enviar comentario",
      comments: "Comentarios:",
      backToGallery: "Volver a galeria de ",
    },
  },
  admin: {
    placeholders: {
      name: "Nombre",
      link: "https://www.pagina.com",
      description: "Descripción",
      playerNumber: "2 - 4",
      playTime: "10 - 15 minutos",
      publisher: "Editorial",
      defaultType: "Elegí un tipo de información",
      defaultCategory: "Elegí la categoría",
      defaultTier: "Elegí el tipo de sponsor",
      defaultHour: "Elegí la hora del evento",
      defaultMinute: "Elegí el minuto",
      defaultDay: "Elegí el día del evento",
      minPlayerNumber: "2",
      maxPlayerNumber: "5",
      minPlayTime: "15",
      maxPlayTime: "70",
      platform: "https://www.tabletopia/ejemplo.com",
      participant: "Persona X",
      eventTypes: "Tipo de evento",
    },
    text: {
      name: "Nombre",
      link: "Link a la página",
      liveLink: "Link a donde se haga el vivo / sala de zoom",
      description: "Descripción",
      playerNumber: "Cantidad de jugadores",
      playTime: "Duración de partida",
      publisher: "Editorial",
      type: "Tipo de información",
      category: "Categoría",
      tier: "Tipo de sponsor",
      password: "Contraseña",
      startHour: "Hora de comienzo",
      startMinute: "Minute de comienzo",
      endHour: "Hora de finalizacion",
      endMinute: "Minute de finalizacion",
      day: "Dia",
      minPlayerNumber: "Minima cantidad de jugadores (SOLO el numero)",
      maxPlayerNumber:
        "Maxima cantidad de jugadores (SOLO el numero, si no hay maximo no pongas nada)",
      minPlayTime:
        "Minima duracion de una partida (en minutos, SOLO el numero)",
      maxPlayTime:
        "Maxima duracion de una partida (en minutos, SOLO el numero, si no es un rango no pongas nada)",
      platformTitle: "Plataformas",
      platformSubtitle:
        "Escribi el link a la plataforma y hace click en agregar plataforma. Si queres borrar una de la lista hace click en el boton con su nombre. No es obligatorio y no hay limite maximo de plataofrmas.",
      submitPlatform: "Agregar plataforma",
      participantTitle: "Participantses",
      participantSubtitle:
        "Escribi quienes van a dar/participar de la charla/taller/evento. Si queres borrar a una persona de la lista hace click en el boton con su nombre. No es obligatorio y no hay limite maximo de participantes.",
      submitParticipants: "Agregar participante",
      eventTypes: "Selecciona el tipo de evento",
    },
    types: {},
  },
};

export const getTextByComposedKeys = (key) => getItemByComposedKeys(text, key);
