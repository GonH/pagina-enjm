export const getItemByComposedKeys = (infoJSON, key) => {
  const keys = key.split(".");

  let result = infoJSON;

  keys.forEach((currentKey) => {
    result = result[currentKey];
  });

  return result;
};

export const getGalleryType = (rawType) => {
  const titlesDict = {
    Muestra_de_Arte: "Competencia_de_Arte",
    Espacio_Abierto: "Arte",
  };

  const parsedTitle = titlesDict[rawType];

  return parsedTitle || rawType;
};
