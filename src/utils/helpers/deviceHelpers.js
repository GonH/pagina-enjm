// Se que esto no es un verdadero id y deberia usar uuid v4,
// pero se usa para algo que no tiene nada que ver con seguridad
// asi que clavar un random no es problema, y si hay colision
// bueno
const generateRandomId = () => `${Math.random() * 10 ** 16}`;

export const getRandomId = () => {
  let id = localStorage.getItem("webId");

  if (!id) {
    id = generateRandomId();

    localStorage.setItem("webId", id);
  }

  return id;
};
