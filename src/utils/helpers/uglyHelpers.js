export const openLinkOnNewTab = (link) => {
  const anchor = document.createElement("a");

  anchor.href = link;
  anchor.target = "_blank";
  anchor.rel = "noopener noreferrer";
  anchor.style.display = "none";

  document.body.appendChild(anchor);
  anchor.click();
  document.body.removeChild(anchor);
};

export const getStyle = ({ height, width }) => {
  if (!height || !width) return {};

  return {
    height: height * 0.8,
    width: width * 0.8,
  };
};
